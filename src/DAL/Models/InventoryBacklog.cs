﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class InventoryBacklog:AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid SerialNumberId { get; set; }
        public string status { get; set; }
        public string ReceivedDate { get; set; }
        public string Remarks { get; set; }
    }
}
