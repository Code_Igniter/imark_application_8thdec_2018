﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class PosStock : AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public Guid SerialNumberId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public Guid? DeploymentId { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string SerialNumber { get; set; }
        public DateTime StockDate { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string Location { get; set; }


    }
}
