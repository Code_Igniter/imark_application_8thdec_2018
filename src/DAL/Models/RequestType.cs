﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class RequestType : AuditableEntity
    {
        public Guid Id { get; set; }
        public string RequestName{get;set;}
    }
}
