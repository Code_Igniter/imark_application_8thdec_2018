﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class SerialNumber: AuditableEntity
    {
        public Guid Id { get; set; }
        public string SerialKey { get; set; }
        
        public Guid InventoryAddId { get; set; }
        public bool IsSold { get; set; }
        public bool IsDeployed { get; set; }


    }
}
