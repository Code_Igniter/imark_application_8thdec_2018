﻿  using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Support: AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public bool Phone { get; set; }
        public bool Field { get; set; }
        public bool Status { get; set; }
        public bool Priority { get; set; }
        public string Issue { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string ResolvedBy { get; set; }
        public string Remarks { get; set; }
        public DateTime SupportDate { get; set; }
        public string Verification { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }

    }
}
