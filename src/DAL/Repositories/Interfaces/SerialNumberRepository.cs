﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public class SerialNumberRepository: Repository<SerialNumber>, ISerialNumberRepository
    {
        public SerialNumberRepository(ApplicationDbContext context): base(context)
        {
        }
    }
}
