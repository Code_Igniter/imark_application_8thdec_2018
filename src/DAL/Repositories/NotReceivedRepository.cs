﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class NotReceivedRepository : Repository<NotReceivedPos>, INotReceivedRepository
    {
        public NotReceivedRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
