﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class PosHistoryRepository : Repository<PosHistory>, IPosHistoryRepository
    {
        public PosHistoryRepository(ApplicationDbContext context): base(context)
        {

        }
    }
}
