﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class PosStockRepository : Repository<PosStock>, IPosStockRepository
    {
        public PosStockRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
