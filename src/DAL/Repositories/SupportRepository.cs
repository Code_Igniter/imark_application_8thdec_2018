﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
   public class SupportRepository: Repository<Support>, ISupportRepository
    {
        public SupportRepository(ApplicationDbContext context): base(context)
        {

        }
    }
}
