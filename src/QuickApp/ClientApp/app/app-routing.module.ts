﻿
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { CustomersComponent } from "./components/customers/customers.component";
import { InventoryComponent } from "./components/inventory/inventory.component";
import { InventoryAddComponent } from "./components/inventory/inventory-add.component";
import { InventoryPosAddComponent } from "./components/inventory/inventory-addSrno.component";
import { InventoryAddListComponent } from "./components/inventory/inventory-add-list.component";
import { HardwareSupportComponent } from "./components/hardwareSupport/hardware-support.component";
import { HardwareSupportEditComponent } from "./components/hardwareSupport/hardware-support-edit.component";
import { HwSupportComponent } from "./components/HwSupport/hwSupport.component";
import { HwSupportListComponent} from "./components/HwSupport/hwSupport-list.component";

import { SerialAddComponent } from "./components/inventory/serial-no.component";
import { ConfigurationsComponent } from "./components/configuration/configuration.component";
import { ConfigurationEditComponent } from "./components/configuration/configuration-edit.component";
import { DeploymentListComponent } from "./components/deployment/deployment-list.component";
import { DeployComponent } from "./components/deploy/deploy.component";
import { BankComponent } from './components/bank/bank.component';
import { BankEditComponent } from './components/bank/bank-edit.component';
import { ApplicationComponent } from './components/applicationVersion/application.component';
import { ApplicationEditComponent } from './components/applicationVersion/application-edit.component';

import { PosHistoryComponent } from "./components/posHistory/history.component";
import { PosStockComponent } from "./components/stock/pos-stock.component";
import { PosSparesStockComponent } from "./components/stock/pos-spares-stock.component";
import { OldRecordComponent } from "./components/posOldRecords/old-records.component";
import { OtherStockListComponent } from "./components/otherstock/otherstock.component";
import { OrdersComponent } from "./components/orders/orders.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { AboutComponent } from "./components/about/about.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { SupportComponent } from './components/support/support.component';
import { SpareRecordComponent } from "./components/spareHistory/spare-records.component";

import { SupportListComponent } from './components/support/support-list.component';
import { DeployListComponent } from './components/deploy/deploy-list.component';
import { PosSparesComponent } from './components/posSpares/posSpares.component';
import { NotReceivedPosListComponent } from './components/notreceivedpos/not-received-pos.component';
import { DamagePosListComponent } from './components/damagepos/damage-pos.component';
import { SerialInStockComponent } from "./components/inventory/serial-in-stock.component";

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: "", component: HomeComponent, canActivate: [AuthGuard], data: { title: "Home" } },
            { path: "login", component: LoginComponent, data: { title: "Login" } },
            { path: "list", component: InventoryAddListComponent, canActivate: [AuthGuard], data: { title: "Inventory List" } },
            { path: "inventory", component: InventoryComponent, canActivate: [AuthGuard], data: { title: "Inventory Information" } },
            { path: "serialinstock", component: SerialInStockComponent, canActivate: [AuthGuard], data: { title: "Serial In Stock" } },
            { path: "posspares", component: PosSparesComponent, canActivate: [AuthGuard], data: { title: "Pos Spares" } },
            
            { path: "add", component: InventoryAddComponent, canActivate: [AuthGuard], data: { title: "Inventory Add" } },
            { path: "addpos", component: InventoryPosAddComponent, canActivate: [AuthGuard], data: { title: "Inventory Pos Add" } },
            { path: "serial/:id", component: SerialAddComponent, canActivate: [AuthGuard], data: { title: "Serial Add" } },
            { path: "configuration", component: ConfigurationsComponent, canActivate: [AuthGuard], data: { title: "Configure" } },
            { path: "deployment", component: DeploymentListComponent, canActivate: [AuthGuard], data: { title: "Deployment" } },
            { path: "deploy", component: DeployComponent, canActivate: [AuthGuard], data: { title: "Deploy" } },
            { path: "poshistory", component: PosHistoryComponent, canActivate: [AuthGuard], data: { title: "Pos History" } },
            { path: "posstock", component: PosStockComponent, canActivate: [AuthGuard], data: { title: "Pos Stock" } },
            { path: "sparesstock", component: PosSparesStockComponent, canActivate: [AuthGuard], data: { title: "Pos Spares Stock" } },
            { path: "posrecords", component: OldRecordComponent, canActivate: [AuthGuard], data: { title: "Old Records" }},
            { path: "otherstock", component: OtherStockListComponent, canActivate: [AuthGuard], data: { title: "Other Stock" } },
            { path: "maintenance", component: MaintenanceComponent, canActivate: [AuthGuard], data: { title: "Maintenance" } },
            { path: "support", component: SupportListComponent, canActivate: [AuthGuard], data: { title: "Support" } },
            { path: "deploylist", component: DeployListComponent, canActivate: [AuthGuard], data: { title: "Deploy List" } },
            { path: "posnotreceivedlist", component: NotReceivedPosListComponent, canActivate: [AuthGuard], data: { title: "Pos Not Received List" } },
            { path: "settings", component: SettingsComponent, canActivate: [AuthGuard], data: { title: "Settings" } },
            { path: "damagepos", component: DamagePosListComponent, canActivate: [AuthGuard], data: { title: "Damage Pos" } },
           
            { path: "about", component: AboutComponent, data: { title: "About Us" } },
            { path: "home", redirectTo: "/", pathMatch: "full" },
            { path: "bank", component: BankComponent, data: { title: "Bank" } },
            { path: "application", component: ApplicationComponent, data: { title: "Application" } },
            { path: "hardwaresupport", component: HardwareSupportComponent, data: { title: "Hardware Support" } },
            { path: "hwsupportlist", component: HwSupportListComponent, data: { title: "Hw Support List" } },
            { path: "sparerecords", component: SpareRecordComponent, data: { title: "Pos Spare History" } },
            { path: "**", component: NotFoundComponent, data: { title: "Page Not Found" } },
        ])
    ],
    exports: [
        RouterModule
    ],
    providers: [
        AuthService, AuthGuard
    ]
})
export class AppRoutingModule { }