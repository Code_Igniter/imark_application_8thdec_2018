
import { NgModule, ErrorHandler } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from "./material.module";


import 'bootstrap';
import "ag-grid-enterprise";

import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastyModule } from 'ng2-toasty';  
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { PopoverModule } from "ngx-bootstrap/popover";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ChartsModule } from 'ng2-charts';
import { DateRangePickerModule } from 'ng-pick-daterange';
import { AgGridModule } from 'ag-grid-angular';




import { AppRoutingModule } from './app-routing.module';
import { AppErrorHandler } from './app-error.handler';
import { AppTitleService } from './services/app-title.service';
import { AppTranslationService, TranslateLanguageLoader } from './services/app-translation.service';
import { ConfigurationService } from './services/configuration.service';
import { AlertService } from './services/alert.service';
import { LocalStoreManager } from './services/local-store-manager.service';
import { EndpointFactory } from './services/endpoint-factory.service';
import { NotificationService } from './services/notification.service';
import { NotificationEndpoint } from './services/notification-endpoint.service';
import { AccountService } from './services/account.service';
import { InventoryService } from './services/inventory.service';
import { AccountEndpoint } from './services/account-endpoint.service';
import { InventoryEndpoint } from './services/inventory-endpoint.service';
import { InventoryAddService } from './services/inventory-add.service';
import { InventoryAddEndpoint } from './services/inventory-add-endpoint.service';
import { HardwareSupportService } from './services/hardwareSupport/hardware-support.service';
import { HardwareSupportEndpoint } from './services/hardwareSupport/hardware-support-endpoint.service';

import { HwSupportService } from './services/hwSupport/hwSupport.service';
import { HwSupportEndpoint } from './services/hwSupport/hwSupport-endpoint.service';

import { PosStockService } from './services/posStock/pos-stock.service';
import { PosStockEndpoint } from './services/posStock/pos-stock-endpoint.service';
import { DashboardEndPoint } from './services/dashboard/dashboard-endpoint.service';
import { DashboardService } from './services/dashboard/dashboard.service';


import { SerialNoService } from './services/serial-no.service';
import { SerialEndpoint } from './services/serial-no-endpoint.service';
import { ConfigurationsService } from './services/configuration/configuration.service';
import { ConfigurationEndpoint } from './services/configuration/configuration-endpoint.service';
import { DeploymentService } from './services/deployment/deployment.service';
import { DeploymentEndpoint } from './services/deployment/deployment-endpoint.service';
import { BankService } from './services/bank/bank.service';
import { BankEndpoint } from './services/bank/bank-endpoint.service';

import { RequestService } from './services/requestType/request-type.service';
import { RequestTypeEndpoint } from './services/requestType/request-type-endpoint.service';
import { StatusEndpoint } from './services/status/status-endpoint.service';
import { StatusService } from './services/status/status.service';
import { CheckerEndpoint } from './services/checker/checker-endpoint.service';
import { CheckerService } from './services/checker/checker.service';
import { ApplicationService } from './services/applicationVersion/application.service';
import { ApplicationEndpoint } from './services/applicationVersion/application-endpoint.service';
import { DeployService } from './services/deploy/deploy.service';
import { DeployEndpoint } from './services/deploy/deploy-endpoint.service';


import { PosHistoryService } from './services/poshistory/pos-history.service';
import { PosHistoryEndpoint } from './services/poshistory/pos-history-endpoint.service';
import { OtherStockService } from './services/otherstock/otherstock.service';
import { OtherStockEndpoint } from './services/otherstock/otherstock-endpoint.service';
import { MaintenanceService } from './services/maintenance/maintenance.service';
import { MaintenanceEndpoint } from './services/maintenance/maintenance-endpoint.service';
import { SupportService } from './services/support/support.service';
import { SupportEndpoint } from './services/support/support-endpoint.service';

import { EqualValidator } from './directives/equal-validator.directive';
import { LastElementDirective } from './directives/last-element.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { BootstrapTabDirective } from './directives/bootstrap-tab.directive';
import { BootstrapToggleDirective } from './directives/bootstrap-toggle.directive';
import { BootstrapSelectDirective } from './directives/bootstrap-select.directive';
import { BootstrapDatepickerDirective } from './directives/bootstrap-datepicker.directive';
import { GroupByPipe } from './pipes/group-by.pipe';

import { AppComponent } from "./components/app.component";
import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { CustomersComponent } from "./components/customers/customers.component";
import { ProductsComponent } from "./components/products/products.component";
import { OrdersComponent } from "./components/orders/orders.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { AboutComponent } from "./components/about/about.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";

import { BannerDemoComponent } from "./components/controls/banner-demo.component";
import { TodoDemoComponent } from "./components/controls/todo-demo.component";
import { StatisticsDemoComponent } from "./components/controls/statistics-demo.component";
import { DashboardStockComponent } from "./components/controls/dashboard-stock.component";
import { PosStatusComponent } from "./components/controls/pos-status.component";
import { NotificationsViewerComponent } from "./components/controls/notifications-viewer.component";
import { SearchBoxComponent } from "./components/controls/search-box.component";
import { UserInfoComponent } from "./components/controls/user-info.component";
import { InventoryEditComponent } from "./components/inventory/inventory-edit.component";
import { UserPreferencesComponent } from "./components/controls/user-preferences.component";
import { UsersManagementComponent } from "./components/controls/users-management.component";
import { InventoryComponent } from "./components/inventory/inventory.component";
import { SerialInStockComponent } from "./components/inventory/serial-in-stock.component";
import { SerialListInStockComponent } from "./components/inventory/serial-list-in-stock.component";

import { HardwareSupportComponent } from "./components/hardwareSupport/hardware-support.component";
import { HardwareSupportEditComponent } from "./components/hardwareSupport/hardware-support-edit.component";
import { HwSupportComponent } from "./components/hwSupport/hwSupport.component";
import { HwSupportEditComponent } from "./components/HwSupport/hwSupport-edit.component";
import { HwSupportListComponent } from "./components/HwSupport/hwSupport-list.component";

import { PosStockComponent } from "./components/stock/pos-stock.component";
import { PosStockEditComponent } from "./components/stock/pos-stock-edit.component";
import { PosStockHistoryComponent } from "./components/stock/pos-stock-history.component";
import { PosSparesStockComponent } from "./components/stock/pos-spares-stock.component";

import { PosSparesComponent } from "./components/posSpares/posSpares.component";
import { PosSparesEditComponent } from "./components/posSpares/posSpares-edit.component";
import { SpareRecordComponent } from "./components/spareHistory/spare-records.component";

import { BankComponent } from "./components/bank/bank.component";
import { BankEditComponent } from "./components/bank/bank-edit.component";
import { ApplicationComponent } from "./components/applicationVersion/application.component";
import { ApplicationEditComponent } from "./components/applicationVersion/application-edit.component";

import { InventoryAddComponent } from "./components/inventory/inventory-add.component";
import { InventoryPosAddComponent } from "./components/inventory/inventory-addSrno.component";
import { InventoryAddListComponent } from "./components/inventory/inventory-add-list.component";
import { InventoryAddEditComponent } from "./components/inventory/inventory-add-edit.component";
import { SerialAddComponent } from "./components/inventory/serial-no.component";
import { EditSerialNoComponent } from "./components/inventory/edit-serial-no.component";
import { ConfigurationsComponent } from "./components/configuration/configuration.component";
import { ConfigurationEditComponent } from "./components/configuration/configuration-edit.component"
import { DeploymentRequestComponent } from "./components/deployment/deployment-add.component";
import { DeploymentListComponent } from "./components/deployment/deployment-list.component";
import { MaintenanceAddStatusComponent } from "./components/deployment/maintenance-add-status.component";
import { DeploymentAddStatusComponent } from "./components/deployment/deployement-add-status.component";
import { RolesManagementComponent } from "./components/controls/roles-management.component";
import { RoleEditorComponent } from "./components/controls/role-editor.component";
import { importType } from "@angular/compiler/src/output/output_ast";
import { DeployComponent } from "./components/deploy/deploy.component";
import { DeployEditComponent } from "./components/deploy/deploy-edit.component";
import { PosHistoryComponent } from './components/posHistory/history.component';
import { PosHistoryRecordComponent } from './components/posHistory/history-record.component';
import { OldRecordComponent } from './components/posOldRecords/old-records.component';
import { OtherStockListComponent } from './components/otherstock/otherstock.component';
import { OtherStockEditComponent } from './components/otherstock/otherstock-edit.component';
import { SideBarComponent } from './components/sidebar/sidebar.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { MaintenanceEditComponent } from './components/maintenance/maintenance-edit.component';
import { SupportComponent } from './components/support/support.component';
import { SupportEditComponent } from './components/support/support-edit.component';
import { DeployListComponent } from './components/deploy/deploy-list.component';
import { AddStatusComponent } from './components/deploy/add-status.component';
import { SupportListComponent } from './components/support/support-list.component';
import { NotReceivedPosListComponent } from './components/notreceivedpos/not-received-pos.component';
import { DamagePosListComponent } from './components/damagepos/damage-pos.component';
import { AddDamagePosComponent } from './components/maintenance/damage-pos-status.component';
import { AddStatusNotReceivedPosComponent } from './components/notreceivedpos/not-received-add-status.component';
import { AddStatusMaintenancePosComponent } from './components/notreceivedpos/maintenance-add-status.component';
import { AddSatusConfigurationComponent } from './components/configuration/add-configuration-status.component';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        CustomMaterialModule,
        AgGridModule.withComponents([]),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useClass: TranslateLanguageLoader
            }
        }),

        ReactiveFormsModule,
        NgbModule.forRoot(),
        NgxDatatableModule,
        DateRangePickerModule,
        ToastyModule.forRoot(),
        TooltipModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        CarouselModule.forRoot(),
        ModalModule.forRoot(),
        ChartsModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        CustomersComponent,
        ProductsComponent,
        OrdersComponent,
        SettingsComponent,
        UsersManagementComponent, UserInfoComponent, UserPreferencesComponent,
        RolesManagementComponent, RoleEditorComponent,
        InventoryComponent, InventoryEditComponent, InventoryAddComponent, InventoryPosAddComponent, InventoryAddListComponent, InventoryAddEditComponent, SerialAddComponent,
        HardwareSupportComponent, HardwareSupportEditComponent, HwSupportEditComponent,
        HwSupportComponent, HwSupportListComponent, SpareRecordComponent,
        EditSerialNoComponent,
        PosStockComponent, PosStockEditComponent, PosStockHistoryComponent, PosSparesStockComponent,
        AddStatusComponent, MaintenanceAddStatusComponent,
        PosSparesComponent, PosSparesEditComponent,
        ConfigurationsComponent, ConfigurationEditComponent,
        DeploymentRequestComponent, DeploymentListComponent,
        DeployComponent, DeployEditComponent, DeployListComponent,
        BankComponent, BankEditComponent,
        ApplicationComponent, ApplicationEditComponent,
        PosHistoryComponent, PosHistoryRecordComponent,
        OldRecordComponent,
        PosHistoryComponent, OtherStockListComponent, OtherStockEditComponent, SideBarComponent,
        MaintenanceComponent, MaintenanceEditComponent,
        SupportComponent, SupportEditComponent, SupportListComponent, NotReceivedPosListComponent, DamagePosListComponent, AddDamagePosComponent,
        AddStatusNotReceivedPosComponent, AddStatusMaintenancePosComponent, AddSatusConfigurationComponent,
        AboutComponent, DeploymentAddStatusComponent,
        NotFoundComponent,
        NotificationsViewerComponent, SerialInStockComponent, SerialListInStockComponent,
        SearchBoxComponent,
        StatisticsDemoComponent, DashboardStockComponent, TodoDemoComponent, BannerDemoComponent, PosStatusComponent,
        EqualValidator,
        LastElementDirective,
        AutofocusDirective,
        BootstrapTabDirective,
        BootstrapToggleDirective,
        BootstrapSelectDirective,
        BootstrapDatepickerDirective,
        GroupByPipe
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl },
        { provide: ErrorHandler, useClass: AppErrorHandler },
        AlertService,
        ConfigurationService,
        AppTitleService,
        AppTranslationService,
        NotificationService,
        NotificationEndpoint,
        AccountService,
        DashboardService, DashboardEndPoint,
        InventoryService,
        InventoryAddService,
        HardwareSupportService,
        HardwareSupportEndpoint,
        HwSupportService, HwSupportEndpoint,
        SerialNoService,
        SerialEndpoint,
        DeploymentService,
        DeploymentEndpoint,
        DeployService,
        DeployEndpoint,
        PosHistoryService,
        PosHistoryEndpoint,
        PosStockService,
        PosStockEndpoint,
        OtherStockService,
        OtherStockEndpoint,
        BankService,
        BankEndpoint,
        ApplicationService,
        ApplicationEndpoint,
        RequestService,
        RequestTypeEndpoint,
        StatusService,
        StatusEndpoint,
        CheckerService,
        CheckerEndpoint,
        AccountEndpoint,
        InventoryEndpoint,
        InventoryAddEndpoint,
        ConfigurationsService,
        ConfigurationEndpoint,
        ApplicationService,
        ApplicationEndpoint,
        MaintenanceService,
        MaintenanceEndpoint,
        SupportService,
        SupportEndpoint,
        LocalStoreManager,
        EndpointFactory
       
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}




export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
