﻿import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { SupportService } from "../../services/support/support.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { HwSupportService } from "../../services/hwSupport/hwSupport.service";
import { HwSupport } from '../../models/hwSupport/hwSupport.model';
import { HwSupportEdit } from '../../models/hwSupport/hwSupport-edit.model';
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { Subject } from 'rxjs/Subject';
import * as moment from 'moment';

@Component({
    selector: 'hwsupport-management',
    templateUrl: './hwSupport.component.html',
   // styleUrls: ['./hwSupport.component.css']
})

export class HwSupportComponent {
    columnDefs: any[] = [];
    columns: any[] = [];
    rows: HwSupport[] = [];
    rowsCache: HwSupport[] = [];

    //editedSupport: SupportEdit;
    //sourceSupport: SupportEdit;
    editingSupportName: {};
    loadingIndicator: boolean;
    private hwSupportEdit: HwSupportEdit = new HwSupportEdit();
    private hwSupport: HwSupport = new HwSupport();
    allSupport: HwSupport[] = [];

   
    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('phoneTemplate')
    phoneTemplate: TemplateRef<any>;

    @ViewChild('fieldTemplate')
    fieldTemplate: TemplateRef<any>;

    @ViewChild('statusTemplate')
    statusTemplate: TemplateRef<any>;

    @ViewChild('priorityTemplate')
    priorityTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('verificationTemplate')
    verificationTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    //@ViewChild('supportEditor')
    //supportEditor: SupportEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private hw_supportService: HwSupportService) {
    }

    ngOnInit() {

            let gT = (key: string) => this.translationService.getTranslation(key);


            this.columnDefs = [
                {
                    headerName: 'Bank Name', field: 'bankName', width: 150, suppressSizeToFit: true
                },
                
                { headerName: 'Contact Person', field: 'contactPerson', width: 150, suppressSizeToFit: true },
                { headerName: 'Contact No', field: 'contactNo', width: 150, suppressSizeToFit: true },
              
                { headerName: 'Issue', field: 'issue', width: 150, suppressSizeToFit: true },
                { headerName: 'Phone', field: 'phone', width: 100, suppressSizeToFit: true },
                { headerName: 'Field', field: 'field', width: 100, suppressSizeToFit: true },
                { headerName: 'Priority', field: 'priority', width: 100, suppressSizeToFit: true },
                { headerName: 'Remarks', field: 'remarks', width: 100, suppressSizeToFit: true },
                { headerName: 'Resolved By', field: 'resolvedBy', width: 150, suppressSizeToFit: true },
                {
                    headerName: 'Support Date', field: 'supportDate', valueFormatter: (data) => moment(data.value).format('L'), width: 150, suppressSizeToFit: true, filter: 'agDateColumnFilter',
                    filterParams: {
                        comparator: function (filterLocalDateAtMidnight, cellValue) {
                            var dateAsString = cellValue;
                            if (dateAsString == null) return -1;
                            var dateParts = dateAsString.split("/");
                            var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                                return 0
                            }

                            if (cellDate < filterLocalDateAtMidnight) {
                                return -1;
                            }

                            if (cellDate > filterLocalDateAtMidnight) {
                                return 1;
                            }
                        },
                        browserDatePicker: true
                    }
                },
                { headerName: 'Verified By', field: 'verifiedBy', width: 150, suppressSizeToFit: true },
                { headerName: 'Verified Date', field: 'verifiedDate', valueFormatter: (data) => moment(data.value).format('L'), width: 150, suppressSizeToFit: true },


                {
                    headerName: "Actions",
                    //suppressMenu: true,
                    suppressSorting: true,
                    template:
                        `<button type="button" data-action-type="Support" class="btn btn-link btn-xs">
                             Delete
                        </button>`
                }
            ];

    }

    ngAfterViewInit() {

        //this.supportEditor.changesSavedCallback = () => {
        //    this.addNewSupportToList();
        //    this.editorModal.hide();
        //};

        //this.supportEditor.changesCancelledCallback = () => {
        //    this.editedSupport = null;
        //    this.sourceSupport = null;
        //    this.editorModal.hide();
        //};

    }

    //------Testing-------
    newSupport() {
         this.alertService.startLoadingMessage("common common...");
        //this.editingSupportName = null;
        //this.sourceSupport = null;
        //this.editedSupport = this.supportEditor.newSupport(this.allSupport);
        //this.editorModal.show();
    }
    //editSupport(row: SupportEdit) {
    //    this.editingSupportName = { name: row.id };
    //    this.sourceSupport = row;
    //    this.editedSupport = this.supportEditor.editSupport(row, this.allSupport);
    //    this.editorModal.show();
    //}

    loadSupportList(allSupport: HwSupport[], id) {
        //this.isGeneralEditor = true;
        this.hwSupportEdit.id = id;
        this.allSupport = [...allSupport];
        this.hwSupport = this.hwSupportEdit = new HwSupportEdit();
        this.loadSupportData(id);
        return this.hwSupportEdit;
    }


    public onRowClicked(e) {
            if (e.event.target !== undefined) {
                let data = e.data;
                let actionType = e.event.target.getAttribute("data-action-type");

                switch (actionType) {
                    case "Support":
                        return this.onActionDeleteClick(data);

                }
            }
    }

    onActionDeleteClick(row: HwSupportEdit) {
        //alert('delete??');
        this.alertService.showDialog('Are you sure you want to delete this record?', DialogType.confirm, () => this.deleteSupportHelper(row));
    }

    deleteSupportHelper(row: HwSupportEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.hw_supportService.deleteSupport(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }


    loadSupportData(id) {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.hw_supportService.getSupportInfo(-1, -1, id).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(hw_supports: HwSupport[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        hw_supports.forEach((hw_support, index, hw_supports) => {

            (<any>hw_support).index = index + 1;
        });
        this.rowsCache = [...hw_supports];
        this.rows = hw_supports;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }

    
}
