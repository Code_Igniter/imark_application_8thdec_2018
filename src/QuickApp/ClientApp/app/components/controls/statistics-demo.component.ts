﻿
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertService, DialogType, AlertMessage, MessageSeverity } from '../../services/alert.service';
import { Bank } from '../../models/bank/bank.model';
import { BankEdit } from '../../models/bank/bank-edit.model';
import { DashboardService } from '../../services/dashboard/dashboard.service';
require('chart.js');

import { Utilities } from "../../services/utilities";

@Component({
    selector: 'statistics-demo',
    templateUrl: './statistics-demo.component.html',
    styleUrls: ['./statistics-demo.component.css']
})
export class StatisticsDemoComponent implements OnInit, OnDestroy {
    rows: Bank[] = [];
    rowsCache: BankEdit[] = [];
    previousMonth: number;
    currentMonth: number;
    total: number;
    bankName: string;
    bank: Bank = new Bank();

    //To display bank names from Database.
    chartData: Array<any> =
        [{ data: [], label: '' }, { data: [], label: '' }, { data: [], label: '' }, { data: [], label: '' }, { data: [], label: '' }, { data: [], label: '' },
            { data: [], label: '' }, { data: [], label: '' }, { data: [], label: '' }, { data: [], label: '' }, { data: [], label: '' }];
    

    chartLabels: Array<any> = ['GEM','EM','IWL','Maintenance','Previous Month', 'Current Month', 'Pending Deploy', 'Total Deploy', 'Roll Distribution'];
   
    chartOptions: any = {
        responsive: true,
        title: {
            display: false,
            fontSize: 16,
            text: 'Important Stuff'
        }
    };
    chartColors: Array<any> = [
        { // grey
            backgroundColor: '#f38b4a',
            borderColor: '#f38b4a',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#f38b4a',
            pointHoverBackgroundColor: '#f38b4a',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: '#56d798',
            borderColor: '#56d798',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#56d798',
            pointHoverBackgroundColor: '#56d798',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        
        { 
            backgroundColor: '#ff8397',
            borderColor: '#ff8397',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#ff8397',
            pointHoverBackgroundColor: '#ff8397',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // something else
            backgroundColor: '#6970d5',
            borderColor: '#6970d5',
            pointBackgroundColor: 'rgba(128,128,128,1)',
            pointBorderColor: '#6970d5',
            pointHoverBackgroundColor: '#6970d5',
            pointHoverBorderColor: 'rgba(128,128,128,0.8)'
        },
        { // grey
            backgroundColor: 'salmon',
            borderColor: 'salmon',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#salmon',
            pointHoverBackgroundColor: '#salmon',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: 'darkgray',
            borderColor: 'darkgray',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#darkgray',
            pointHoverBackgroundColor: '#darkgray',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        
        {
            backgroundColor: 'coral',
            borderColor: 'coral',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#coral',
            pointHoverBackgroundColor: '#coral',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // something else
            backgroundColor: 'pink',
            borderColor: 'pink',
            pointBackgroundColor: 'rgba(128,128,128,1)',
            pointBorderColor: '#pink',
            pointHoverBackgroundColor: '#pink',
            pointHoverBorderColor: 'rgba(128,128,128,0.8)'
        },
              {
            backgroundColor: 'magneta',
                  borderColor: 'magneta',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#magneta',
            pointHoverBackgroundColor: '#magneta',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // something else
            backgroundColor: 'purple',
            borderColor: 'purple',
            pointBackgroundColor: 'rgba(128,128,128,1)',
            pointBorderColor: '#purple',
            pointHoverBackgroundColor: '#purple',
            pointHoverBorderColor: 'rgba(128,128,128,0.8)'
        }
    ];
    chartLegend: boolean = true;
    chartType: string = 'line';

    timerReference: any;



    constructor(private alertService: AlertService, private dashboardService: DashboardService) {
    }


    ngOnInit() {
        //this.timerReference = setInterval(() => this.randomize(), 5000);
        this.loadBankDeployData();
        //this.chartType = "line";
    }

    ngOnDestroy() {
        clearInterval(this.timerReference);
    }

    loadBankDeployData() {

        this.dashboardService.getBankDeployInfo().subscribe(results => {
            let GEM = results.map(results => results.gem);
            let EM = results.map(results => results.em);
            let IWL = results.map(results => results.iwl);

            let Maintenance = results.map(results => results.maintenanceNumber);
            let previousDeploy = results.map(results => results.previousMonth);
            let currentDeploy = results.map(results => results.currentMonth);
            let pendingDeploy = results.map(results => results.pendingDeployment);
            let totalDeploy = results.map(results => results.total);
            let totalRoll = results.map(results => results.rollNumber);
            let allBank = results.map(results => results.bankName);
            var counter = allBank.length;
            var count = 0;
            let chart=
                [{ data: [], label: '' }];
            this.chartData = Object.assign(chart);
            //this.chartType = 'line';
            for (count; count < counter; count++) {
                chart.splice(0, 0, {
                    data: [GEM[count], EM[count], IWL[count], Maintenance[count], previousDeploy[count], currentDeploy[count],
                    pendingDeploy[count], totalDeploy[count], totalRoll[count]], label: allBank[count]
                })
                //this.chartType = 'line';
                //this.changeChartType('line');
            }

            {//} else {
                //    return false;
                //}
                //var label1, label2, label3, label4, label5, label6, label7, label8 = [];
                //var data1,data2,data3,data4,data5,data6,data7,data8 = [];
                //data1 = [previousDeploy[0], currentDeploy[0], totalDeploy[0], totalDeploy[0]];
                //data2 = [previousDeploy[1], currentDeploy[1], totalDeploy[1], totalDeploy[1]];
                //data3 = [previousDeploy[2], currentDeploy[2], totalDeploy[2], totalDeploy[2]];
                //data4 = [previousDeploy[3], currentDeploy[3], totalDeploy[3], totalDeploy[3]];
                //data5 = [previousDeploy[4], currentDeploy[4], totalDeploy[4], totalDeploy[4]];
                //data6 = [previousDeploy[5], currentDeploy[5], totalDeploy[5], totalDeploy[5]];
                ////data7 = [previousDeploy[6], currentDeploy[6], totalDeploy[6], totalDeploy[6]];
                ////data8 = [previousDeploy[7], currentDeploy[7], totalDeploy[7], totalDeploy[7]];
                ////data8 = [previousDeploy[1], currentDeploy[1], totalDeploy[1], totalDeploy[1]];
                ////data3 = [previousDeploy[2], currentDeploy[2], totalDeploy[2], totalDeploy[2]];
                ////data4 = [previousDeploy[3], currentDeploy[3], totalDeploy[3], totalDeploy[3]];
                ////data5 = [previousDeploy[4], currentDeploy[4], totalDeploy[4], totalDeploy[4]];
                ////data6 = [previousDeploy[5], currentDeploy[5], totalDeploy[5], totalDeploy[5]];
                //label1 = [allBank[0]];
                //label2 = [allBank[1]];
                //label3 = [allBank[2]];
                //label4 = [allBank[3]];
                //label5 = [allBank[4]];
                //label6 = [allBank[5]];
                //label7 = [allBank[6]];
                //label8 = [allBank[7]];
                //    this.chartData = [
                //        { data: data1, label: label1 },
                //        { data: data2, label: label2 },
                //        { data: data3, label: label3 },
                //        { data: data4, label: label4 },
                //        { data: data5, label: label5 },
                //        { data: data6, label: label6 },
                //        { data: data7, label: label7 },
                //        { data: data8, label: label8 }];
                //return this.chartData;
            }
                })
    };

   //this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));



    //onDataLoadSuccessful(banks: Bank[]) {


    //    banks.forEach((bank, index, banks) => {
    //        //Object.assign(this.bank, bank);
    //        this.bank.previousMonth = bank.previousMonth;
    //        this.bank.currentMonth = bank.currentMonth;
    //        this.bank.total = bank.total;
    //        this.bank.bankName = bank.bankName;
    //        this.bank.data = bank.data;
    //        this.bank.label = bank.label;
    //        //var bankDashboard = Object.assign
            
    //        let chartData: Array<any> = [
    //            { data: [this.bank.previousMonth, this.bank.currentMonth, this.bank.total], label: this.bank.bankName }
              
    //        ];
    //        //this.chartData = chartData;
    //        //        let _chartData: Array<any> = new Array(this.chartData.length);
    //        //for (let i = 0; i < this.chartData.length; i++) {
    //        //    _chartData[i] = { data: new Array(this.chartData[i].data.length), label: this.chartData[i].label };

    //        //    //for (let j = 0; j < this.chartData[i].data.length; j++) {
    //        //    //    _chartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
    //        //    //}
    //        //}

    //        //this.chartData = _chartData;
    //        (<any>bank).index = index + 1;
    //        return this.chartData;
    //    });
    //    this.rowsCache = [...banks];
    //    this.rows = banks;
    //    this.chartData = [{ data: banks[this.bank.data, this.bank.label] }];
        
    //}


    //onDataLoadFailed(error: any) {

    //    this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
    //        MessageSeverity.error, error);
    //}


    //randomize(): void {
      
    //    let _chartData: Array<any> = new Array(this.chartData.length);
    //    for (let i = 0; i < this.chartData.length; i++) {
    //        _chartData[i] = { data: new Array(this.chartData[i].data.length), label: this.chartData[i].label };

    //        //for (let j = 0; j < this.chartData[i].data.length; j++) {
    //        //    _chartData[i].data[j] = Math.floor((Math.random() * 1));
    //        //}
    //    }

    //    this.chartData = _chartData;
    //}
   
    changeChartType(type: string) {
        this.chartType = type;
    }

    showMessage(msg: string): void {
        this.alertService.showMessage("Demo", msg, MessageSeverity.info);
    }

    showDialog(msg: string): void {
        this.alertService.showDialog(msg, DialogType.prompt, (val) => this.configure(true, val), () => this.configure(false));
    }

    configure(response: boolean, value?: string) {

        if (response) {

            this.alertService.showStickyMessage("Simulating...", "", MessageSeverity.wait);

            setTimeout(() => {

                this.alertService.resetStickyMessage();
                this.alertService.showMessage("Demo", `Your settings was successfully configured to \"${value}\"`, MessageSeverity.success);
            }, 2000);
        }
        else {
            this.alertService.showMessage("Demo", "Operation cancelled by user", MessageSeverity.default);
        }
    }



    // events
    chartClicked(e: any): void {
        console.log(e);
    }

    chartHovered(e: any): void {
        console.log(e);
    }
}