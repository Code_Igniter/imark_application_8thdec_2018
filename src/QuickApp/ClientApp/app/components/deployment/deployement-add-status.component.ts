﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { DeploymentService } from "../../services/deployment/deployment.service";

import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';


@Component({
    selector: 'deployment-add-status',
    templateUrl: './deployment-add-status.component.html',
    styleUrls: ['./deployment.component.css']
})
export class DeploymentAddStatusComponent implements OnInit {



    private isEditMode = false;
    private isNewDeployment = false;
    private isSaving = false;
    private isEditingSelf = false;
    private isImarkProperty: boolean;
    private showValidationErrors = false;
    private editingDeploymentName: string;
    private uniqueId: string = Utilities.uniqueId();
    private deployment: DeploymentRequest = new DeploymentRequest();

    private allDeployment: DeploymentRequest[] = [];
    private deploymentEdit: DeploymentRequestEdit = new DeploymentRequestEdit();

    public formResetToggle = true;
    private selectedLink: string = "no";
    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope

    @ViewChild('remarksForDeployItem')
    private remarksForDeploy;


    constructor(private alertService: AlertService, private deploymentService: DeploymentService) {
    }
    ngOnInit() {


    }





    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.deploymentEdit = new DeploymentRequestEdit();
            Object.assign(this.deploymentEdit, this.deployment);
        }
        else {
            if (!this.deploymentEdit)
                this.deploymentEdit = new DeploymentRequestEdit();

            this.isEditingSelf = this.deploymentService.updateDeployment(this.deployment) ? this.deploymentEdit.id == this.deployment.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveDeployment() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        if (this.isNewDeployment) {

            this.deploymentService.newDeployment(this.deploymentEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {

            this.deploymentService.updateDeployment(this.deploymentEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(deployment?: DeploymentRequest) {
        this.testIsInventoryChanged(this.deployment, this.deploymentEdit);

        if (deployment)
            Object.assign(this.deploymentEdit, deployment);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.deployment, this.deploymentEdit);
        this.deploymentEdit = new DeploymentRequestEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewDeployment)
                this.alertService.showMessage("Success", `Deployment Request \"${this.deployment.statusName}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Deployment Request \"${this.deployment.statusName}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsInventoryChanged(deployment: DeploymentRequest, editedDeployment: DeploymentRequest) {

    }



    private cancel() {
        if (this.isGeneralEditor)
            this.deploymentEdit = this.deployment = new DeploymentRequestEdit();
        else
            this.deploymentEdit = new DeploymentRequestEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.deploymentEdit = this.deployment = new DeploymentRequestEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newDeployment(allDeployment: DeploymentRequest[]) {
        this.isGeneralEditor = true;
        this.isNewDeployment = true;

        this.allDeployment = [...allDeployment];
        this.editingDeploymentName = null;
        this.deployment = this.deploymentEdit = new DeploymentRequestEdit();
        this.deploymentEdit.statusName = "New";
        this.edit();

        return this.deploymentEdit;
    }

    editDeployment(deployment: DeploymentRequest, allDeployment: DeploymentRequest[]) {
        if (deployment) {
            this.isGeneralEditor = true;
            this.isNewDeployment = false;
            this.editingDeploymentName = deployment.requestName;
            this.deployment = new DeploymentRequest();
            this.deploymentEdit = new DeploymentRequestEdit();
            Object.assign(this.deployment, deployment);
            Object.assign(this.deploymentEdit, deployment);
            this.edit();
            return this.deploymentEdit;
        }
        else {
            return this.newDeployment(allDeployment);
        }
    }


    get canViewAllRoles() {
        return this.deploymentService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.deploymentService.userHasPermission(Permission.assignRolesPermission);
    }
}
