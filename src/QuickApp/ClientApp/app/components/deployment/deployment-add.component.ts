﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { DeploymentService } from "../../services/deployment/deployment.service";
import { BankService } from "../../services/bank/bank.service";
import { InventoryService } from "../../services/inventory.service";
import { InventoryAddService } from "../../services/inventory-add.service";
//import { CheckerService } from "../../services/checker/checker.service";
import { RequestService } from "../../services/requestType/request-type.service";
import { StatusService } from "../../services/status/status.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';
import { Inventory } from '../../models/inventory.model';
import { InventoryEdit } from '../../models/inventory-edit.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { Bank } from '../../models/bank/bank.model';
//import { Checker } from '../../models/checker.model';
import { RequestType } from '../../models/request-type.model';
import { Status } from '../../models/status.model';
import { Configuration } from '../../models/configuration/configuration.model';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl, ReactiveFormsModule } from '@angular/forms'
import { PosStockEdit } from '../../models/posStock/pos-stock-edit.model';
import { PosStock } from "../../models/posStock/pos-stock.model";
import { PosStockService } from '../../services/posStock/pos-stock.service';

@Component({
    selector: 'deployment-add',
    templateUrl: './deployment-add.component.html',
    styleUrls: ['./deployment.component.css']
})
export class DeploymentRequestComponent implements OnInit {
    rows: Bank[] = [];
    rowsBank: Bank[] = [];
    rowsPosStock: Inventory[] = [];
    rowsNewBank: Bank[] = [];
    rows1: RequestType[] = [];
    rows2: Inventory[] = [];
  
    //rows2: Checker[] = [];
    rows3: Status[] = [];
    rowsConfig: Configuration[] = [];
    bankControl = new FormControl('', [Validators.required]);
    districtControl = new FormControl('', [Validators.required]);
    inventoryControl = new FormControl('', [Validators.required]);
    inventoryPartialControl = new FormControl('', [Validators.required]);
    verificationControl = new FormControl('', [Validators.required]);
    verify = [
        { value: 1, text: 'Pending' },
        { value: 2, text: 'Verified' },
        { value: 3, text: 'Rejected' },
    ];
    currencyz = [
        { value: 1, text: 'NPR' },
        { value: 2, text: 'USD' },
    ];
    districts = [
        { value: 1, text: 'Achham' },
        { value: 2, text: 'Arghakhanchi' },
        { value: 3, text: 'Baglung' },
        { value: 4, text: 'Baitadi' },
        { value: 5, text: 'Bajhang' },
        { value: 6, text: 'Bajura' },
        { value: 7, text: 'Banke' },
        { value: 8, text: 'Bara' },
        { value: 9, text: 'Bardiya' },
        { value: 10, text: 'Bhaktapur' },
        { value: 11, text: 'Bhojpur' },
        { value: 12, text: 'Chitawan' },
        { value: 13, text: 'Dadeldhura' },
        { value: 14, text: 'Dailekh' },
        { value: 15, text: 'Dang' },
        { value: 16, text: 'Darchaula' },
        { value: 17, text: 'Dhading' },
        { value: 18, text: 'Dhankuta' },
        { value: 19, text: 'Dhanusa' },
        { value: 20, text: 'Dolakha' },
        { value: 21, text: 'Dolpa' },
        { value: 22, text: 'Doti' },
        { value: 23, text: 'Gorkha' },
        { value: 24, text: 'Gulmi' },
        { value: 25, text: 'Humla' },
        { value: 26, text: 'Ilam' },
        { value: 27, text: 'Jajarkot' },
        { value: 28, text: 'Jhapa' },
        { value: 29, text: 'Jumla' },
        { value: 30, text: 'Kailali' },
        { value: 31, text: 'Kalikot' },
        { value: 32, text: 'Kanchanpur' },
        { value: 33, text: 'Kapilbastu' },
        { value: 34, text: 'Kaski' },
        { value: 35, text: 'Kathmandu' },
        { value: 36, text: 'Kavre' },
        { value: 37, text: 'Khotang' },
        { value: 38, text: 'Lalitpur' },
        { value: 39, text: 'Lamjung' },
        { value: 40, text: 'Mahottari' },
        { value: 41, text: 'Makwanpur' },
        { value: 42, text: 'Manang' },
        { value: 43, text: 'Morang' },
        { value: 44, text: 'Mugu' },
        { value: 45, text: 'Mustang' },
        { value: 46, text: 'Myagdi' },
        { value: 47, text: 'Nawalparasi' },
        { value: 48, text: 'Nuwakot' },
        { value: 49, text: 'Okhaldhunga' },
        { value: 50, text: 'Palpa' },
        { value: 51, text: 'Panchthar' },
        { value: 52, text: 'Parbat' },
        { value: 53, text: 'Parsa' },
        { value: 54, text: 'Pyuthan' },
        { value: 55, text: 'Ramechhap' },
        { value: 56, text: 'Rasuwa' },
        { value: 57, text: 'Rautahat' },
        { value: 58, text: 'Rolpa' },
        { value: 59, text: 'Rukum' },
        { value: 60, text: 'Rupandehi' },
        { value: 61, text: 'Salyan' },
        { value: 62, text: 'Sankhuwasabha' },
        { value: 63, text: 'Saptari' },
        { value: 64, text: 'Sarlahi' },
        { value: 65, text: 'Sindhuli' },
        { value: 66, text: 'Sindhupalchok' },
        { value: 67, text: 'Siraha' },
        { value: 68, text: 'Solukhumbu' },
        { value: 69, text: 'Sunsari' },
        { value: 70, text: 'Surkhet' },
        { value: 71, text: 'Syangja' },
        { value: 72, text: 'Tanahu' },
        { value: 73, text: 'Taplejung' },
        { value: 74, text: 'Terhathum' },
        { value: 75, text: 'Udayapur' },
    ];
    private isPartialRental = false;
    private showRadioButton = false;

    private isEditMode = false;
    private isNewDeployment = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingDeploymentName: string;
    private uniqueId: string = Utilities.uniqueId();
    private deployment: DeploymentRequest = new DeploymentRequest();
    private bank: Bank = new Bank();
    private inventory: Inventory = new Inventory();
    private request: RequestType = new RequestType();
   // private checker: Checker = new Checker();
    private status: Status = new Status();
    private allDeployment: DeploymentRequest[] = [];
    private deploymentEdit: DeploymentRequestEdit = new DeploymentRequestEdit();
    private deploymentReqId: string;

    public formResetToggle = true;
    private selectedLink: string = "no";  
    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('bankNameItem')
    private bankId;

    @ViewChild('merchantItem')
    private merchant;

    @ViewChild('idMerchantItem')
    private idMerchant;

    @ViewChild('outletItem')
    private outlet;

    @ViewChild('districtItem')
    private district;

    @ViewChild('addressItem')
    private address;

    @ViewChild('contactPersonItem')
    private contactPerson;

    @ViewChild('contactNo1Item')
    private contactNo1;

    @ViewChild('contactNo2Item')
    private contactNo2;

    @ViewChild('idTerminalItem')
    private idTerminal;

    @ViewChild('terminalTypeItem')
    private inventoryId;

    @ViewChild('currencyItem')
    private currency;

    @ViewChild('tipAdjustmentItem')
    private tipAdjustment;

    @ViewChild('manualTransactionItem')
    private manualTransaction;

    @ViewChild('refundItem')
    private refund;

    @ViewChild('preAuthorizationItem')
    private preAuthorization;

    @ViewChild('primaryNacNumberItem')
    private primaryNacNumber;

    @ViewChild('secondaryNacNumberItem')
    private secondaryNacNumber;

    @ViewChild('priorityItem')
    private priority;

    @ViewChild('statusNameItem')
    private statusId;

    @ViewChild('remarksItem')
    private remarks;

    @ViewChild('verificationItem')
    private verification;

    @ViewChild('requestTypeItem')
    private requestTypeId;

    constructor(private alertService: AlertService, private deploymentService: DeploymentService, private inventoryAddService: InventoryAddService, private bankService: BankService, private requestService: RequestService, private statusService: StatusService, private posStockService: PosStockService) {
    }
    ngOnInit() {
        if (!this.isGeneralEditor) {
            this.loadCurrentDeploymentData();
            
        }

        this.loadCurrentBankData();
        this.loadCurrentRequestData();
       // this.loadCurrentCheckerData();
        this.loadCurrentStatusData();
        this.loadData();
        //this.getBankId("id");
    }
    private loadCurrentDeploymentData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.deploymentService.getDeployment().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
            }
         

    }
   
    private onCurrentUserDataLoadSuccessful(deployment: DeploymentRequest) {
        this.alertService.stopLoadingMessage();
        this.deployment = deployment;

    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.deployment = new DeploymentRequest();
    }

    private loadCurrentBankData() {
        this.bankService.getRentalBankInfo().subscribe(results => this.onBankDataLoadSuccessful(results), error => this.onBankDataLoadFailed(error));
    }


    private onBankDataLoadSuccessful(banks: Bank[]) {
        banks.forEach((bank, index, banks) => {
            (<any>bank).index = index + 1;
        });
        this.rows = banks;   
    }

    private onBankDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.bank = new Bank();
    }

    public getBankId(bankName) {
        this.deploymentService.getBankIdInfo(bankName,this.deploymentReqId, -1, -1).subscribe(results => this.onBankIdLoadSuccessful(results), error => this.onBankIdLoadFailed(error));
    }


    private onBankIdLoadSuccessful(banks: Inventory[]) {
        banks.forEach((bank, index, banks) => {
            if (bank.bankStatus == "Partial Rental" || bank.bankStatus == "Both")
                this.isPartialRental = true;
            this.showRadioButton = true;
            if ((bank.bankStatus == "Partial Rental" || bank.bankStatus == "Both") && bank.deploymentRequestId!=null)
                this.isPartialRental = true;
               this.showRadioButton = true;
            if (bank.bankStatus == "Rental")
                this.isPartialRental = false;
            this.showRadioButton = false;
            (<any>bank).index = index + 1;
        });
        this.rowsPosStock = banks;
    }

    private onBankIdLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

    }

    setradio(e: string): void {
        if (e == "yes") {
            this.selectedLink = e;
            this.isPartialRental = false;
            this.showRadioButton = true;
            this.deploymentEdit.isImarkProperty = true;
            this.deployment.isImarkProperty = true;
        }
        else {
            this.selectedLink = e;
            this.isPartialRental = true;
            this.showRadioButton = true;
            this.deploymentEdit.isImarkProperty = false;
            this.deployment.isImarkProperty = false;
        }
    }

    isSelected(name: string): boolean {

        if (!this.selectedLink) { 
           
            return false;
        }
     
        return (this.selectedLink === name); 
    }   


    loadData() {
        //this.inventory.hasSerialNo = true;
        this.inventoryAddService.getInventoryList().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }
    onDataLoadSuccessful(inventorys: Inventory[]) {
        inventorys.forEach((inventory, index, inventorys) => {
            (<any>inventory).index = index + 1;
        });
       
        this.rows2 = inventorys;
    }
    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        //this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    private loadCurrentRequestData() {
        this.requestService.getRequestTypeInfo().subscribe(results => this.onRequestDataLoadSuccessful(results), error => this.onRequestDataLoadFailed(error));
    }


    private onRequestDataLoadSuccessful(requests: RequestType[]) {
        requests.forEach((request, index, requests) => {
            (<any>request).index = index + 1;
        });
        this.rows1 = requests;
    }

    private onRequestDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.request = new RequestType();
    }
  
    private loadCurrentStatusData() {
        this.statusService.getStatusInfo().subscribe(results => this.onStatusDataLoadSuccessful(results), error => this.onStatusDataLoadFailed(error));
    }


    private onStatusDataLoadSuccessful(statuses: Status[]) {
        statuses.forEach((status, index, statuses) => {
            (<any>status).index = index + 1;
        });
        this.rows3 = statuses;
    }

    private onStatusDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.status = new Status();
    }
    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }



    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.deploymentEdit = new DeploymentRequestEdit();
            Object.assign(this.deploymentEdit, this.deployment);
        }
        else {
            if (!this.deploymentEdit)
                this.deploymentEdit = new DeploymentRequestEdit();

            this.isEditingSelf = this.deploymentService.updateDeployment(this.deployment) ? this.deploymentEdit.id == this.deployment.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveDeployment() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        if (this.isNewDeployment) {
            //if (this.isPartialRental==false) {    
            //    this.deploymentEdit.rentalInventoryAdd = "RentalInventory";
            //}
            //if (this.isPartialRental==true) {
            //    this.deploymentEdit.rentalInventoryAdd = "PartialRentalInventory";
            //}
            this.deploymentService.newDeployment(this.deploymentEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            //if (this.isPartialRental == false) {
            //    this.deploymentEdit.rentalInventoryAdd = "RentalInventory";
            //}
            //if (this.isPartialRental == true) {
            //    this.deploymentEdit.rentalInventoryAdd = "PartialRentalInventory";
            //}
            this.deploymentService.updateDeployment(this.deploymentEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(deployment?: DeploymentRequest) {
        this.testIsInventoryChanged(this.deployment, this.deploymentEdit);

        if (deployment)
            Object.assign(this.deploymentEdit, deployment);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.deployment, this.deploymentEdit);
        this.deploymentEdit = new DeploymentRequestEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewDeployment)
                this.alertService.showMessage("Success", `Deployment Request \"${this.deployment.statusName}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Deployment Request \"${this.deployment.statusName}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsInventoryChanged(deployment: DeploymentRequest, editedDeployment: DeploymentRequest) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.deploymentEdit = this.deployment = new DeploymentRequestEdit();
        else
            this.deploymentEdit = new DeploymentRequestEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.deploymentEdit = this.deployment = new DeploymentRequestEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newDeployment(allDeployment: DeploymentRequest[]) {
        this.isGeneralEditor = true;
        this.isNewDeployment = true;

        this.allDeployment = [...allDeployment];
        this.editingDeploymentName = null;
        this.deployment = this.deploymentEdit = new DeploymentRequestEdit();
        this.deploymentEdit.statusName = "New";
        this.edit();

        return this.deploymentEdit;
    }

    editDeployment(deployment: DeploymentRequest, allDeployment: DeploymentRequest[]) {
        if (deployment) {
            this.isGeneralEditor = true;
            this.isNewDeployment = false;
            this.deploymentReqId = this.deployment.id;
            this.editingDeploymentName = deployment.requestName;
            this.deployment = new DeploymentRequest();
            this.deploymentEdit = new DeploymentRequestEdit();
            Object.assign(this.deployment, deployment);
            Object.assign(this.deploymentEdit, deployment);
            this.edit();
           
            this.getBankId(this.deployment.bankId);
            return this.deploymentEdit;
        }
        else {
            return this.newDeployment(allDeployment);
        }
    }


    get canViewAllRoles() {
        return this.deploymentService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.deploymentService.userHasPermission(Permission.assignRolesPermission);
    }
}
