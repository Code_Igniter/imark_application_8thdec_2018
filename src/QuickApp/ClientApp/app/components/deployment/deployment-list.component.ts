﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { DeploymentService } from "../../services/deployment/deployment.service";
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { DeployService } from "../../services/deploy/deploy.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { PosStockService } from "../../services/posStock/pos-stock.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';
import { DeploymentRequestComponent } from "./deployment-add.component";
import { ConfigurationEdit } from '../../models/configuration/configuration-edit.model';
import { Configuration } from '../../models/configuration/configuration.model';
import { ConfigurationEditComponent } from '../configuration/configuration-edit.component';
import { DeploymentAddStatusComponent } from './deployement-add-status.component';
import { Deploy } from '../../models/deploy/deploy.model';
import { DeployEdit } from '../../models/deploy/deploy-edit.model';
import { SerialNo } from '../../models/serial-no.model';
import { SerialNoEdit } from '../../models/serial-no-edit.model';
import { DeployEditComponent } from '../deploy/deploy-edit.component';
import { PosHistory } from '../../models/posHistory/pos-history.model';
import { PosStock } from '../../models/posStock/pos-stock.model';
import { PosStockEdit } from '../../models/posStock/pos-stock-edit.model';
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import * as moment from 'moment';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { MaintenanceAddStatusComponent } from "./maintenance-add-status.component";
@Component({
    selector: 'deployment-list',
    templateUrl: './deployment-list.component.html',
    styleUrls: ['./deployment.component.css']
})
export class DeploymentListComponent implements OnInit, AfterViewInit {
    private gridApi;
    private gridColumnApi;
    columnDefs: any[] = [];
    columnDefs1: any[] = [];
    columnDefs2: any[] = [];
    columnDefs3: any[] = [];
    columns: any[] = [];
    columnsHistory: any[] = [];
    columnsHistory1: any[] = [];
    editedMaintenance: MaintenanceEdit;
    rows: DeploymentRequest[] = [];
    rowsCache: DeploymentRequest[] = [];
    public maintenanceEdit: MaintenanceEdit = new MaintenanceEdit();
    rowsHistoryCache: PosHistory[] = [];
    rowsHistory: PosHistory[] = [];
    rowsHistoryCache1: PosHistory[] = [];
    rowsHistory1: PosHistory[] = [];
    rowsPartialRentalCache: DeploymentRequest[] = [];
    rowsPartialRental: DeploymentRequest[] = [];
    editedConfiguration: ConfigurationEdit;
    sourceConfiguration: ConfigurationEdit;
    editedDeploy: DeployEdit;
    sourceDeploy: DeployEdit;
    editedDeployment: DeploymentRequestEdit;
    sourceDeployment: DeploymentRequestEdit;
    editedStatusDeployment: DeploymentRequestEdit;
    sourceStatusDeployment: DeploymentRequestEdit;
    editedSerial: SerialNoEdit;
    serial: SerialNo = new SerialNo();
    editingDeploymentName: {};
    editingConfigurationName: {};
    editingDeployName: {};
    loadingIndicator: boolean;
    private posHistory: DeploymentRequest = new DeploymentRequest();
    deploymentRequest: DeploymentRequest = new DeploymentRequest();
    allDeployment: DeploymentRequest[] = [];
    allConfiguration: ConfigurationEdit[] = [];
    allDeploy: DeployEdit[] = [];
    allSerial: SerialNoEdit[] = [];
    newDeploymentFlag = false;
    @Input()
    isGeneralEditor = false;

    @ViewChild('maintenanceStatusModal')
    maintenanceStatusModal: ModalDirective;

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('indexTemplate1')
    indexTemplate1: TemplateRef<any>;

    @ViewChild('indexTemplate2')
    indexTemplate2: TemplateRef<any>;

    @ViewChild('merchantItemTemplate')
    merchantItemTemplate: TemplateRef<any>;


    @ViewChild('merchantItemTemplate1')
    merchantItemTemplate1: TemplateRef<any>;

    @ViewChild('merchantItemTemplate2')
    merchantItemTemplate2: TemplateRef<any>;


    @ViewChild('outletItemTemplate')
    outletItemTemplate: TemplateRef<any>;

    @ViewChild('bankItemTemplate')
    bankItemTemplate: TemplateRef<any>;

    @ViewChild('bankItemTemplate1')
    bankItemTemplate1: TemplateRef<any>;

    @ViewChild('bankItemTemplate2')
    bankItemTemplate2: TemplateRef<any>;

    @ViewChild('statusItemTemplate')
    statusItemTemplate: TemplateRef<any>;

    @ViewChild('verificationItemTemplate')
    verificationItemTemplate: TemplateRef<any>;

    @ViewChild('requestTypeItemTemplate')
    requestTypeItemTemplate: TemplateRef<any>;

    @ViewChild('idMerchantTemplate')
    idMerchantTemplate: TemplateRef<any>;

    @ViewChild('idMerchantTemplate1')
    idMerchantTemplate1: TemplateRef<any>;

    @ViewChild('idMerchantTemplate2')
    idMerchantTemplate2: TemplateRef<any>;

    @ViewChild('idTerminalTemplate')
    idTerminalTemplate: TemplateRef<any>;

    @ViewChild('idTerminalTemplate1')
    idTerminalTemplate1: TemplateRef<any>;

    @ViewChild('idTerminalTemplate2')
    idTerminalTemplate2: TemplateRef<any>;


    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('selectTemplate')
    selectTemplate: TemplateRef<any>;

    @ViewChild('retrieveTemplate')
    retrieveTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('configModal')
    configModal: ModalDirective;

    @ViewChild('deployModal')
    deployModal: ModalDirective;

    @ViewChild('reconfigureModal')
    reconfigureModal: ModalDirective;

    @ViewChild('reconfigurePartialRentalModal')
    reconfigurePartialRentalModal: ModalDirective;

    @ViewChild('retrieveModal')
    retrieveModal: ModalDirective;

    @ViewChild('addStatusModal')
    addStatusModal: ModalDirective;

    @ViewChild('deploymentEditor')
    deploymentEditor: DeploymentRequestComponent;

    @ViewChild('configurationEditor')
    configurationEditor: ConfigurationEditComponent;

    @ViewChild('deployEditor')
    deployEditor: DeployEditComponent;


    @ViewChild('addStatusEditor')
    addStatusEditor: DeploymentAddStatusComponent;

    @ViewChild('merchantTemplate')
    merchantTemplate: TemplateRef<any>;

    @ViewChild('merchantTemplate1')
    merchantTemplate1: TemplateRef<any>;

    @ViewChild('merchantTemplate2')
    merchantTemplate2: TemplateRef<any>;

    @ViewChild('inventoryTemplate1')
    inventoryTemplate1: TemplateRef<any>;

    @ViewChild('inventoryTemplate2')
    inventoryTemplate2: TemplateRef<any>;

    @ViewChild('maintenanceStatusEditor')
    maintenanceStatusEditor: MaintenanceAddStatusComponent;

    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService,
        private deploymentService: DeploymentService, private configurationService: ConfigurationsService,
        private deployService: DeployService, private posHistoryService: PosHistoryService, private posStockService: PosStockService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columnDefs = [
            { headerName: 'Bank1 Name', field: 'bankName', width: 180, suppressSizeToFit: true},
            { headerName: 'Merchant Name', field: 'merchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 130, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 130, suppressSizeToFit: true },
            { headerName: 'Contact Person', field: 'contactPerson', width: 130, suppressSizeToFit: true},
            { headerName: 'Contact No', field: 'contactNo1', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            //{ headerName: 'Application Version', field: 'applicationVersion', width: 180, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            //{ headerName: 'Previous S.No', field: 'previousSerial', width: 180, suppressSizeToFit: true, cellRenderer: this.previousSerial },
           
            //{ headerName: 'Configuration Status', field: 'configStatus', width: 180, suppressSizeToFit: true,cellRenderer: this.configStatus },
            { headerName: 'Request Remark', field: 'remarks', width: 150, suppressSizeToFit: true },
            { headerName: 'Current Status', field: 'remarksForDeploy', width: 150, suppressSizeToFit: true },
            { headerName: 'Request Type', field: 'requestName', width: 150, suppressSizeToFit: true },
            { headerName: 'Created By', field: 'createdBy', width: 150, suppressSizeToFit: true },
           
          
        ];

        //Grid Data for Re-Configure PopUp page//
        this.columnDefs1 = [
            { headerName: 'Bank Name', field: 'bankName', width: 200, suppressSizeToFit: true },
            { headerName: 'Merchant', field: 'merchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'Outlet', field: 'outlet', width: 150, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 150, suppressSizeToFit: true },
            //{ headerName: 'Terminal Type', field: '', width: 250, suppressSizeToFit: true, enableTooltip: true, cellRenderer: this.item },
            //{ headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true, enableTooltip: true },
            { headerName: 'Termianl Type', field: 'item', width: 200, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },

            {
                headerName: "Actions",
                suppressMenu: true,
                suppressSorting: true,
                template:
                    `<button type="button" data-action-type="Reconfigure" class="btn btn-link btn-xs">
              Select
             </button>`
            }
        ];

        //Grid Data for Retrieve PopUp page//
        this.columnDefs2 = [
            { headerName: 'Bank Name', field: 'bankName', width: 200, suppressSizeToFit: true },
            { headerName: 'Merchant', field: 'merchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'Outlet', field: 'outlet', width: 150, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 150, suppressSizeToFit: true },
            //{ headerName: 'Terminal Type', field: '', width: 250, suppressSizeToFit: true, enableTooltip: true, cellRenderer: this.item },
            //{ headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true, enableTooltip: true },
            { headerName: 'Termianl Type', field: 'item', width: 200, suppressSizeToFit: true},
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },


            {
                headerName: "Actions",
                suppressMenu: true,
                suppressSorting: true,
                template:
                    `<button type="button" data-action-type="Retrieve" class="btn btn-link btn-xs">
              Select
             </button>`
            }
        ];

        this.columnDefs3 = [
            { headerName: 'Bank4 Name', field: 'bankName', width: 200, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 200, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialNumber', width: 200, suppressSizeToFit: true },
            { headerName: 'Location', field: 'locationForPartialRental', width: 150, suppressSizeToFit: true},
            { headerName: 'Remarks', field: 'remarksForPartialRental', width: 250, suppressSizeToFit: true },
            { headerName: 'Sold Date', field: 'soldDate', valueFormatter: (data) => moment(data.value).format('L'), width: 250, suppressSizeToFit: true },
           
        ];

        if (this.canManageUsers) {
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: 'requestName',
                suppressSorting: true,
                width: 300,

                cellRenderer: this.actionsTwo,

            });
        }
        else if (this.canCreateUsers) {
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: 'requestName',
                suppressSorting: true,
                width: 250,

                cellRenderer: this.actionsOne,

            });
        }

        this.addNewDeploymentToList();
       
    }
    public actionsOne(params) {

        //if (params.data.statusName == "Requested") {

        //        return (`<button type= "button"  data-action-type="Verify"  class="btn btn-link btn-xs"  
        //style = "background-color:maroon;color:white;text-decoration:none">Deployment Request Verify</button>`);

        //}
        if (params.data.statusName == "RequestVerified") {
            return (`<button type= "button"  data-action-type="Add"  class= "btn btn-link btn-xs"
                    style = "background-color:maroon;color:white;text-decoration:none" > Add Status</button> <button type= "button"  data-action-type="Configuration"  class="btn btn-link btn-xs"
                    style = "background-color:maroon;color:white;text-decoration:none" > Configuration </button>`);
        }

        //else if (params.data.statusName == "Configured") {
        //    if (this.canManageUsers)
        //    return (`<button type= "button"  data-action-type="Verify1"  class="btn btn-link btn-xs"   
        //        style = "background-color:maroon;color:white;text-decoration:none" > Configuration Verify < /button>`);
        //}
        else if (params.data.statusName == "Retrieve") {
            return (`<button type= "button"  data-action-type="Maintenance"  class="btn btn-link btn-xs"   
                style = "background-color:maroon;color:white;text-decoration:none">Send to Maintenance</button>
                <button type = "button"  data-action-type="Reuse"  class="btn btn-link btn-xs"
                style = "background-color:maroon;color:white;text-decoration:none" > Send to Stock </button>`);
        }

        else {
            return null;
        }

    };

    public actionsTwo(params) {

        if (params.data.statusName == "Requested") {

            return (`<button type= "button"  data-action-type="Add"  class= "btn btn-link btn-xs"
                    style = "background-color:maroon;color:white;text-decoration:none" > Add Status</button> <button type= "button"  data-action-type="Verify"  class="btn btn-link btn-xs"  
            style = "background-color:maroon;color:white;text-decoration:none">Deployment Request Verify</button>`);

        }
        else if (params.data.statusName == "RequestVerified") {
            return (`<button type= "button"  data-action-type="Add"  class= "btn btn-link btn-xs"
                    style = "background-color:maroon;color:white;text-decoration:none" > Add Status</button>  <button type= "button"  data-action-type="Configuration"  class="btn btn-link btn-xs"
                    style = "background-color:maroon;color:white;text-decoration:none" > Configuration </button>`);
        }

        else if (params.data.statusName == "Configured") {
            return (`<button type= "button"  data-action-type="Add"  class= "btn btn-link btn-xs"
                    style = "background-color:maroon;color:white;text-decoration:none" > Add Status</button>  <button type= "button"  data-action-type="Verify1"  class="btn btn-link btn-xs"   
                    style = "background-color:maroon;color:white;text-decoration:none" > Configuration Verify </button>`);
        }
        else if (params.data.statusName == "Retrieve") {
            return (`<button type= "button"  data-action-type="Maintenance"  class="btn btn-link btn-xs"   
                style = "background-color:maroon;color:white;text-decoration:none">Send to Maintenance</button>
                <button type = "button"  data-action-type="Reuse"  class="btn btn-link btn-xs"
                style = "background-color:maroon;color:white;text-decoration:none" > Send to Stock </button>
                <button type = "button"  data-action-type="Undo"  class="btn btn-link btn-xs"
                style = "background-color:maroon;color:white;text-decoration:none" > UNDO </button>`);
        }

        else {
            return null;
        }

    };

    public configStatus(params) {
        if (params.data.configStatus == null) {
            return(`<span >--------</span>`)
        }
        {
            return (params.data.configStatus);
        }
    }
    public previousSerial(params) {
        if (params.data.previousSerial == 'notreceived') {
            return (`<span style="font-size:12px;background-color:maroon;color:white">NOT RECEIVED</span>`)
        }
        else if (params.data.previousSerial == 'received') {
            return (`<span style="font-size:12px">RECEIVED</span>`)
        }
        else{;
            return (`<span style="font-size:12px">USING SAME SERIAL NO</span>`);
        }

    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
      
            let actionType = e.event.target.getAttribute("data-action-type");
           
            switch (actionType) {
                case "Add":
                    return this.onAdd(data);
                case "Verify":
                    return this.onVerify(data);
                case "Detail":
                    return this.onDetail(data);
                case "Configuration":
                    return this.onConfiguration(data);
                case "Verify1":
                    return this.onVerify1(data);
                case "Maintenance":
                    return this.onMaintenance(data);
                case "Reuse":
                    return this.onReuse(data);
                case "Undo":
                    return this.onUndo(data);
            }
        }
    }
    public onAdd(row: DeploymentRequestEdit) {

        this.addStatusDeployment(row);

    }
    public onVerify(row: DeploymentRequestEdit) {
 
            this.editDeployment(row);
        
    }
    public onDetail(row: DeploymentRequestEdit) {

            this.editDeployment(row);

    }
    public onConfiguration(row: DeploymentRequestEdit) {
        this.loadConfiguration(row.id);

    }
    public onVerify1(row: DeploymentRequestEdit) {

            this.loadConfiguration(row.id);

    }

    public onMaintenance(row: DeploymentRequestEdit) {

            this.maintenanceRetrieve(row);

    }
    public onReuse(row: DeploymentRequestEdit) {

            this.updateRetrieve(row);
 
    }
    public onUndo(row: DeploymentRequestEdit) {

        this.undoRetrieve(row);

    }
    public onRowSelectReconfigure(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "Reconfigure":
                    return this.ReconfigureSelect(data);

            }
        }
    }
    public onRowSelectReconfigurePartial(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "ReConfigurePartial":
                    return this.ReconfigurePartialSelect(data);
                case "DeployPartial":
                    return this.DeployPartialSelect(data);

            }
        }
    }


    ///when Retrieve button is Clicked in Pos Master.
    public onRowSelectRetrieve(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "Retrieve":
                    return this.RetrieveSelect(data);

            }
        }
    }



    private ReconfigureSelect(row: PosHistory) {
        this.addHistory(row);
    }
    private ReconfigurePartialSelect(row: DeploymentRequest) {
        this.addReconfigPartialRental(row);
     
    }
    private DeployPartialSelect(row: DeploymentRequest) {
        this.SaveDeployPartialRental(row);

    }
    private SaveDeployPartialRental(row: DeploymentRequest) {
        this.deploymentService.newPartialRentalDeploy(this.deploymentRequest).subscribe(user => this.saveSuccessHelper2(user), error => this.saveFailedHelper2(error));

    }
    private saveSuccessHelper2(deploy?: DeploymentRequestEdit) {
    }


    private saveFailedHelper2(error: any) {

    }
    private RetrieveSelect(row: PosHistory) {
        this.addRetrieve(row);
    }

    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.deploymentService.getDeploymentInfo().subscribe(data => {
            params.api.setRowData(data);
        });

    }
   
    ngAfterViewInit() {

        this.deploymentEditor.changesSavedCallback = () => {
            this.addNewDeploymentToList();
            this.editorModal.hide();
        };
        this.deploymentEditor.changesCancelledCallback = () => {
            this.editedDeployment = null;
            this.sourceDeployment = null;
            this.editorModal.hide();
        };
        this.configurationEditor.changesConfigSavedCallback = () => {
            this.addNewDeploymentToList();
            this.configModal.hide();
        }
        this.configurationEditor.changesCancelledCallback = () => {
            this.editedConfiguration = null;
            this.sourceConfiguration = null;
            this.configModal.hide();
        }
        this.addStatusEditor.changesSavedCallback = () => {
            this.addNewDeploymentToList();
            this.addStatusModal.hide();
        }
        this.addStatusEditor.changesCancelledCallback = () => {
            this.editedStatusDeployment = null;
            this.sourceStatusDeployment = null;
            this.addStatusModal.hide();
        }
        this.maintenanceStatusEditor.changesSavedCallback = () => {
            this.addNewDeploymentToList();
            this.maintenanceStatusModal.hide();
        }
        this.maintenanceStatusEditor.changesCancelledCallback = () => {
            this.editedStatusDeployment = null;
            this.sourceStatusDeployment = null;
            this.maintenanceStatusModal.hide();
        }
        //this.deployEditor.changesSavedCallback = () => {
        //    this.addNewDeploymentToList();
        //    this.deployModal.hide();
        //}
        //this.deployEditor.changesCancelledCallback = () => {
        //    this.editedDeploy = null;
        //    this.sourceDeploy = null;
        //    this.deployModal.hide();
        //} 
      
    }


    addNewDeploymentToList() {
        if (this.sourceDeployment) {
            Object.assign(this.sourceDeployment, this.editedDeployment);

            let sourceIndex = this.rowsCache.indexOf(this.sourceDeployment, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceDeployment, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedDeployment = null;
            this.sourceDeployment = null;
        }
        else {
            let deployment = new DeploymentRequest();
            Object.assign(deployment, this.editedDeployment);
            this.editedDeployment = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>DeploymentRequest).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, deployment);
            this.rows.splice(0, 0, deployment);
        }
        this.loadDeploymentData();
        this.loadPosHistory();
        
    }


    loadDeploymentData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.deploymentService.getDeploymentInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));     
    }
    

    onDataLoadSuccessful(deployments: DeploymentRequest[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        deployments.forEach((deployment, index, deployments) => {
            (<any>deployment).index = index + 1;
        });
        this.rowsCache = [...deployments];
        this.rows = deployments;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.merchant, r.idMerchant, r.idTerminal, r.serialNumber, r.serialKey, r.address, r.contactNo1, r.contactPerson, r.item, r.statusName, r.requestName, r.configStatus, r.remarks));
    }
    onSearchChanged1(value: string) {
        this.rowsHistory = this.rowsHistoryCache.filter(r => Utilities.searchArray(value, false, r.merchant, r.bankName, r.serialKey, r.serialNumber, r.idMerchant, r.idTerminal));
    }
    onSearchChanged2(value: string) {
        this.rowsHistory1 = this.rowsHistoryCache1.filter(r => Utilities.searchArray(value, false, r.merchant, r.bankName, r.serialKey, r.serialNumber, r.idMerchant, r.idTerminal));
    }
    onSearchChanged3(value: string) {
        this.rowsPartialRental = this.rowsPartialRentalCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.remarksForDeploy, r.item, r.serialNumber));
    }

    onEditorModalHidden() {
        this.editingDeploymentName = null;
        this.deploymentEditor.resetForm(true);
    }
    onConfigModalHidden() {
        this.editingConfigurationName = null;
        this.configurationEditor.resetForm(true);
    }
    onDeployModalHidden() {
        this.editingDeployName = null;
        this.deployEditor.resetForm(true);
    }
    onAddStatusModalHidden() {

        this.addStatusEditor.resetForm(true);
    }
    onreconfigureModalHidden() {
        //this.editingDeployName = null;
        //this.reconfigureModal.resetForm(true);
    }
    onretrieveModalHidden() {

    }
    onPartialRentalModalHidden() {

    }
    newDeployment() {
        this.editingDeploymentName = null;
        this.sourceDeployment = null;
        this.editedDeployment = this.deploymentEditor.newDeployment(this.allDeployment);
        this.editorModal.show();
        this.newDeploymentFlag = true;
    }


    editDeployment(row: DeploymentRequestEdit) {
        this.editingDeploymentName = { name: row.requestName };
        this.sourceDeployment = row;
        this.editedDeployment = this.deploymentEditor.editDeployment(row, this.allDeployment);
        this.editorModal.show();
    }
    addStatusDeployment(row: DeploymentRequestEdit) {
        this.editingDeploymentName = { name: row.requestName };
        this.sourceStatusDeployment = row;
        this.editedStatusDeployment = this.addStatusEditor.editDeployment(row, this.allDeployment);
        this.addStatusModal.show();
    }
    //loadSerialData(id) {
    //    this.editedSupport = this.supportListEditor.loadSupportList(this.allSupport, id);
    //}
    loadConfiguration(id) {
        this.alertService.startLoadingMessage();

        this.loadingIndicator = true;
        this.editingConfigurationName = { name: this.deploymentRequest.requestName };
        this.configurationService.getConfiguration(id).subscribe(results => this.onConfigLoadSuccessful(results), error => this.onConfigLoadFailed(error));
        this.posHistoryService.getPosHistory(id).subscribe(results => this.onLoadSuccessful(results), error => this.onLoadFailed(error));
        this.editedSerial = this.configurationEditor.loadSerialList(this.allSerial, id);
    }
    onConfigLoadSuccessful(configuration: Configuration) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
       
        this.editConfiguration(configuration);
       
    }


    onConfigLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    onLoadSuccessful(posHistory: DeploymentRequest) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.posHistory = posHistory;
        //this.editConfiguration(configuration);

    }


    onLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    editConfiguration(configuration: ConfigurationEdit) {
        this.editingConfigurationName = { name: configuration.requestName };
        this.sourceConfiguration = configuration;
        this.editedConfiguration = this.configurationEditor.editConfiguration(configuration, this.allConfiguration);
        this.configModal.show();
    }

    //loadDeploy(id) {
    //    this.alertService.startLoadingMessage();
    //    this.loadingIndicator = true;
    //    this.deployService.getDeploy(id).subscribe(results => this.onDeployLoadSuccessful(results), error => this.onDeployLoadFailed(error));
    //}
    //onDeployLoadSuccessful(deploy: Deploy) {
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;
    //    this.editDeploy(deploy);
    //}
    //onDeployLoadFailed(error: any) {
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;

    //    this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
    //        MessageSeverity.error, error);
    //}
    editDeploy(deploy: DeployEdit) {
        this.editingDeployName = { name: deploy.receivedBy };
        this.sourceDeploy = deploy;
        this.editedDeploy = this.deployEditor.editDeploy(deploy, this.allDeploy);
        this.deployModal.show();
    }
    reconfigure() {
        this.loadPosHistory();
        //this.editingDeployName = { name: deploy.receivedBy };
        //this.sourceDeploy = deploy;
        //this.editedDeploy = this.deployEditor.editDeploy(deploy, this.allDeploy);
        this.reconfigureModal.show();
    }
    reconfigurePartialRental() {
        this.loadPartialRental();
        //this.editingDeployName = { name: deploy.receivedBy };
        //this.sourceDeploy = deploy;
        //this.editedDeploy = this.deployEditor.editDeploy(deploy, this.allDeploy);
        this.reconfigurePartialRentalModal.show();
    }

    loadPosHistory() {
        this.posHistoryService.getPosHistoryInfo().subscribe(results => this.onPosHistoryLoadSuccessful(results), error => this.onPosHistoryLoadFailed(error));
    }
    private onPosHistoryLoadSuccessful(posHistorys: PosHistory[]) {
        posHistorys.forEach((pos, index, posHistorys) => {
            (<any>pos).index = index + 1;
        });
        this.rowsHistoryCache = [...posHistorys];
        this.rowsHistory = posHistorys;
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
       
       
    }

    private onPosHistoryLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve Pos History from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

       
    }
   

    loadPosHistory1() {
        this.posHistoryService.getPosHistoryInfo().subscribe(results => this.onPosHistoryLoadSuccessful1(results), error => this.onPosHistoryLoadFailed1(error));
    }
    private onPosHistoryLoadSuccessful1(posHistorys: PosHistory[]) {
        posHistorys.forEach((pos, index, posHistorys) => {
            (<any>pos).index = index + 1;
        });
        this.rowsHistoryCache1 = [...posHistorys];
        this.rowsHistory1 = posHistorys;
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
    }

    private onPosHistoryLoadFailed1(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve Pos History from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

    }

    loadPartialRental() {
        this.deploymentService.getPartialRental().subscribe(results => this.onPartialRentalLoadSuccessful(results), error => this.onPartialRentalLoadFailed(error));
    }
    private onPartialRentalLoadSuccessful(posStocks: DeploymentRequest[]) {
        posStocks.forEach((pos, index, posStocks) => {
            (<any>pos).index = index + 1;
        });
        this.rowsPartialRentalCache = [...posStocks];
        this.rowsPartialRental = posStocks;
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;       
    }

    private onPartialRentalLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve Pos History from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        // this.inventoryAdd = new InventoryAdd();
    }
    addReconfigPartialRental(row: DeploymentRequest) {
        this.alertService.showDialog('Are you sure you want to Reconfig Partial Rental \"' + row.serialNumber + '\"?', DialogType.confirm, () => this.partialRentalHelper(row));
    }

    partialRentalHelper(row: DeploymentRequest) {
        this.alertService.startLoadingMessage("Requesting...");
        this.loadingIndicator = true;

        this.deploymentService.updateDeployment(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsPartialRentalCache = this.rowsPartialRentalCache.filter(item => item !== row)
                this.rowsPartialRental = this.rowsPartialRental.filter(item => item !== row)
                this.loadDeploymentData();
                this.addNewDeploymentToList();
            },
                error => {
                    this.alertService.stopLoadingMessage();
                    this.loadingIndicator = false;

                    this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                        MessageSeverity.error, error);
            });
        this.reconfigurePartialRentalModal.hide();
    }

  




    addHistory(row: PosHistory) {
        this.alertService.showDialog('Are you sure you want to re-configure \"' + row.serialKey + '\"?', DialogType.confirm, () => this.reConfigureHelper(row));
    }
    reConfigureHelper(row: PosHistory) {
        this.alertService.startLoadingMessage("Requesting...");
        this.loadingIndicator = true;

        this.posHistoryService.newPosHistory(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsHistoryCache = this.rowsHistoryCache.filter(item => item !== row)
                this.rowsHistory = this.rowsHistory.filter(item => item !== row)
                this.loadDeploymentData();
                this.addNewDeploymentToList();
            },
                error => {
                    this.alertService.stopLoadingMessage();
                    this.loadingIndicator = false;

                    this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                        MessageSeverity.error, error);
                });
        this.reconfigureModal.hide();
        this.loadDeploymentData();
        this.addNewDeploymentToList();
    }



    addRetrieve(row: PosHistory) {
        this.alertService.showDialog('Are you sure you want to retrieve \"' + row.serialKey + '\"?', DialogType.confirm, () => this.retrieveHelper(row));
    }
    retrieveHelper(row: PosHistory) {
        this.alertService.startLoadingMessage("Requesting...");
        this.loadingIndicator = true;

        this.posHistoryService.newPosRetrieve(row,'retrieve', row.deploymentRequestId)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsHistoryCache1 = this.rowsHistoryCache1.filter(item => item !== row)
                this.rowsHistory1 = this.rowsHistory1.filter(item => item !== row)
                this.loadDeploymentData();
                this.addNewDeploymentToList();
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
        this.retrieveModal.hide();
    }
    maintenanceRetrieve(row: DeploymentRequest) {

        this.alertService.showDialog('Are you sure you want to send to Maintenance \"' + row.bankName + '\"?', DialogType.confirm, () => this.retrieveHelper2(row));
    }

    //}
    retrieveHelper2(row: DeploymentRequest) {
        //this.alertService.startLoadingMessage("Requesting...");
        //this.loadingIndicator = true;
        this.maintenanceEdit.deploymentRequestId = row.id;
        //this.maintenanceEdit.applicationVersionId = row.applicationVersionId;
        this.maintenanceEdit.inventoryId = row.inventoryId;
        //this.maintenanceEdit.serialNumberId = row.serialNumberId;
        this.maintenanceEdit.address = row.address;
        this.maintenanceEdit.contactNo = row.contactNo1;
        this.maintenanceEdit.contactPerson = row.contactPerson;
        this.maintenanceEdit.district = row.district;
        this.maintenanceEdit.idMerchant = row.idMerchant;
        this.maintenanceEdit.merchant = row.merchant;
        this.maintenanceEdit.idTerminal = row.idTerminal;
        this.maintenanceEdit.outlet = row.outlet;
        this.editedMaintenance = this.maintenanceStatusEditor.addMaintenanceStatus(this.maintenanceEdit, row.id);
        this.maintenanceStatusModal.show();
        //this.posHistoryService.newMaintenance(this.maintenanceEdit, this.maintenanceEdit.deploymentRequestId)
        //    .subscribe(results => {
        //        this.alertService.stopLoadingMessage();
        //        this.loadingIndicator = false;
        //        this.alertService.showMessage("Success", `Serial Number+""+ has been sent to maintenance`, MessageSeverity.success);
        //        this.rowsCache = this.rowsCache.filter(item => item !== row)
        //        this.rows = this.rows.filter(item => item !== row)
        //    },
        //    error => {
        //        this.alertService.stopLoadingMessage();
        //        this.loadingIndicator = false;

        //        this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
        //            MessageSeverity.error, error);
        //    });
    }

    onMaintenanceStatusModalHidden() {
        this.maintenanceStatusEditor.resetForm(true);
    } 
    updateRetrieve(row: DeploymentRequest) {
       
        this.alertService.showDialog('Are you sure you want to send to stock?', DialogType.confirm, () => this.retrieveHelper1(row));
    }

    retrieveHelper1(row: DeploymentRequest) {
        //this.alertService.startLoadingMessage("Requesting...");
        //this.loadingIndicator = true;

        this.posHistoryService.newPosVerifyRetrieve('retrieveVerify', row.id)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
                this.loadDeploymentData();
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }

    undoRetrieve(row: DeploymentRequest) {

        this.alertService.showDialog('Are you sure you want to UNDO Retrieve POS?', DialogType.confirm, () => this.retrieveUndo(row));
    }

    retrieveUndo(row: DeploymentRequest) {
        //this.alertService.startLoadingMessage("Requesting...");
        //this.loadingIndicator = true;

        this.posHistoryService.undoRetrievePos('undo', row.id)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
                this.loadDeploymentData();
            },
                error => {
                    this.alertService.stopLoadingMessage();
                    this.loadingIndicator = false;

                    this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                        MessageSeverity.error, error);
                });
    }

    retrieve() {
        this.loadPosHistory1();
        //this.editingDeployName = { name: deploy.receivedBy };
        //this.sourceDeploy = deploy;
        //this.editedDeploy = this.deployEditor.editDeploy(deploy, this.allDeploy);
        this.retrieveModal.show();
    }

    deleteDeployment(row: DeploymentRequestEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.requestName + '\"?', DialogType.confirm, () => this.deleteInventoryHelper(row));
    }


    deleteInventoryHelper(row: DeploymentRequestEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.deploymentService.deleteDeployment(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }




    get canViewRoles() {
        return this.deploymentService.userHasPermission(Permission.viewRolesPermission)
        
    }
    get canCreateUsers() {
        return this.deploymentService.userHasPermission(Permission.viewRolesPermission)

    }

    get canManageUsers() {
        return this.deploymentService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.deploymentService.userHasPermission(Permission.manageUsersPermission);
    }

}
