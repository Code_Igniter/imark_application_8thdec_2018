
import { Component, Injectable, Injector, OnInit } from '@angular/core';
import { fadeInOut } from '../../services/animations';
import { ConfigurationService } from '../../services/configuration.service';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { Utilities } from "../../services/utilities";
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { Bank } from '../../models/bank/bank.model';
import { BankEdit } from '../../models/bank/bank-edit.model';



@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    animations: [fadeInOut]
})

//@Injectable()
export class HomeComponent implements OnInit {
    columnDefs: any[] = [];
    columns: any[] = [];
    rows: Bank[] = [];
    rowsCache: BankEdit[] = [];


    constructor(private alertService: AlertService, public configurations: ConfigurationService, private dashboardService: DashboardService) {

    }

    ngOnInit() {
        this.columnDefs = [
           
            { headerName: 'Bank', field: 'bankName' },
            { headerName: 'Previous Month', field: 'previousMonth' },
            { headerName: 'Current Month', field: 'currentMonth' },
            { headerName: 'Total Deploy', field: 'total' },
        ];
        this.loadBankDeployData();
       
    }

    loadBankDeployData() {

        this.dashboardService.getBankDeployInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(banks: Bank[]) {


        banks.forEach((bank, index, banks) => {
            (<any>bank).index = index + 1;
        });
        this.rowsCache = [...banks];
        this.rows = banks;
    }


    onDataLoadFailed(error: any) {

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName));
    }

}
