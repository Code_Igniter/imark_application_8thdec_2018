﻿import { Component, AfterViewInit, OnInit, ViewChild, TemplateRef, Input, OnDestroy } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormControl, FormBuilder, Validators, PatternValidator } from '@angular/forms';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { SerialNoService } from "../../services/serial-no.service";
import { InventoryAddService } from "../../services/inventory-add.service";
import { Utilities } from '../../services/utilities';
import { Inventory } from '../../models/inventory.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { SerialNo } from '../../models/serial-no.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { SerialNoEdit } from '../../models/serial-no-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';



@Component({
    selector: 'edit-serial',
    templateUrl: './edit-serial-no.component.html',
    styleUrls: ['./inventory.component.css']
})
export class EditSerialNoComponent implements OnInit {

    private isEditMode = false;
    private isNewSerial = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingDeployName: string;
    private uniqueId: string = Utilities.uniqueId();
    private serial: SerialNo = new SerialNo();
    private allSerialNo: SerialNo[] = [];
    private serialEdit: SerialNoEdit = new SerialNoEdit();


    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;
    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;




    @ViewChild('status')
    private status;



    constructor(private alertService: AlertService, private serialService: SerialNoService) {
    }

    ngOnInit() {


    }





    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.serialEdit = new SerialNoEdit();
            Object.assign(this.serialEdit, this.serial);
        }
        else {
            if (!this.serialEdit)
                this.serialEdit = new SerialNoEdit();
            this.isEditingSelf = this.serialService.updateSerial(this.serial) ? this.serialEdit.id == this.serial.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveSerial() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");
        this.serialService.updateSerial(this.serialEdit).subscribe(response => this.saveSerialSuccessHelper(), error => this.saveSerialFailedHelper(error));


    }


    private saveSerialSuccessHelper(serial?: SerialNo) {


        if (serial)
            Object.assign(this.serialEdit, serial);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.serial, this.serialEdit);
        this.serialEdit = new SerialNoEdit();
        this.resetForm();


        if (this.isGeneralEditor) {

            this.alertService.showMessage("Success", `  \"${this.serialEdit.serialKey}\" was updated successfully`, MessageSeverity.success);

        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveSerialFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "Duplicate Serial Number", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.serialEdit = this.serial = new SerialNoEdit();
        else
            this.serialEdit = new SerialNoEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.serialEdit = this.serial = new SerialNoEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }





    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }



    editSerial(serial: SerialNo, allSerial: SerialNo[]) {

        this.isGeneralEditor = true;
        this.serial = serial;
        this.serialEdit = serial;
        
        this.edit();

        return this.serialEdit;
       
    }





    get canViewAllRoles() {
        return this.serialService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.serialService.userHasPermission(Permission.assignRolesPermission);
    }
}
