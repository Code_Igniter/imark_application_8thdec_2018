﻿
import { Component, AfterViewInit, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, MessageSeverity } from '../../services/alert.service';
import { InventoryService } from "../../services/inventory.service";
import { InventoryAddService } from "../../services/inventory-add.service";
import { Utilities } from '../../services/utilities';
import { InventoryAdd } from '../../models/inventory-add.model';
import { Inventory } from '../../models/inventory.model';
import { InventoryEdit } from '../../models/inventory-edit.model';
import { InventoryEditComponent } from "./inventory-edit.component";
import { Permission } from '../../models/permission.model';
import { AccountService } from "../../services/account.service";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import * as moment from 'moment';
import * as $ from 'jquery';
import { Observable } from 'rxjs/Observable';
import { MatFormFieldAppearance } from '@angular/material';
import { catchError, map, tap, startWith, switchMap, debounceTime, distinctUntilChanged, takeWhile, first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl, ReactiveFormsModule } from '@angular/forms'
import { hasLifecycleHook } from '@angular/compiler/src/lifecycle_reflector';

@Component({
    selector: 'inventory-add',
    templateUrl: './inventory-add.component.html',
    styleUrls: ['./inventory.component.css']
})
export class InventoryAddComponent implements OnInit, AfterViewInit {
    rows: Inventory[] = [];
    maxDate = new Date();
    rowsCache: Inventory[] = [];
    //private isEditMode = false;
    private isNewInventory = false;
    private isSaving = false;
    //private isEditingSelf = false;
    private showValidationErrors = false;
    //private editingInventoryName: string;
    private uniqueId: string = Utilities.uniqueId();
    private inventory: InventoryAdd = new InventoryAdd();
    private invent: Inventory = new Inventory();
    private modalInventory: Inventory[] = [];
    private inventoryAdd: InventoryAdd;
    editedInventory: InventoryEdit;
    sourceInventory: InventoryEdit;
    editingInventoryName: {};
    loadingIndicator: boolean;
    public formResetToggle = true;
    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;
    @ViewChild('editorModal')
    editorModal: ModalDirective;
    @ViewChild('inventoryEditor')
    inventoryEditor: InventoryEditComponent;
    @ViewChild('f')
    private form;
    @ViewChild('inventoryNameItem')
    private inventoryId;
    @ViewChild('totalNumberItem')
    private totalNumber;
    @ViewChild('remarksItem')
    private remarks;
    @ViewChild('buyDateItem')
    private buyDate;
    myControl = new FormControl();
    inventoryControl = new FormControl('', [Validators.required]);
    public filteredOptions: Observable<Inventory[]>;


    constructor(private alertService: AlertService, private inventoryService: InventoryService, private inventoryAddService: InventoryAddService, private accountService: AccountService) {
      
    }
    ngOnInit() {
        this.addNewInventoryToList();
        //this.filteredOptions = this.myControl.valueChanges
        //    .pipe(
        //    startWith(null)
        //    //map(value => typeof value === 'object' ? value.)            
        //    );
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
            startWith(''),  
            debounceTime(200),
            distinctUntilChanged(),
            switchMap(val => {
                return this.filter(val || '')

            }))



    }
    ngAfterViewInit() {
        this.inventoryEditor.changesSavedCallback = () => {
            //this.addNewInventoryToList();
            this.editorModal.hide();
        };
        this.inventoryEditor.changesCancelledCallback = () => {
            this.editedInventory = null;
            this.sourceInventory = null;
            this.editorModal.hide();
        };
       
    }

    addNewInventoryToList() {
        if (this.sourceInventory) {
            Object.assign(this.sourceInventory, this.editedInventory);
            let sourceIndex = this.rowsCache.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);
            sourceIndex = this.rows.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);
            this.editedInventory = null;
            this.sourceInventory = null;
        }
        else {
            let inventory = new Inventory();
            Object.assign(inventory, this.editedInventory);
            this.editedInventory = null;
            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }
            (<any>Inventory).index = maxIndex + 1;
            this.rowsCache.splice(0, 0, inventory);
            this.rows.splice(0, 0, inventory);
        }
        this.loadData();
    }
 
    //public filter(val: string): Observable<Inventory[]> {
    //    return this.inventoryService.getInventoryInfo()
    //        .pipe(
    //        map(response => response.filter(option => {
    //            return option.primaryItem.toLowerCase().indexOf(val.toLowerCase()) === 0
    //        }))
    //    )

    //}

    public filter(name: string): Observable<Inventory[]> {
        //return this.invent.filter(option =>
        //    option.primaryItem.toLowerCase().indexOf(name.toLowerCase()) === 0);
        return this.inventoryService.getInventoryInfo(-1, -1, this.inventory.hasSerialNo)
            .pipe(
            map(response => response.filter(option => 
                option.primaryItem.toLowerCase().indexOf(option.primaryItem.toLowerCase()) === 0
            ))
        )
    }
    public displayFn(inventory?: Inventory): string | undefined {
        return inventory ? inventory.primaryItem : undefined;
    }

    //diplayFn() {
    //    const itemList: Inventory[] = [];
    //    this.inventoryService.getInventoryInfo(-1, -1, this.inventory.hasSerialNo).subscribe(items => itemList.push(...items));
    //    return (id?: string) => id ? itemList.find(i => i.id === id).primaryItem : null;
    //}
  
    loadData() {
        //this.inventory.hasSerialNo
        this.inventoryService.getInventoryInfo(-1, -1, false).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }
    onDataLoadSuccessful(inventorys: Inventory[]) {
        inventorys.forEach((inventory, index, inventorys) => {
            (<any>inventory).index = index + 1;
        });
        this.rowsCache = [...inventorys];   
        this.rows = inventorys;
    }
    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    
    onEditorModalHidden() {
        this.editingInventoryName = null;
        this.inventoryEditor.resetForm(true);
    }
    newInventory() {
        this.editingInventoryName = null;
        this.sourceInventory = null;
        this.editedInventory = this.inventoryEditor.newInventory(this.modalInventory);
        this.editorModal.show();
    }

    private save() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");
        
        this.inventoryAddService.newAddInventory(this.inventory).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
       
    }
    private saveSuccessHelper(inventory?: InventoryAdd) {
        this.testIsInventoryChanged(this.inventory, this.inventoryAdd);
        if (inventory)
            Object.assign(this.inventoryAdd, inventory);
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        this.alertService.showMessage("Success", `Inventory was successfully updated.`, MessageSeverity.success);
        Object.assign(this.inventory, this.inventoryAdd);
        this.inventoryAdd = new InventoryAdd();
        this.resetForm();
          
       
    }
    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);
       
    }
    private testIsInventoryChanged(inventory: InventoryAdd, addedInventory: InventoryAdd) {
        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);
        //let modifiedRoles = rolesAdded.concat(rolesRemoved);
        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }

  
    resetForm(replace = false) {
        this.isSaving = false;
        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;
            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }

    get canViewUsers() {
        return this.accountService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.accountService.userHasPermission(Permission.viewRolesPermission);
    }
}