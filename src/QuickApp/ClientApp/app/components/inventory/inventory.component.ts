﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { InventoryService } from "../../services/inventory.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Inventory } from '../../models/inventory.model';
import { InventoryEdit } from '../../models/inventory-edit.model';
import { InventoryEditComponent } from "./inventory-edit.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';


@Component({
    selector: 'inventory-management',
    templateUrl: './inventory.component.html',
    styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit, AfterViewInit {
    columns: any[] = [];
    rows: Inventory[] = [];
    rowsCache: Inventory[] = [];
    editedInventory: InventoryEdit;
    sourceInventory: InventoryEdit;
    editingInventoryName: {};
    loadingIndicator: boolean;
    inventory: Inventory = new Inventory();

    allInventory: Inventory[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('PrimaryItemTemplate')
    primaryItemTemplate: TemplateRef<any>;

    @ViewChild('secondaryItemTemplate')
    secondaryItemTemplate: TemplateRef<any>;

    @ViewChild('tertiaryItemTemplate')
    tertiaryItemTemplate: TemplateRef<any>;

    @ViewChild('detailItemTemplate')
    detailItemTemplate: TemplateRef<any>;

    @ViewChild('hasSerialNoTemplate')
    hasSerialNoTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('inventoryEditor')
    inventoryEditor: InventoryEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private inventoryService: InventoryService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'primaryItem', name: gT('inventory.management.PrimaryItem'), width: 50, cellTemplate: this.primaryItemTemplate },
            { prop: 'secondaryItem', name: gT('inventory.management.SecondaryItem'), width: 90, cellTemplate: this.secondaryItemTemplate },
            { prop: 'tertiaryItem', name: gT('inventory.management.TertiaryItem'), width: 120, cellTemplate: this.tertiaryItemTemplate},
            { prop: 'detailItem', name: gT('inventory.management.DetailItem'), width: 140, cellTemplate: this.detailItemTemplate },
            //{ prop: 'hasSerialNo', name: gT('inventory.management.HasSerialNo'), width: 140, cellTemplate: this.hasSerialNoTemplate },
        ];
       
        if (this.canManageUsers)
            this.columns.push({ name: '', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });

       
        this.addNewInventoryToList();
        
    }


    ngAfterViewInit() {

        this.inventoryEditor.changesSavedCallback = () => {
            this.addNewInventoryToList();
            this.editorModal.hide();
        };

        this.inventoryEditor.changesCancelledCallback = () => {
            this.editedInventory = null;
            this.sourceInventory = null;
            this.editorModal.hide();
        };
    
    }


    addNewInventoryToList() {
        if (this.sourceInventory) {
            Object.assign(this.sourceInventory, this.editedInventory);

            let sourceIndex = this.rowsCache.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedInventory = null;
            this.sourceInventory = null;
        }
        else {
            let inventory = new Inventory();
            Object.assign(inventory, this.editedInventory);
            this.editedInventory = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>Inventory).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, inventory);
            this.rows.splice(0, 0, inventory);
        }
        this.loadInventoryData();
    }


    loadInventoryData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.inventory.hasSerialNo = true;
        this.inventoryService.getInventoryInfo(-1, -1, this.inventory.hasSerialNo).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(inventorys: Inventory[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        inventorys.forEach((inventory, index, inventorys) => {
            (<any>inventory).index = index + 1;
        });
        this.rowsCache = [...inventorys];
        this.rows = inventorys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.primaryItem, r.secondaryItem, r.tertiaryItem, r.detailItem));
    }

    onEditorModalHidden() {
        this.editingInventoryName = null;
        this.inventoryEditor.resetForm(true);
    }


    newInventory() {
        this.editingInventoryName = null;
        this.sourceInventory = null;
        this.editedInventory = this.inventoryEditor.newInventory(this.allInventory);
        this.editorModal.show();
    }


    editInventory(row: InventoryEdit) {
        this.editingInventoryName = { name: row.primaryItem };
        this.sourceInventory = row;
        this.editedInventory = this.inventoryEditor.editInventory(row, this.allInventory);
        this.editorModal.show();
    }


    deleteInventory(row: InventoryEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.primaryItem + '\"?', DialogType.confirm, () => this.deleteInventoryHelper(row));
    }


    deleteInventoryHelper(row: InventoryEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.inventoryService.deleteInventory(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }

    get canCreateUsers() {
        return this.inventoryService.userHasPermission(Permission.viewRolesPermission)

    }

    get canViewUsers() {
        return this.inventoryService.userHasPermission(Permission.viewUsersPermission);
    }
   
    get canViewRoles() {
        return this.inventoryService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.inventoryService.userHasPermission(Permission.manageUsersPermission);
    }
   
}
