﻿
import { Component, AfterViewInit, OnInit, ViewChild, TemplateRef, Input, OnDestroy } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { SerialNoService } from "../../services/serial-no.service";
import { Utilities } from '../../services/utilities';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { SerialNo } from '../../models/serial-no.model';
import { SerialNoEdit } from '../../models/serial-no-edit.model';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl, ReactiveFormsModule } from '@angular/forms'
import { PosStockEdit } from '../../models/posStock/pos-stock-edit.model';
import { PosStock } from "../../models/posStock/pos-stock.model";
import { PosStockService } from '../../services/posStock/pos-stock.service';
import { BankService } from "../../services/bank/bank.service";
import { Bank } from '../../models/bank/bank.model';
import { DeploymentService } from "../../services/deployment/deployment.service";
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { Configuration } from '../../models/configuration/configuration.model';
import { ConfigurationEdit } from '../../models/configuration/configuration-edit.model';
import { Inventory } from '../../models/inventory.model';
import { InventoryEdit } from '../../models/inventory-edit.model';

@Component({
    selector: 'serial-list',
    templateUrl: './serial-list-in-stock.component.html',
    styleUrls: ['./inventory.component.css']
})
export class SerialListInStockComponent implements OnInit {
    columns: any[] = [];
    rowsSerialCache: SerialNo[] = [];
    rowsSerial: SerialNo[] = [];
    inventoryId: string;
    inventoryAddId: string;
    rows: Bank[] = [];
    private serialRef = null;
    private isSale = true;
    private isEditMode = false;
    private isNewPosStock = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingPosStockName: string;
    private uniqueId: string = Utilities.uniqueId();
    private posStock: PosStock = new PosStock();
    private serialEdit: SerialNoEdit = new SerialNoEdit();
    private serial: SerialNo = new SerialNo();
    private serialAdd: SerialNo;
    editedSerial: SerialNoEdit;
    private deploymentEdit: DeploymentRequestEdit = new DeploymentRequestEdit();
    private configurationEdit: ConfigurationEdit = new ConfigurationEdit();
    private allPosStock: PosStock[] = [];
    private posStockEdit: PosStockEdit = new PosStockEdit();
    loadingIndicator: boolean;
    allSerial: SerialNo[] = [];
    allInventory: Inventory[] = [];
    private inventoryAddEdit: InventoryEdit = new InventoryEdit();
    private inventoryAdd: Inventory = new Inventory();
    bankControl = new FormControl('', [Validators.required]);
    public formResetToggle = true;

    public depId: string;

    public changesPosStockSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    @ViewChild('location')
    private location;

    @ViewChild('remarks')
    private remarks;

    @ViewChild('serialKey')
    private serialKey;

    @ViewChild('serialNumberId')
    private serialNumberId;


    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;
    @ViewChild('serialKeyTemplate')
    serialKeyTemplate: TemplateRef<any>;
    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    constructor(private alertService: AlertService, private deploymentService: DeploymentService, private translationService: AppTranslationService, private posStockService: PosStockService,
        private serialService: SerialNoService, private bankService: BankService, private configurationsService: ConfigurationsService) {
    }

    ngOnInit() {

        this.loadSerialData();
        this.loadCurrentBankData();
    }
    public loadSerialData() {

        this.serialService.getPosStockList(this.inventoryId).subscribe(results => this.onSerialDataLoadSuccessful(results), error => this.onSerialDataDataLoadFailed(error));
    }


    private onSerialDataLoadSuccessful(serials: SerialNo[]) {
        serials.forEach((ser, index, serials) => {
            (<any>ser).index = index + 1;
        });
        this.rowsSerialCache = [...serials];
        this.rowsSerial = serials;
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        let gT = (key: string) => this.translationService.getTranslation(key);
        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'serialKey', name: gT('inventory.management.SerialNumber'), width: 150, cellTemplate: this.serialKeyTemplate },
        ];

        //if (this.canManageUsers)
        //    this.columns.push({ name: 'Action', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });

    }

    private onSerialDataDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve Serial Keys from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.serial = new SerialNo();
    }



    //public loadSerialData() {

    //    this.serialService.getSerialPosStockInfo(this.serialId).subscribe(results => this.onSerialDataLoadSuccessful(results), error => this.onSerialDataDataLoadFailed(error));
    //}


    //private onSerialDataLoadSuccessful(serials: SerialNo[]) {
    //    serials.forEach((ser, index, serials) => {
    //        (<any>ser).index = index + 1;
    //    });
    //    this.rowsSerialCache = [...serials];
    //    this.rowsSerial = serials;
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;

    //    let gT = (key: string) => this.translationService.getTranslation(key);
    //    this.columns = [
    //        { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
    //        { prop: 'serialKey', name: gT('inventory.management.SerialNumber'), width: 150, cellTemplate: this.serialKeyTemplate },
    //    ];

    //    if (this.canManageUsers)
    //        this.columns.push({ name: 'Action', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });

    //}

    //private onSerialDataDataLoadFailed(error: any) {
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;
    //    this.alertService.showStickyMessage("Load Error", `Unable to retrieve Serial Keys from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
    //        MessageSeverity.error, error);

    //    this.serial = new SerialNo();
    //}


    loadSerialList(allInventory: Inventory[], id, inventoryAdd: Inventory) {
        this.posStockEdit.inventoryAddId = inventoryAdd.id;
        this.inventoryId = id;

        this.allInventory = [...allInventory];
        this.inventoryAdd = this.inventoryAddEdit = new InventoryEdit();
        this.loadSerialData();
        return this.inventoryAddEdit;
    }

    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

    private loadCurrentBankData() {
        this.bankService.getPartialRentalBankInfo().subscribe(results => this.onBankDataLoadSuccessful(results), error => this.onBankDataLoadFailed(error));
    }


    private onBankDataLoadSuccessful(banks: Bank[]) {
        banks.forEach((bank, index, banks) => {
            (<any>bank).index = index + 1;
        });
        this.rows = banks;
    }

    private onBankDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    //private saveDeployment() {
    //    this.deploymentEdit.inventoryAddId = this.posStockEdit.inventoryAddId;
    //    this.deploymentEdit.locationForPartialRental = this.posStockEdit.location;
    //    this.deploymentEdit.remarksForPartialRental = this.posStockEdit.remarks;
    //    this.deploymentEdit.serialNumberId = this.posStockEdit.serialNumberId;
    //    this.deploymentEdit.bankId = this.posStockEdit.bankId;
    //    this.deploymentService.newPartialRentalDeployment(this.deploymentEdit).subscribe(user => this.saveSuccessHelper1(user), error => this.saveFailedHelper1(error));

    //}


    //private saveSuccessHelper1(deployment?: DeploymentRequestEdit) {
    //    if (deployment)
    //        Object.assign(this.deploymentEdit, deployment);
    //}


    //private saveFailedHelper1(error: any) {

    //}


    private savePosStock() {

        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        this.isSale = true;
        this.posStockService.addPosStock(this.posStockEdit, this.inventoryId, this.isSale).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        //this.saveDeployment();



    }


    private saveSuccessHelper(posStock?: PosStockEdit) {
        //this.testIsConfigurationChanged(this.configuration, this.configurationEdit);

        if (posStock)
            Object.assign(this.posStockEdit, posStock);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.posStock, this.posStockEdit);
        this.posStockEdit = new PosStockEdit();
        this.posStock = new PosStock();
        this.resetForm();


        if (this.isGeneralEditor) {

            this.alertService.showMessage("Success", ` \"${this.posStock.serialKey}\" has been added successfully`, MessageSeverity.success);

        }

        this.isEditMode = false;


        if (this.changesPosStockSavedCallback)
            this.changesPosStockSavedCallback();

    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();

        this.alertService.showStickyMessage("Save Error", "Dublicate Serial Number/ Please Enter a Serial Number", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);



        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }




    private cancel() {
        if (this.isGeneralEditor)
            this.posStockEdit = this.posStock = new PosStockEdit();
        else
            this.posStockEdit = new PosStockEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.posStockEdit = this.posStock = new PosStockEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }



    onSearchChanged(value: string) {
        this.rowsSerial = this.rowsSerialCache.filter(r => Utilities.searchArray(value, false, r.serialKey));
    }



    get canViewAllRoles() {
        return this.posStockService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.posStockService.userHasPermission(Permission.assignRolesPermission);
    }
    get canManageUsers() {
        return this.posStockService.userHasPermission(Permission.manageUsersPermission);
    }
}
