﻿import { Component, AfterViewInit, OnInit, ViewChild, TemplateRef, Input, OnDestroy  } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormControl, FormBuilder, Validators, PatternValidator } from '@angular/forms';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { SerialNoService } from "../../services/serial-no.service";
import { InventoryAddService } from "../../services/inventory-add.service";
import { Utilities } from '../../services/utilities';
import { Inventory } from '../../models/inventory.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { SerialNo } from '../../models/serial-no.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { SerialNoEdit } from '../../models/serial-no-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { EditSerialNoComponent } from '../inventory/edit-serial-no.component';

@Component({
    selector: 'serial-add',
    templateUrl: './serial-no.component.html',
    styleUrls: ['./inventory.component.css']
})
export class SerialAddComponent implements OnInit, AfterViewInit  {
    columns: any[] = [];
    rowsCache: SerialNo[] = [];
    rows: SerialNo[] = [];
    private editingInventoryName: string;
    //private isEditMode = false;   
    private isNewSerial = false;
    private isSaving = false;
  
    //private isEditingSelf = false;
    //private showValidationErrors = false;
    //private editingInventoryName: string;
    private serial: SerialNo = new SerialNo();
    private serialEdit: SerialNoEdit = new SerialNoEdit();
    //private inventorys: Inventory = new Inventory();
    private serialAdd: SerialNo;
    editedSerial: SerialNoEdit;
    sourceSerial: SerialNoEdit;
    editingSerialName: {};
    loadingIndicator: boolean;
    private allSerial: SerialNo[] = [];
    private uniqueId: string = Utilities.uniqueId();
    private inventoryAdd: InventoryAdd = new InventoryAdd();
    private allInventory: InventoryAdd[] = [];
    private inventoryAddEdit: InventoryAddEdit;
    private showValidationErrors = false;
    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;
    @Input()
    isViewOnly: boolean;
    @Input()
    isGeneralEditor = false;
    @ViewChild('editorModal')
    editorModal: ModalDirective;
    @ViewChild('f')
    private form;
    @ViewChild('serialKey')
    private serialKey;
    @ViewChild('isSold')
    private isSold;
    @ViewChild('inventoryName')
    private inventoryName;
    @ViewChild('totalNumber')
    private totalNumber;
    @ViewChild('buyDate')
    private buyDate;
    @ViewChild('InventoryAddListComponent')
    private InventoryAddListComponent;
    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;
    @ViewChild('serialKeyTemplate')
    serialKeyTemplate: TemplateRef<any>;
    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;
    @ViewChild('serialEditor')
    serialEditor: EditSerialNoComponent;
    @ViewChild('serialStatusModal')
    serialStatusModal: ModalDirective;
    constructor(private alertService: AlertService, private translationService: AppTranslationService, private serialService: SerialNoService, private inventoryAddService: InventoryAddService) {
    }
    ngOnInit() {
        let gT = (key: string) => this.translationService.getTranslation(key);
        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'serialKey', name: gT('inventory.management.SerialNumber'), width: 100, cellTemplate: this.serialKeyTemplate }
                   ];
        if (this.canManageUsers) {
            this.columns.push({ name: '', width: 230, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });
        }
        //this.loadSerialData();
        //this.loadInventoryAdd();
    }
    ngAfterViewInit() {
        this.serialKey.changesSavedCallback = () => {
            this.addNewSerialToList();
         
        };
        this.serialKey.changesCancelledCallback = () => {
            this.editedSerial = null;
            this.sourceSerial = null;
          
        };
        this.serialEditor.changesSavedCallback = () => {
            this.addNewSerialToList();
            this.serialStatusModal.hide();
        };
        this.serialEditor.changesCancelledCallback = () => {
            this.editedSerial = null;
            this.sourceSerial = null;
            this.serialStatusModal.hide();
        };
    }
    loadSerialData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.serial.inventoryAddId = this.inventoryAdd.id;
       
        this.serialService.getSerialPage(-1,-1,this.serial.inventoryAddId).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
        
    }
    onDataLoadSuccessful(serials: SerialNo[]) {
        serials.forEach((ser, index, serials) => {
            (<any>ser).index = index + 1;
        });
        this.rowsCache = [...serials];
        this.rows = serials;
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
    }
    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    addNewSerialToList() {
        if (this.sourceSerial) {
            Object.assign(this.sourceSerial, this.editedSerial);
            let sourceIndex = this.rowsCache.indexOf(this.sourceSerial, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);
            sourceIndex = this.rows.indexOf(this.sourceSerial, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);
            this.editedSerial = null;
            this.sourceSerial = null;
        }
        else {
            let serial = new SerialNo();
            Object.assign(serial, this.editedSerial);
            this.editedSerial = null;
            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }
            (<any>SerialNo).index = maxIndex + 1;
            this.rowsCache.splice(0, 0, serial);
            this.rows.splice(0, 0, serial);
        }
        this.loadSerialData();
    }


    private save(inventoryAddId) {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");
        this.serial.inventoryAddId = this.inventoryAdd.id;
        this.serialService.addSerial(this.serial).subscribe(serial => this.saveSuccessHelper(serial), error => this.saveFailedHelper(error));
        //}
    }
    private saveSuccessHelper(serial?: SerialNo) {
       
        if (serial)
            Object.assign(this.serialAdd, serial);
        this.isSaving = false;
        this.showValidationErrors = true;
        this.alertService.stopLoadingMessage();
        Object.assign(this.serial, this.serialAdd);
        this.serialAdd = new SerialNo();
        this.loadSerialData();
        this.addNewSerialToList();
        this.loadInventoryAdd();
        this.resetForm();
    }

    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "Dublicate Serial Number / Please Enter a Serial Number:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);
    }
    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.serialKey, r.isSold));
    }

    private updateSerial(row: SerialNoEdit, isSold) {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        //row.isSold = true;
        this.serialService.updateSerial(row).subscribe(response => this.saveSerialSuccessHelper(), error => this.saveSerialFailedHelper(error));
        
    }


    private saveSerialSuccessHelper(serial?: SerialNo) {


        if (serial)
            Object.assign(this.serialEdit, serial);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.serial, this.serialEdit);
        this.serialEdit = new SerialNoEdit();
        this.addNewSerialToList();
        //this.resetForm();      
        }
    


    private saveSerialFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "Unable to save", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);
    }


    deleteSerialKey(row: SerialNoEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.serialKey + '\"?', DialogType.confirm, () => this.deleteInventoryHelper(row));
    }
    deleteInventoryHelper(row: SerialNoEdit) {
        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;
        this.serialService.deleteSerial(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
                this.alertService.showMessage("Success", + row.serialKey +" " +"was deleted successfully", MessageSeverity.success);
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }
    newAddInventory(allInventory: InventoryAdd[]) {
        this.isGeneralEditor = true;
        //this.isNewInventory = true;
        this.allInventory = [...allInventory];
        this.editingInventoryName = null;
        this.inventoryAdd = this.inventoryAddEdit = new InventoryAddEdit();
        return this.inventoryAddEdit;
    }
    serialPage(inventoryAdd: InventoryAdd, allInventory: InventoryAdd[]) {
        if (inventoryAdd) {
            this.isGeneralEditor = true;
      
            this.editingInventoryName = inventoryAdd.primaryItem;
           
            this.inventoryAdd = new InventoryAdd();
            this.inventoryAddEdit = new InventoryAddEdit();
            Object.assign(this.inventoryAdd, inventoryAdd);
            Object.assign(this.inventoryAddEdit, inventoryAdd);
            this.serialService.getSerialPage(-1, -1,this.inventoryAddEdit.id).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
            this.inventoryAddService.getInventoryAdd(this.inventoryAdd.id).subscribe(results => this.onDataLoadSuccessful1(results), error => this.onDataLoadFailed1(error));
            return this.inventoryAddEdit;
        }
        else {
            return this.newAddInventory(allInventory);
        }
    }
    loadInventoryAdd() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;

        this.inventoryAddService.getInventoryAdd(this.inventoryAdd.id).subscribe(results => this.onDataLoadSuccessful1(results), error => this.onDataLoadFailed1(error));

    }
    onDataLoadSuccessful1(inventoryAdd: InventoryAdd) {
        this.inventoryAdd = new InventoryAdd();
        this.inventoryAddEdit = new InventoryAddEdit();
        Object.assign(this.inventoryAdd, inventoryAdd);
        Object.assign(this.inventoryAddEdit, inventoryAdd);
        
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        return this.inventoryAdd;
    }
    onDataLoadFailed1(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    resetForm(replace = false) {
        this.isSaving = false;
        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;
            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }
    editSerial(row: SerialNo) {

        this.alertService.showDialog('Are you sure you want to edit Serial Number \"' + row.serialKey + '\"?', DialogType.confirm, () => this.retrieveHelper2(row));
    }

    //}
    retrieveHelper2(row: SerialNo) {

        this.editedSerial = this.serialEditor.editSerial(row, this.allSerial);
        this.serialStatusModal.show();
       
    }

    onSerialModalHidden() {
        this.serialEditor.resetForm(true);
    } 
  
    get canViewAllRoles() {
        return this.serialService.userHasPermission(Permission.viewRolesPermission);
    }
    get canAssignRoles() {
        return this.serialService.userHasPermission(Permission.assignRolesPermission);
    }
    get canManageUsers() {
        return this.serialService.userHasPermission(Permission.manageUsersPermission);
    }
}