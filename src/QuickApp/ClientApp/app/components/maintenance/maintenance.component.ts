﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { MaintenanceService } from "../../services/maintenance/maintenance.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Maintenance } from '../../models/maintenance/maintenance.model';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { MaintenanceEditComponent } from "./maintenance-edit.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import * as moment from 'moment';
import { DamagePosEdit } from '../../models/configuration/configuration-edit.model';
import { AddDamagePosComponent } from "./damage-pos-status.component";

@Component({
    selector: 'maintenance-management',
    templateUrl: './maintenance.component.html',
    styleUrls: ['./maintenance.component.css']
})
export class MaintenanceComponent implements OnInit, AfterViewInit {
    private gridApi;
    private gridColumnApi;
    columnDefs: any[] = [];

    columns: any[] = [];
    columnsSerial: any[] = [];
    rows: Maintenance[] = [];
    rowsCache: Maintenance[] = [];
    serialRows: Maintenance[] = [];
    serialRowsCache: Maintenance[] = [];
    editedMaintenance: MaintenanceEdit;
    sourceMaintenance: MaintenanceEdit;
    editedDamage: DamagePosEdit;
    editingMaintenanceName: {};
    loadingIndicator: boolean;
    private maintenance: Maintenance = new Maintenance();
    private serialMaintenanceAdd: MaintenanceEdit = new MaintenanceEdit();
    private damagaPos: DamagePosEdit = new DamagePosEdit();
    allMaintenance: Maintenance[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('status')
    private status;

    @ViewChild('serialKeyTemplate')
    serialKeyTemplate: TemplateRef<any>;

    @ViewChild('serialKeyTemplate1')
    serialKeyTemplate1: TemplateRef<any>;

    @ViewChild('statusTemplate')
    statusTemplate: TemplateRef<any>;

    @ViewChild('terminateTemplate')
    terminateTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('verificationTemplate')
    verificationTemplate: TemplateRef<any>; 

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('actionserialTemplate')
    actionserialTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('statusModal')
    statusModal: ModalDirective;

    @ViewChild('serialListModal')
    serialListModal: ModalDirective;

    @ViewChild('maintenanceEditor')
    maintenanceEditor: MaintenanceEditComponent;


    @ViewChild('damagePosEditor')
    damagePosEditor: AddDamagePosComponent;

    @ViewChild('serialKeyTemplate')
    serialKeyListTemplate: TemplateRef<any>;

   


    constructor(private alertService: AlertService, private translationService: AppTranslationService, private maintenanceService: MaintenanceService, private posHistoryService: PosHistoryService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        //this.columns = [
        //    { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
        //    { prop: 'serialkey', name: "Serial Number", width: 180, cellTemplate: this.serialKeyTemplate },
        //    { prop: 'primaryItem', name: "Terminal Type", width: 150, cellTemplate: this.primaryItemTemplate },
        //    { prop: 'status', name: gT('maintenance.management.Status'), width: 90, cellTemplate: this.statusTemplate },
        //    { prop: 'terminate', name: gT('maintenance.management.Terminate'), width: 120, cellTemplate: this.terminateTemplate },
        //    { prop: 'remarks', name: gT('maintenance.management.Remarks'), width: 180, cellTemplate: this.remarksTemplate },
        //    { prop: 'resolvedBy', name: "Resolved By", width: 140, cellTemplate: this.verificationTemplate },
        //];

        //if (this.canManageUsers)
        //    this.columns.push({ name: '', width: 270, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });

        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 180, suppressSizeToFit: true },
            { headerName: 'Merchant Name', field: 'merchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialNumber', width: 150, suppressSizeToFit: true },
            { headerName: 'Application Version', field: 'applicationFormat', width: 150, suppressSizeToFit: true },
            { headerName: 'Status', field: 'status', width: 150, suppressSizeToFit: true },
            //{ headerName: 'Terminate', field: 'terminate', width: 150, suppressSizeToFit: true },
            { headerName: 'Remarks', field: 'remarks', width: 150, suppressSizeToFit: true },
            { headerName: 'Resolved By', field: 'resolvedBy', width: 150, suppressSizeToFit: true },
            { headerName: 'Created By', field: 'createdBy', width: 150, suppressSizeToFit: true },
            {
                headerName: 'Created Date', field: 'createdDate', width: 150, valueFormatter: (data) => moment(data.value).format('lll'), suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },
            
        ];


        if (this.canCreateUsers)
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: 'requestName',
                suppressSorting: true,
                width: 300,

                template:
                    `<button type= "button"  data-action-type="Issue"  class="btn btn-link btn-xs"   style = "background-color:maroon;color:white;text-decoration:none">Add Issue</button>
                     <button type= "button"  data-action-type="Stock"  class="btn btn-link btn-xs"   style = "background-color:maroon;color:white;text-decoration:none">Send to Stock</button>
                     <button type = "button"  data-action-type="Damage"  class="btn btn-link btn-xs"   style = "background-color:maroon;color:white;text-decoration:none" > Send to Damage </button>`,

            });

        this.addNewMaintenanceToList();

    }

    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;

            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {

                case "Issue":
                    return this.onIssue(data);
                case "Stock":
                    return this.onStock(data);
                case "Damage":
                    return this.onDamage(data);
               
            }
        }
    }
    public onIssue(row: MaintenanceEdit) {

        this.editMaintenance(row);

    }
    public onStock(row: MaintenanceEdit) {

        this.deleteMaintenance(row);

    }
    public onDamage(row: MaintenanceEdit) {
        this.addDamage(row);

    }
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.maintenanceService.getMaintenanceInfo().subscribe(data => {
            params.api.setRowData(data);
        });

    }
    ngAfterViewInit() {

        this.maintenanceEditor.changesSavedCallback = () => {
            this.addNewMaintenanceToList();
            this.editorModal.hide();
        };

        this.maintenanceEditor.changesCancelledCallback = () => {
            this.editedMaintenance = null;
            this.sourceMaintenance = null;
            this.editorModal.hide();
        };
        this.damagePosEditor.changesSavedCallback = () => {
            this.addNewMaintenanceToList();
            this.statusModal.hide();
        };

        this.damagePosEditor.changesCancelledCallback = () => {
            this.editedMaintenance = null;
            this.sourceMaintenance = null;
            this.statusModal.hide();
        };

    }

    //newSerialKey() {
    //    this.alertService.startLoadingMessage();
    //    this.loadingIndicator = true;
    //    this.maintenanceService.getSerialListInfo().subscribe(results => this.onSerialDataLoadSuccessful(results), error => this.onSerialDataLoadFailed(error));

    //    let gT = (key: string) => this.translationService.getTranslation(key);
    //    this.columnsSerial = [
    //        { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
    //        { prop: 'serialListModal', name: gT('maintenance.management.SerialKey'), width: 50, cellTemplate: this.serialKeyTemplate1 },
            
    //    ];

    //    if (this.canManageUsers)
    //        this.columnsSerial.push({ name: '', width: 130, cellTemplate: this.actionserialTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });
    //}
    //onSerialDataLoadSuccessful(maintenances: Maintenance[]) {
    //    this.serialListModal.show();
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;

    //    maintenances.forEach((maintenance, index, maintenances) => {
    //        (<any>maintenance).index = index + 1;
    //    });
    //    this.serialRowsCache = [...maintenances];
    //    this.serialRows = maintenances;
    //}


    //onSerialDataLoadFailed(error: any) {
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;

    //    this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
    //        MessageSeverity.error, error);
    //}

    addNewMaintenanceToList() {
        if (this.sourceMaintenance) {
            Object.assign(this.sourceMaintenance, this.editedMaintenance);

            let sourceIndex = this.rowsCache.indexOf(this.sourceMaintenance, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceMaintenance, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedMaintenance = null;
            this.sourceMaintenance = null;
        }
        else {
            let maintenance = new Maintenance();
            Object.assign(maintenance, this.editedMaintenance);
            this.editedMaintenance = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>Maintenance).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, maintenance);
            this.rows.splice(0, 0, maintenance);
        }
        this.loadMaintenanceData();
    }


    loadMaintenanceData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.maintenanceService.getMaintenanceInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(maintenances: Maintenance[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        maintenances.forEach((maintenance, index, maintenances) => {
            (<any>maintenance).index = index + 1;
        });
        this.rowsCache = [...maintenances];
        this.rows = maintenances;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.serialNumber, r.bankName, r.merchant, r.idMerchant, r.idTerminal, r.item, r.applicationFormat, r.status, r.resolvedBy, r.verifiedBy, r.verification));
    }

    onEditorModalHidden() {
        this.editingMaintenanceName = null;
        this.maintenanceEditor.resetForm(true);
    }


    newMaintenance() {
        this.editingMaintenanceName = null;
        this.sourceMaintenance = null;
        this.editedMaintenance = this.maintenanceEditor.newMaintenance(this.allMaintenance);
        this.editorModal.show();
        this.serialListModal.hide();
    }
    newSerial() {
        this.serialListModal.show();
    }
    saveMaintenance(serialKey) {
        this.maintenanceService.newMaintenance(serialKey).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
    }
    private saveSuccessHelper(maintenance?: Maintenance) {
       

        if (maintenance)
            Object.assign(this.serialMaintenanceAdd, maintenance);

        this.alertService.stopLoadingMessage();
        Object.assign(this.maintenance, this.serialMaintenanceAdd);
        this.serialMaintenanceAdd = new MaintenanceEdit();
        //this.resetForm();
        this.alertService.showMessage("Success", `User \"${this.maintenance.id}\" was created successfully`, MessageSeverity.success);   
        //if (this.changesSavedCallback)
        //    this.changesSavedCallback();
        }


    private saveFailedHelper(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        //if (this.changesFailedCallback)
        //    this.changesFailedCallback();
    }
    editMaintenance(row: MaintenanceEdit) {
        this.editingMaintenanceName = { name: row.serialNumber };
        this.sourceMaintenance = row;

        this.editedMaintenance = this.maintenanceEditor.editMaintenance(row, this.allMaintenance, row.id);
        this.editorModal.show();
    }


    deleteMaintenance(row: Maintenance) {
        this.alertService.showDialog('Are you sure you want to Send to stock this Serial Number \"' + row.serialNumber + '\"?', DialogType.confirm, () => this.deleteMaintenanceHelper(row));
    }


    deleteMaintenanceHelper(row: Maintenance) {

       

        this.maintenanceService.deleteMaintenance(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
                this.loadMaintenanceData();
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }
    addDamage(row: Maintenance) {

        this.alertService.showDialog('Are you sure this Pos is damage? \"' + row.serialNumber + '\"?', DialogType.confirm, () => this.retrieveHelper2(row));
    }

    //}
    retrieveHelper2(row: Maintenance) {
       this.damagaPos.applicationVersionId = row.applicationVersionId;
        this.damagaPos.deploymentRequestId = row.deploymentRequestId;
        this.damagaPos.inventoryId = row.inventoryId;
        this.damagaPos.inventoryAddId = row.inventoryAddId;
        this.damagaPos.serialNumberId = row.serialNumberId;
        this.damagaPos.merchant = row.merchant;
        this.damagaPos.idMerchant = row.idMerchant;
        this.damagaPos.idTerminal = row.idTerminal;
        this.damagaPos.outlet = row.outlet;
        this.damagaPos.address = row.address;
        this.damagaPos.district = row.district;
        this.editedDamage = this.damagePosEditor.addDamageStatus(this.damagaPos, row.deploymentRequestId);
        this.statusModal.show();

        //this.alertService.startLoadingMessage("Requesting...");
        //this.loadingIndicator = true;
        //this.damagaPos.applicationVersionId = row.applicationVersionId;
        //this.damagaPos.deploymentRequestId = row.deploymentRequestId;
        //this.damagaPos.inventoryId = row.inventoryId;
        //this.damagaPos.inventoryAddId = row.inventoryAddId;
        //this.damagaPos.serialNumberId = row.serialNumberId;
        //this.posHistoryService.damagePos(this.damagaPos, row.deploymentRequestId)
        //    .subscribe(results => {
        //        this.alertService.stopLoadingMessage();
        //        this.loadingIndicator = false;

        //        this.rowsCache = this.rowsCache.filter(item => item !== row)
        //        this.rows = this.rows.filter(item => item !== row)
        //    },
        //    error => {
        //        this.alertService.stopLoadingMessage();
        //        this.loadingIndicator = false;

        //        this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
        //            MessageSeverity.error, error);
        //    });
    }
  
    onEditorStatusModalHidden() {
        this.damagePosEditor.resetForm(true);
    }

    get canCreateUsers() {
        return this.maintenanceService.userHasPermission(Permission.createUsersPermission);
    }

    get canViewUsers() {
        return this.maintenanceService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.maintenanceService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.maintenanceService.userHasPermission(Permission.manageUsersPermission);
    }

}
