﻿
import { Component, AfterViewInit, OnInit, ViewChild, TemplateRef, Input, OnDestroy } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { DeploymentService } from "../../services/deployment/deployment.service";
import { InventoryAddService } from "../../services/inventory-add.service";
import { PosHistoryService } from "../../services/posHistory/pos-history.service";
import { OtherStockService } from "../../services/otherstock/otherstock.service";
import { Utilities } from '../../services/utilities';
import { OtherStock } from '../../models/otherstock/otherstock.model';
import { OtherStockEdit } from '../../models/otherstock/otherstock-edit.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { PosHistoryComponent } from "../posHistory/history.component";
import { OtherStockListComponent } from "../otherstock/otherstock.component";
import { InventoryAddListComponent } from "../inventory/inventory-add-list.component";
import { Inventory } from '../../models/inventory.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { OtherParty } from '../../models/otherparty/otherparty.model';
import * as moment from 'moment';

@Component({
    selector: 'otherstock-edit',
    templateUrl: './otherstock-edit.component.html',
    styleUrls: ['./otherstock.component.css']
})
export class OtherStockEditComponent implements OnInit {
    columns: any[] = [];
    columns1: any[] = [];
    rowsInventoryAdd: InventoryAdd[] = [];
    rowsInventoryAddCache: InventoryAdd[] = [];
    private inventoryAdd: InventoryAdd = new InventoryAdd();
    private inventory: Inventory = new Inventory();
    rows: OtherParty[] = [];
    rowsCache: OtherParty[] = [];

    private isEditMode = false;
    private isNewOtherStock = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingOtherStockName: string;
    private uniqueId: string = Utilities.uniqueId();
    private otherStock: OtherStock = new OtherStock();
  
    //private inventoryAdd: InventoryAdd = new InventoryAdd();
    //private serial: SerialNo = new SerialNo();
    ////private inventorys: Inventory = new Inventory();
    //private serialAdd: SerialNo;
   

    private allOtherStock: OtherStock[] = [];
    private otherStockEdit: OtherStockEdit = new OtherStockEdit();
    loadingIndicator: boolean;

    //allOtherStock: OtherStock[] = [];
    //allSerial: SerialNo[] = [];
    editedOtherStock: OtherStockEdit;
    sourceOtherStock: OtherStockEdit;

    public formResetToggle = true;

    public changesOtherStockSavedCallback: () => void;
    public changesOtherStockFailedCallback: () => void;
    public changesOtherStockCancelledCallback: () => void;

  

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    @ViewChild('deploymentRequestId')
    private deploymentRequestId;

    @ViewChild('otherPartyId')
    private otherPartyIdtId;

    @ViewChild('inventoryAddId')
    private inventoryAddId;

    @ViewChild('otherStockId')
    private otherStockId;

    //@ViewChild('totalNumber')
    ////private totalNumber;

    @ViewChild('soldOut')
    private soldOut;


    @ViewChild('remarks')
    private remarks;


    @ViewChild('receivedBy')
    private receivedBy;


    @ViewChild('contactNo')
    private contactNo;


    @ViewChild('distributedBy')
    private distributedBy;


    @ViewChild('posHistoryEditor')
    posHistoryEditor: PosHistoryComponent;

    @ViewChild('inventoryAddListEditor')
    inventoryAddListEditor: InventoryAddListComponent;

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('index1Template')
    index1Template: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate1')
    actionsTemplate1: TemplateRef<any>;


    @ViewChild('primaryItemTemplate')
    primaryItemTemplate: TemplateRef<any>;

    @ViewChild('secondaryItemTemplate')
    secondaryItemTemplate: TemplateRef<any>;

    @ViewChild('tertiaryItemTemplate')
    tertiaryItemTemplate: TemplateRef<any>;

    @ViewChild('totalNumberTemplate')
    totalNumberTemplate: TemplateRef<any>;

    @ViewChild('remainingTemplate')
    remainingTemplate: TemplateRef<any>;

    @ViewChild('boughtDateTemplate')
    boughtDateTemplate: TemplateRef<any>;

    @ViewChild('partyNameTemplate')
    partyNameTemplate: TemplateRef<any>;

    @ViewChild('partyAddresTemplate')
    partyAddressTemplate: TemplateRef<any>;

    @ViewChild('phoneNoTemplate')
    phoneNoTemplate: TemplateRef<any>;


    constructor(private alertService: AlertService, private translationService: AppTranslationService, private otherStockService: OtherStockService, private inventoryAddService: InventoryAddService, private deploymentServive: DeploymentService) {
    }

    ngOnInit() {
            this.loadInventoryAddData();
            //this.loadOtherPartyData();
        }
    

    private loadInventoryAddData() {
     
            this.inventoryAddService.getAddInventory1Info().subscribe(results => this.onInventoryDataLoadSuccessful(results), error => this.onInventoryAddDataDataLoadFailed(error));
    }

    
        
   
    private onInventoryDataLoadSuccessful(inventorys: InventoryAdd[]) {
       
        inventorys.forEach((inv, index, inventorys) => {
            
                (<any>inv).index = index + 1;
            
        });
        let gT = (key: string) => this.translationService.getTranslation(key);
        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'primaryItem', name: "Stock Item", width: 140, cellTemplate: this.primaryItemTemplate },
            //{ prop: 'secondaryItem', name: "Sub Item", width: 50, cellTemplate: this.secondaryItemTemplate },
            //{ prop: 'tertiaryItem', name: "Tertiary Item", width: 50, cellTemplate: this.tertiaryItemTemplate },
            { prop: 'totalNumber', name: "Total", width: 50, cellTemplate: this.totalNumberTemplate },
            { prop: 'remainingNumber', name: "Remaining", width: 75, cellTemplate: this.remainingTemplate },
            //{ prop: 'buyDate', name: gT('inventory.management.BuyDate'), width: 80, cellTemplate: this.boughtDateTemplate },
        ];

        if (this.canManageUsers)
            this.columns.push({ name: '', width: 80, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });


            this.rowsInventoryAddCache = [...inventorys];
            this.rowsInventoryAdd = inventorys;



            this.alertService.stopLoadingMessage();
            this.loadingIndicator = false;
      
          
        }
    public item(params) {
        return (params.data.primaryItem + " - " + params.data.secondaryItem + " - " + params.data.tertiaryItem + " - " + params.data.detailItem);
    }
    private onInventoryAddDataDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

    }
 


    //private loadOtherPartyData() {
    //    this.otherStockService.getOtherPartyInfo().subscribe(results => this.onOtherPartyDataLoadSuccessful(results), error => this.onOtherPartyDataDataLoadFailed(error));
    //}


    //private onOtherPartyDataLoadSuccessful(partys: OtherParty[]) {
    //    partys.forEach((part, index, partys) => {
    //        (<any>part).index = index + 1;
    //    });
    //    this.rowsCache = [...partys];
    //    this.rows = partys;
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;
    //    let gT = (key: string) => this.translationService.getTranslation(key);
    //    this.columns1 = [
    //        { prop: "index", name: '#', width: 40, cellTemplate: this.index1Template, canAutoResize: false },
    //        { prop: 'partyName', name: gT('otherstock.management.PartyName'), width: 140, cellTemplate: this.partyNameTemplate },
    //        { prop: 'partyAddress', name: gT('otherstock.management.PartyAddress'), width: 80, cellTemplate: this.partyAddressTemplate },
    //        //{ prop: 'phoneNo', name: gT('otherstock.management.PhoneNo'), width: 80, cellTemplate: this.phoneNoTemplate },
    //    ];

    //    if (this.canManageUsers)
    //        this.columns1.push({ name: '', width: 80, cellTemplate: this.actionsTemplate1, resizeable: false, canAutoResize: false, sortable: false, draggable: false });
      
    //}

    //private onOtherPartyDataDataLoadFailed(error: any) {
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;
    //    this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
    //        MessageSeverity.error, error);

    //    //this.inventoryAdd = new InventoryAdd();
    //}

    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

    
    private edit(id) {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.otherStockEdit.deploymentRequestId = id;
            //this.otherStockEdit = new OtherStockEdit();
            Object.assign(this.otherStockEdit, this.otherStock);
        }
        else {
            if (!this.otherStockEdit) {
                //this.otherStockEdit = new OtherStockEdit();
            }
                
                     this.otherStockEdit.deploymentRequestId = id;
            this.isEditingSelf = this.otherStockService.updateOtherStock(this.otherStock) ? this.otherStockEdit.id == this.otherStock.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveOtherStock() {
        this.isSaving = true; 
        this.alertService.startLoadingMessage("Saving changes...");
        //this.otherStockEdit.deploymentRequestId = id;
        if (this.isNewOtherStock) {
            this.otherStockService.newOtherStock(this.otherStockEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.otherStockService.updateOtherStock(this.otherStockEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
       
    }


    private saveSuccessHelper(otherstock?: OtherStockEdit) {
        //this.testIsConfigurationChanged(this.configuration, this.configurationEdit);

        if (otherstock)
            Object.assign(this.otherStockEdit, otherstock);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.otherStock, this.otherStockEdit);
        this.otherStockEdit = new OtherStockEdit();
        this.otherStockEdit = new OtherStockEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewOtherStock)
                this.alertService.showMessage("Success",  `Stock was saved successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Stock was saved successfully was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesOtherStockSavedCallback)
            this.changesOtherStockSavedCallback();
        this.loadInventoryAddData();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesOtherStockFailedCallback)
            this.changesOtherStockFailedCallback();
    }


    private cancel() {
        if (this.isGeneralEditor)
            this.otherStockEdit = this.otherStock = new OtherStockEdit();
        else
            this.otherStockEdit = new OtherStockEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesOtherStockCancelledCallback)
            this.changesOtherStockCancelledCallback();
    }


    private close() {
        this.otherStockEdit = this.otherStock = new OtherStockEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesOtherStockCancelledCallback)
            this.changesOtherStockCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newOtherStock(allOtherStock: OtherStock[],id) {
        this.isGeneralEditor = true;
        this.isNewOtherStock = true;
        this.otherStockEdit.deploymentRequestId = id;
        this.allOtherStock = [...allOtherStock];
        //this.editingConfigurationName = null;
        this.otherStock = this.otherStockEdit = new OtherStockEdit();
        //this.posEdit.isEnabled = true;
        this.edit(id);
        //this.saveOtherStock(id);
        return this.otherStockEdit;
    }
  


    editOtherStock(otherstock: OtherStock, allOtherStock: OtherStock[]) {
        if (otherstock) {
            this.isGeneralEditor = true;
            this.isNewOtherStock = false;
            //this.editingConfigurationName = configuration.id;
            this.otherStock = new OtherStock();
            this.otherStockEdit = new OtherStockEdit();
            Object.assign(this.otherStock, otherstock);
            Object.assign(this.otherStockEdit, otherstock);
            //this.edit();
            //this.loadCurrentInventoryAddData(-1, -1, this.configuration.deploymentRequestId);
            return this.otherStockEdit;
            //loadOtherStock(id);
        }
        else {
            //return this.newOtherStock(allOtherStock);
        }
    }

    onSearchChanged(value: string) {
        this.rowsInventoryAdd = this.rowsInventoryAddCache.filter(r => Utilities.searchArray(value, false, r.primaryItem, r.secondaryItem, r.tertiaryItem, r.detailItem));
    }

    onSearchChangedParty(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.partyName, r.partyAddress, r.phoneNo));
    }

    get canViewAllRoles() {
        return this.otherStockService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.otherStockService.userHasPermission(Permission.assignRolesPermission);
    }
    get canManageUsers() {
        return this.otherStockService.userHasPermission(Permission.manageUsersPermission);
    }
}
