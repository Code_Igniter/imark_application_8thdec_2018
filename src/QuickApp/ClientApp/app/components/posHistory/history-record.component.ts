﻿import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { PosHistory } from "../../models/posHistory/pos-history.model";
import { PosHistoryEdit } from '../../models/posHistory/pos-edit.model';
import * as moment from 'moment';
import * as $ from 'jquery';

@Component({
    selector: 'individual-record',
    templateUrl: './history-record.component.html',
    styleUrls: ['./history.component.css']
})
export class PosHistoryRecordComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;
    rows: PosHistory[] = [];
    rowsCache: PosHistory[] = [];
    @Input()
    isGeneralEditor = false;
    private posHistoryEdit: PosHistoryEdit = new PosHistoryEdit();
    private posHistory: PosHistory = new PosHistory();
  
    editedPosHistory: PosHistoryEdit;
    sourcePosHistory: PosHistoryEdit;
  
    loadingIndicator: boolean;

    allPosHistory: PosHistory[] = [];
    private historyId;


    @ViewChild('individualHistoryModal')
    individualHistoryModal: ModalDirective;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private poshistoryService: PosHistoryService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);


        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 180, suppressSizeToFit: true },
            { headerName: 'Merchant', field: 'merchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'District', field: 'district', width: 150, suppressSizeToFit: true },
            { headerName: 'Outlet', field: 'outlet', width: 150, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 150, suppressSizeToFit: true },
            { headerName: 'Deployed Location', field: 'merchantLocation', width: 150, suppressSizeToFit: true },
            { headerName: 'Contact Person', field: 'contactPerson', width: 150, suppressSizeToFit: true },
            { headerName: 'Contact No.', field: 'contactNo', width: 150, suppressSizeToFit: true },
            { headerName: 'Received By', field: 'receivedBy', width: 150, suppressSizeToFit: true },
            { headerName: 'Received Contact No.', field: 'longitude', width: 180, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Connectivity', field: 'connectivity', width: 250, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            { headerName: 'Property', field: 'isSold', width: 150, suppressSizeToFit: true, cellRenderer: this.property },
            { headerName: 'Application Version', field: 'applicationDateVersion', width: 150, suppressSizeToFit: true },
            { headerName: 'Serial No Status', field: 'isReceived', width: 200, suppressSizeToFit: true },
            { headerName: 'Status', field: 'scanned', width: 180, suppressSizeToFit: true },
            { headerName: 'Deployed Remarks', field: 'remarks', width: 150, suppressSizeToFit: true },
            {
                headerName: 'Deployed Date', field: 'deployDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },
            { headerName: 'Created By', field: 'createdBy', width: 150, suppressSizeToFit: true },
            {
                headerName: 'Created Date', field: 'createdDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },




        ];
        //this.loadHistoryData(this.posHistoryEdit.deploymentRequestId);

    }
    public tooltipRenderer(params) {
        return ('<span title="' + params.value + '" >' + params.value + '</span>')
    }
    public item(params) {
        return (params.data.primaryItem + " - " + params.data.secondaryItem + " - " + params.data.tertiaryItem + " - " + params.data.detailItem);
    }
    public item1(params) {
        return (params.data.applicationVersion + " - " + params.data.applicationDate.slice(0, 10).split('-'));
    }
    //public item2(params) {
    //    if (params.data.isReceived == "") {
    //        return (`<span style="background-color:red;color:white">NOT RECEIVED YET</span>`);
    //    }
    //    else if (params.data.isReceived == "Received") {
    //        return (`<b>RECEIVED</b>`);
    //    }
    //    else {
    //        return (`<b>IN USE</b>`);
    //    }
    //}
    
    ngAfterViewInit() {
    }
    public property(params) {
        if (params.data.isSold == false) {
            return (`<span style="font-size:12px">IMARK PROPERTY</span>`)
        }
        {
            return (`<span style="font-size:12px">BANK PROPERTY</span>`)
        }
    }
  
    //public setReturnValue(dateRange) {
    //    let startDate = dateRange['from'];
    //    let endDate = dateRange['to'];
    //    let sDate = moment(startDate);
    //    let eDate = moment(endDate);
    //    let result = this.rows.filter(function (posHistory, index, posHistorys) {
    //        if (moment(posHistory.deployDate) >= sDate && moment(posHistory.deployDate) <= eDate) {
    //            (<any>posHistory).index = index + 1;
    //            return posHistory;
    //        }
    //        else {
    //            return false;
    //        }
    //    });
    //    return this.rows = result;
    //}
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;


        this.poshistoryService.getIndividualHistoryInfo(-1, -1, this.historyId).subscribe(data => {
            params.api.setRowData(data);
        });
    }
  
    loadHistoryData(id) {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.poshistoryService.getIndividualHistoryInfo(-1, -1, id).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(historys: PosHistory[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        historys.forEach((posHistory, index, historys) => {

            (<any>posHistory).index = index + 1;
        });
        this.rowsCache = [...historys];
        this.rows = historys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }

    


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.serialKey, r.merchant, r.idMerchant, r.idTerminal, r.bankName, r.primaryItem, r.secondaryItem, r.tertiaryItem, r.detailItem, r.createdBy));
    }



    loadHistoryList(allPosHistory: PosHistory[], id) {
        this.isGeneralEditor = true;
        this.posHistoryEdit.deploymentRequestId = id;
        this.historyId = id;
        this.allPosHistory = [...allPosHistory];
        this.posHistory = this.posHistoryEdit = new PosHistoryEdit();
        this.loadHistoryData(id);
        return this.posHistoryEdit;
    }



    get canViewUsers() {
        return this.poshistoryService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.poshistoryService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.poshistoryService.userHasPermission(Permission.manageUsersPermission);
    }

}

