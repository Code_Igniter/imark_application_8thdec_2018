﻿import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { DeploymentService } from "../../services/deployment/deployment.service";
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { DeployService } from "../../services/deploy/deploy.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';
import { DeploymentRequestComponent } from "../deployment/deployment-add.component";
import { ConfigurationEdit } from '../../models/configuration/configuration-edit.model';
import { Configuration } from '../../models/configuration/configuration.model';
import { ConfigurationEditComponent } from '../configuration/configuration-edit.component';
import { Deploy } from '../../models/deploy/deploy.model';
import { DeployEdit } from '../../models/deploy/deploy-edit.model';
import { DeployEditComponent } from '../deploy/deploy-edit.component';
import { PosHistory } from "../../models/posHistory/pos-history.model";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { OtherStockEditComponent } from '../../components/otherstock/otherstock-edit.component';
import { OtherStockListComponent } from '../../components/otherstock/otherstock.component';
import { SupportEditComponent } from '../../components/support/support-edit.component';
import { SupportComponent } from '../../components/support/support.component';
import { PosHistoryRecordComponent } from '../../components/posHistory/history-record.component';
import { OtherStock } from '../../models/otherstock/otherstock.model';
import { OtherStockEdit } from '../../models/otherstock/otherstock-edit.model';
import { Support } from '../../models/support/support.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { PosHistoryEdit } from '../../models/posHistory/pos-edit.model';


import * as moment from 'moment';
import * as $ from 'jquery';

//import "ag-grid-enterprise";


@Component({
    selector: 'pos-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.css']
})
export class PosHistoryComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;
    
    columns: any[] = [];
    rows: PosHistory[] = [];
    rowsCache: PosHistory[] = [];
    @Input()
    isGeneralEditor = false;
 
    private other: OtherStock = new OtherStock();
    private support: Support = new Support();
    private posHistory: DeploymentRequest = new DeploymentRequest();
    editedOtherStock: OtherStockEdit;
    sourceOtherStock: OtherStockEdit;

    editedSupport: SupportEdit;
    sourceSupport: SupportEdit;

    editedPosHistory: PosHistoryEdit;
    editedPosHistory1: PosHistoryEdit;
    sourcePosHistory: PosHistoryEdit;

    allOtherStock: OtherStock[] = [];
    allSupport: Support[] = [];
    loadingIndicator: boolean;

    allPosHistory: PosHistory[] = [];
    allPosHistory1: PosHistory[] = [];

    

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('supportModal')
    supportModal: ModalDirective;

    @ViewChild('rollModal')
    rollModal: ModalDirective;

    @ViewChild('individualHistoryModal')
    individualHistoryModal: ModalDirective;

    @ViewChild('supportEditor')
    supportEditor: SupportEditComponent;

    @ViewChild('supportListEditor')
    supportListEditor: SupportComponent;

    @ViewChild('otherStockEditor')
    otherStockEditor: OtherStockEditComponent;

    @ViewChild('stockListEditor')
    stockListEditor: OtherStockListComponent;

    @ViewChild('historyRecordEditor')
    historyRecordEditor: PosHistoryRecordComponent;


    //@ViewChild('stockListEditor')
    //stockListEditor: OtherStockListComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private poshistoryService: PosHistoryService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

       
        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 180, suppressSizeToFit: true },
            { headerName: 'Merchant', field: 'merchant', width: 250, suppressSizeToFit: true},
            { headerName: 'Merchant ID', field: 'idMerchant', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 150, suppressSizeToFit: true },
            { headerName: 'Deployed Location', field: 'merchantLocation', width: 180, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Connectivity', field: 'connectivity', width: 150, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            { headerName: 'Property', field: 'isSold', width: 150, suppressSizeToFit: true, cellRenderer: this.property },
            { headerName: 'Previous Serial No.', field: 'previousSerial', width: 180, suppressSizeToFit: true, cellRenderer: this.previousSerial },
            { headerName: 'Configuration Status', field: 'configStatus', width: 180, suppressSizeToFit: true, cellRenderer: this.configStatus },
         
            { headerName: 'Contact Person', field: 'contactPerson', width: 150, suppressSizeToFit: true },
            { headerName: 'Contact No', field: 'contactNo1', width: 150, suppressSizeToFit: true },
            { headerName: 'Received By', field: 'receivedBy', width: 150, suppressSizeToFit: true },
            { headerName: 'Received Contact No', field: 'receivedContactNo', width: 180, suppressSizeToFit: true },
          
            { headerName: 'Created By', field: 'createdBy', width: 150, suppressSizeToFit: true },
            { headerName: 'Deployed By', field: 'verifiedBy', width: 150, suppressSizeToFit: true },
           
            {
                headerName: 'Deployed Date', field: 'deployDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },

            
           
        ];
        if (this.canCreateUsers)
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: '',
                suppressSorting: true,
                width: 300,

                //cellRenderer: this.canViewUsers,
                template:
                    `
                <button type="button" data-action-type="Support"  class="btn btn-link btn-xs" style="background-color:maroon;color:white;text-decoration:none">
               Support
             </button>
               <button type="button" data-action-type="History" class="btn btn-link btn-xs"style="background-color:maroon;color:white;text-decoration:none">
               History
             </button>
             <button type="button" data-action-type="Roll" class="btn btn-link btn-xs"style="background-color:maroon;color:white;text-decoration:none">
               Roll Distribution
             </button>`
            });

        //this.addNewPosHistoryToList();
        this.loadPosHistorysData();
    }
    public previousSerial(params) {
        if (params.data.previousSerial == 'notreceived') {
            return (`<span style="font-size:12px;background-color:maroon;color:white">NOT RECEIVED</span>`)
        }
        else if (params.data.previousSerial == 'received') {
            return (`<span style="font-size:12px">RECEIVED</span>`)
        }
        else {
            return (`<span style="font-size:12px">USING SAME SERIAL NO</span>`);
        }

    }
    public configStatus(params) {
        if (params.data.configStatus == null) {
            return (`<span >--------</span>`)
        }
        {
            return (params.data.configStatus);
        }
    }
    public property(params) {
        if (params.data.isSold == false) {
            return (`<span style="font-size:12px">IMARK PROPERTY</span>`)
        }
        {
            return (`<span style="font-size:12px">BANK PROPERTY</span>`)
        }
    }
  
   
   

    public updated(params) {
        if (params.data.verifiedBy == null) {
            return ('NA');
        }
        else {
            return (params.data.verifiedBy);
        }
    }


    ngAfterViewInit() {

        this.otherStockEditor.changesOtherStockSavedCallback = () => {
            this.addNewPosHistoryToList();

            this.rollModal.hide();
        };

        this.otherStockEditor.changesOtherStockCancelledCallback = () => {
            this.editedOtherStock = null;
            this.sourceOtherStock = null;
            this.rollModal.hide();
        };
        this.supportEditor.changesSavedCallback = () => {
            this.addNewPosHistoryToList();
            this.supportModal.hide();
        };

        this.supportEditor.changesCancelledCallback = () => {
            this.editedSupport = null;
            this.sourceSupport = null;
            this.supportModal.hide();
        };
        this.historyRecordEditor.changesSavedCallback = () => {
            this.addNewPosHistoryToList();
            this.individualHistoryModal.hide();
        };

        this.historyRecordEditor.changesCancelledCallback = () => {
            this.addNewPosHistoryToList();
            this.individualHistoryModal.hide();
        };

       
    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
               
                case "Support":
                    return this.onSupportClick(data);
                case "History":
                    return this.onHistoryClick(data);
                case "Roll":
                    return this.onRollClick(data);
            }
        }
    }
    public onSupportClick(data: PosHistoryEdit) {
        this.editedSupport = this.supportEditor.newSupport(this.allSupport, data.deploymentRequestId);
        this.poshistoryService.getPosHistory(data.deploymentRequestId).subscribe(results => this.onLoadSuccessful(results), error => this.onLoadFailed(error));
        this.loadSupportList(data.deploymentRequestId);
        this.supportModal.show();
    }
    public loadSupportList(id) {
      
        this.editedSupport = this.supportListEditor.loadSupportList(this.allSupport, id);
    }
    onLoadSuccessful(posHistory: DeploymentRequest) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.posHistory = posHistory;
        //this.editConfiguration(configuration);

    }


    onLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    private onHistoryClick(data: PosHistoryEdit) {
        //this.editedPosHistory = this.historyRecordEditor.loadHistoryList(this.allPosHistory, data.deploymentRequestId);
        
        this.loadHistoryList(data.deploymentRequestId);
        this.individualHistoryModal.show();
    }
    public loadHistoryList(id) {

        this.editedPosHistory1 = this.historyRecordEditor.loadHistoryList(this.allPosHistory1, id);
       
    }
    public onRollClick(data: PosHistoryEdit) {
        this.other.deploymentRequestId = data.id;
        this.editedOtherStock = this.otherStockEditor.newOtherStock(this.allOtherStock, data.deploymentRequestId);
        this.poshistoryService.getPosHistory(data.deploymentRequestId).subscribe(results => this.onLoadSuccessful(results), error => this.onLoadFailed(error));
        this.rollModal.show();
        this.loadStockList(data.deploymentRequestId);
        //this.rollModal.show();
    }
    public loadStockList(id) {

        this.editedOtherStock = this.stockListEditor.loadStockList(this.allOtherStock, id);
    }
    onRollModalHidden() {
        this.otherStockEditor.resetForm(true);
    }
    //onEditorModalHidden() {
    //    this.otherStockEditor.resetForm(true);
    //}
    supportModalHidden() {
        this.supportEditor.resetForm(true);
    }
    individualHistoryModalHidden() {
        //this.historyRecordEditor.resetForm(true);
        this.individualHistoryModal.hide();
    }
    public setReturnValue(dateRange) {
        let startDate = dateRange['from'];
        let endDate = dateRange['to'];
        let sDate = moment(startDate);
        let eDate = moment(endDate);
        let result = this.rows.filter(function (posHistory, index, posHistorys) {
            if (moment(posHistory.deployDate) >= sDate && moment(posHistory.deployDate) <= eDate) {
                (<any>posHistory).index = index + 1;
                return posHistory;
            }
            else {
                return false;
            }
        });
        return this.rows = result;
    }
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.poshistoryService.getPosHistoryInfo().subscribe(data => {
            params.api.setRowData(data);
        });

    }
   
    addNewPosHistoryToList() {
        if (this.sourcePosHistory) {
            Object.assign(this.sourcePosHistory, this.editedPosHistory);

            let sourceIndex = this.rowsCache.indexOf(this.sourcePosHistory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourcePosHistory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedPosHistory = null;
            this.sourcePosHistory = null;
        }
        else {
            let pos = new PosHistory();
            Object.assign(pos, this.editedPosHistory);
            this.editedPosHistory = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>PosHistory).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, pos);
            this.rows.splice(0, 0, pos);
        }
        this.loadPosHistorysData();
    }


    loadPosHistorysData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.poshistoryService.getPosHistoryInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(posHistorys: PosHistory[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        posHistorys.forEach((posHistory, index, posHistorys) => {
            (<any>posHistory).index = index + 1;
        });
        this.rowsCache = [...posHistorys];
        this.rows = posHistorys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }



    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.serialKey, r.merchant, r.idMerchant, r.idTerminal, r.bankName, r.district, r.address, r.contactPerson, r.merchantLocation, r.outlet, r.receivedBy, r.requestName, r.statusName, r.previousSerial));
    }



    deletePosHistory(row: PosHistory) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.bankName + '\"?', DialogType.confirm, () => this.deletePosHistoryHelper(row));
    }


    deletePosHistoryHelper(row: PosHistory) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.poshistoryService.deletePosHistory(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }

    get canCreateUsers() {
        return this.poshistoryService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewUsers() {
        return this.poshistoryService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.poshistoryService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.poshistoryService.userHasPermission(Permission.manageUsersPermission);
    }

}

