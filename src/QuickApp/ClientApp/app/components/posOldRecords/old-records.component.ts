﻿import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { PosHistory } from "../../models/posHistory/pos-history.model";
import * as moment from 'moment';
@Component({
    selector: 'old-records',
    templateUrl: './old-records.component.html',
    styleUrls: ['./old-records.component.css']
})
export class OldRecordComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;
    columns: any[] = [];
    rows: PosHistory[] = [];
    rowsCache: PosHistory[] = [];
    loadingIndicator: boolean;

    allPosHistory: PosHistory[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('bankNameTemplate')
    bankNameTemplate: TemplateRef<any>;

    @ViewChild('merchantTemplate')
    merchantTemplate: TemplateRef<any>;

    @ViewChild('idMerchantTemplate')
    idMerchantTemplate: TemplateRef<any>;

    @ViewChild('idTerminalTemplate')
    idTerminalTemplate: TemplateRef<any>;

    @ViewChild('outletTemplate')
    outletTemplate: TemplateRef<any>;

    @ViewChild('contactPersonTemplate')
    contactPersonTemplate: TemplateRef<any>;

    @ViewChild('contactNoTemplate')
    contactNoTemplate: TemplateRef<any>;

    @ViewChild('districtTemplate')
    districtTemplate: TemplateRef<any>;

    @ViewChild('addressTemplate')
    addressTemplate: TemplateRef<any>;

    @ViewChild('applicationVersionTemplate')
    applicationVersionTemplate: TemplateRef<any>;

    @ViewChild('applicationDateTemplate')
    applicationDateTemplate: TemplateRef<any>;

    @ViewChild('serialKeyTemplate')
    serialKeyTemplate: TemplateRef<any>;

    @ViewChild('receivedByTemplate')
    receivedByTemplate: TemplateRef<any>;

    @ViewChild('remarksDeployTemplate')
    remarksDeployTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('createdByTemplate')
    createdByTemplate: TemplateRef<any>;

    @ViewChild('updatedByTemplate')
    updatedByTemplate: TemplateRef<any>;

    @ViewChild('deployDateTemplate')
    deployDateTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;


    constructor(private alertService: AlertService, private translationService: AppTranslationService, private poshistoryService: PosHistoryService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 180, suppressSizeToFit: true },
            { headerName: 'Merchant', field: 'merchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'District', field: 'district', width: 150, suppressSizeToFit: true },
            { headerName: 'Outlet', field: 'outlet', width: 150, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 150, suppressSizeToFit: true },
            { headerName: 'Deployed Location', field: 'merchantLocation', width: 150, suppressSizeToFit: true },
            { headerName: 'Contact Person', field: 'contactPerson', width: 150, suppressSizeToFit: true },
            { headerName: 'Contact No.', field: 'contactNo', width: 150, suppressSizeToFit: true },
            { headerName: 'Received By', field: 'receivedBy', width: 150, suppressSizeToFit: true },
            { headerName: 'Received Contact No.', field: 'longitude', width: 180, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Connectivity', field: 'connectivity', width: 250, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            { headerName: 'Property', field: 'isSold', width: 150, suppressSizeToFit: true, cellRenderer:this.property },
            { headerName: 'Application Version', field: 'applicationDateVersion', width: 150, suppressSizeToFit: true },
            { headerName: 'Serial No Status', field: 'isReceived', width: 200, suppressSizeToFit: true},
           // { headerName: 'Status', field: 'scanned', width: 180, suppressSizeToFit: true },
            { headerName: 'Remarks', field: 'remarks', width: 150, suppressSizeToFit: true },
            {
                headerName: 'Retrieved Date', field: 'createdDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },

            {
                headerName: 'Deployed Date', field: 'deployDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },
            { headerName: 'Created By', field: 'createdBy', width: 150, suppressSizeToFit: true },
            
         
        ];
        this.loadPosRecordsData();
    }


    ngAfterViewInit() {


    }
    public property(params) {
        if (params.data.isSold == false) {
            return (`<span style="font-size:12px">IMARK PROPERTY</span>`)
        }
        {
            return (`<span style="font-size:12px">BANK PROPERTY</span>`)
        }
    }

    //public isReceived(params) {
    //    if (params.data.isReceived == "POSRECEIVED") {
    //        return ('<span style="font-size:15px">POS RECEIVED</span>');
    //    }
    //    else if (params.data.isReceived == "POSNOTRECEIVED") {
    //        return ('<span style="font-size:15px">POS NOT RECEIVED</span>')
    //    }
    //    else {
    //        return ('<span style="font-size:15px">IN USE</span>')
    //    }
    //}

    public status(params) {
        if (params.data.scanned != null) {
            return (params.data.scanned);
        }
        else {
            return ('<span style="font-size:15px">----------</span>')
        }
    }


    loadPosRecordsData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.poshistoryService.getPosRecordInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(posHistorys: PosHistory[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        posHistorys.forEach((posHistory, index, posHistorys) => {
            (<any>posHistory).index = index + 1;
        });
        this.rowsCache = [...posHistorys];
        this.rows = posHistorys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }

    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.poshistoryService.getPosRecordInfo().subscribe(data => {
            params.api.setRowData(data);
        });

    }
    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.idMerchant, r.idTerminal, r.bankName, r.district, r.serialKey, r.merchant, r.address, r.contactPerson, r.receivedBy, r.merchantLocation, r.createdBy, r.isReceived, r.item));
    }



    get canViewUsers() {
        return this.poshistoryService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.poshistoryService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.poshistoryService.userHasPermission(Permission.manageUsersPermission);
    }

}

