﻿
import { Component, Injectable, Injector, OnInit } from '@angular/core';


import * as $ from 'jquery';


@Component({
    selector: 'side-bar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css'],
})

//@Injectable()
export class SideBarComponent implements OnInit {
    ngOnInit() {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    }

}
