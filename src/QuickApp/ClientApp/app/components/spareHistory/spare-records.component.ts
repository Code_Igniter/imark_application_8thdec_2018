﻿import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { PosHistory } from "../../models/posHistory/pos-history.model";
import * as moment from 'moment';
import { OtherStock } from '../../models/otherstock/otherstock.model';
import { OtherStockEdit } from '../../models/otherstock/otherstock-edit.model';
import { OtherStockEditComponent } from "../otherstock/otherstock-edit.component";
import { OtherStockService } from "../../services/otherstock/otherstock.service";

@Component({
    selector: 'spare-records',
    templateUrl: './spare-records.component.html',
    styleUrls: ['./old-records.component.css']
})
export class SpareRecordComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;
    columns: any[] = [];
    rows: OtherStock[] = [];
    rowsCache: OtherStock[] = [];

    editedOtherStock: OtherStockEdit;
    sourceOtherStock: OtherStockEdit;
    editingOtherStockName: {};
    loadingIndicator: boolean;

    private otherStockEdit: OtherStockEdit = new OtherStockEdit();
    allOtherStock: OtherStock[] = [];
    private otherStock: OtherStock = new OtherStock();

    @Input()
    isGeneralEditor = false;

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('bankTemplate')
    bankTemplate: TemplateRef<any>;

    @ViewChild('merchantTemplate')
    merchantTemplate: TemplateRef<any>;

    @ViewChild('merchantIdTemplate')
    merchantIdTemplate: TemplateRef<any>;

    @ViewChild('terminalIdTemplate')
    terminalIdTemplate: TemplateRef<any>;

    @ViewChild('outletTemplate')
    outletTemplate: TemplateRef<any>;

    @ViewChild('inventoryTemplate')
    inventoryTemplate: TemplateRef<any>;

    @ViewChild('soldOutTemplate')
    soldOutTemplate: TemplateRef<any>;

    @ViewChild('remainingTemplate')
    remainingTemplate: TemplateRef<any>;

    @ViewChild('stockDateTemplate')
    stockDateTemplate: TemplateRef<any>;

    @ViewChild('partyNameTemplate')
    partyNameTemplate: TemplateRef<any>;

    @ViewChild('partyAddressTemplate')
    partyAddressTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('otherStockEditor')
    otherStockEditor: OtherStockEditComponent;


    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService, private otherStockService: OtherStockService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columnDefs = [
            {


                headerName: 'Bank Name', field: 'bankName'
            },
            { headerName: 'Merchant', field: 'merchant' },
            { headerName: 'Merchant ID', field: 'idMerchant' },
            { headerName: 'Terminal ID', field: 'idTerminal' },
            { headerName: 'Outlet', field: 'outlet' },
            { headerName: 'Terminal Type', field: '', width: 250, suppressSizeToFit: true, enableTooltip: true, cellRenderer: this.item },
            { headerName: 'Distribute Number', field: 'soldOut' },
            { headerName: 'Received By', field: 'receivedBy' },
            { headerName: 'Contact No', field: 'contactNo' },
            { headerName: 'Distributed By', field: 'distributedBy' },
            { headerName: 'Created By', field: 'createdBy' },
            //{ headerName: 'Remaining', field: 'remaining' },
            {
                headerName: 'Distributed Date', field: 'stockDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },
            { headerName: 'remarks', field: 'remarks' },

            {
                headerName: "Actions",
                suppressMenu: true,
                suppressSorting: true,
                template:
                    `<button type="button" data-action-type="DeleteStock" class="btn btn-link btn-xs">
              Delete
             </button>`
            }
        ];

        //this.addNewOtherStockToList();
        //this.loadStockData(this.otherStockEdit.deploymentRequestId);
        this.loadStockData('deploymentRequestId');
    }



    public item(params) {
        return (params.data.primaryItem + " - " + params.data.secondaryItem + " - " + params.data.tertiaryItem + " - " + params.data.detailItem);
    }

    ngAfterViewInit() {
        //this.addNewOtherStockToList();
        //this.otherStockEditor.changesOtherStockSavedCallback = () => {
        //    this.addNewOtherStockToList();
        //    this.editorModal.hide();
        //};

        //this.otherStockEditor.changesOtherStockCancelledCallback = () => {
        //    this.editedOtherStock = null;
        //    this.sourceOtherStock = null;
        //    this.editorModal.hide();
        //};

    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "DeleteStock":
                    return this.onActionDeleteClick(data);

            }
        }
    }

    public onActionDeleteClick(data: OtherStockEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + data.bankName + '\"?', DialogType.confirm, () => this.deleteOtherStockHelper(data));
    }

    addNewOtherStockToList() {
        if (this.sourceOtherStock) {
            Object.assign(this.sourceOtherStock, this.editedOtherStock);

            let sourceIndex = this.rowsCache.indexOf(this.sourceOtherStock, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceOtherStock, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedOtherStock = null;
            this.sourceOtherStock = null;
        }
        else {
            let otherStock = new OtherStock();
            Object.assign(otherStock, this.editedOtherStock);
            this.editedOtherStock = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>otherStock).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, otherStock);
            this.rows.splice(0, 0, otherStock);
        }
        this.loadStockData('deploymentRequestId');
    }


    loadStockData(id) {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.otherStockService.getOtherStockInfo(-1, -1, id).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(stocks: OtherStock[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        stocks.forEach((stock, index, stocks) => {
            (<any>stock).index = index + 1;
        });
        this.rowsCache = [...stocks];
        this.rows = stocks;

    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    } loadStockList(allOtherStock: OtherStock[], id) {
        //this.isGeneralEditor = true;
        this.otherStockEdit.deploymentRequestId = id;
        this.allOtherStock = [...allOtherStock];
        this.otherStock = this.otherStockEdit = new OtherStockEdit();
        this.loadStockData(id);
        return this.otherStockEdit;
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.merchant, r.partyName, r.outlet, r.idMerchant, r.idTerminal));
    }
  


    deleteOtherStockHelper(row: OtherStockEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.otherStockService.deleteOtherStock(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
                error => {
                    this.alertService.stopLoadingMessage();
                    this.loadingIndicator = false;

                    this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                        MessageSeverity.error, error);
                });
    }



    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }

   

    get canViewRoles() {
        return this.otherStockService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.otherStockService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.otherStockService.userHasPermission(Permission.manageUsersPermission);
    }

}

