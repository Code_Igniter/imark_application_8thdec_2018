﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { SupportService } from "../../services/support/support.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Support } from '../../models/support/support.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { SupportEditComponent } from "./support-edit.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { Subject } from 'rxjs/Subject';
import * as moment from 'moment';

@Component({
    selector: 'support-management',
    templateUrl: './support.component.html',
    styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    columns: any[] = [];
    rows: Support[] = [];
    rowsCache: Support[] = [];
    private gridApi;
    private gridColumnApi;
    editedSupport: SupportEdit;
    sourceSupport: SupportEdit;
    editingSupportName: {};
    loadingIndicator: boolean;
    private supportEdit: SupportEdit = new SupportEdit();
    private support: Support = new Support();
    allSupport: Support[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('phoneTemplate')
    phoneTemplate: TemplateRef<any>;

    @ViewChild('fieldTemplate')
    fieldTemplate: TemplateRef<any>;

    @ViewChild('statusTemplate')
    statusTemplate: TemplateRef<any>;

    @ViewChild('priorityTemplate')
    priorityTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('verificationTemplate')
    verificationTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('supportEditor')
    supportEditor: SupportEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private supportService: SupportService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

       
        this.columnDefs = [
            {
                headerName: 'Bank Name', field: 'bankName', width: 150, suppressSizeToFit: true
            },
            { headerName: 'Merchant', field: 'merchant', width: 150, suppressSizeToFit: true },
            { headerName: 'Merchant Id', field: 'idMerchant', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 150, suppressSizeToFit: true },
            { headerName: 'Deployed Location', field: 'merchantLocation', width: 180, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            { headerName: 'Application Version', field: 'applicationFormat', width: 180, suppressSizeToFit: true },
            { headerName: 'Contact Person', field: 'contactPerson', width: 150, suppressSizeToFit: true },
            { headerName: 'Contact No', field: 'contactNo', width: 150, suppressSizeToFit: true },
            { headerName: 'Received By', field: 'receivedBy', width: 170, suppressSizeToFit: true },
            { headerName: 'Received ContactNo', field: 'contactNo', width: 180, suppressSizeToFit: true },
            { headerName: 'Issue', field: 'issue', width: 150, suppressSizeToFit: true },
            { headerName: 'Phone', field: 'phone', width: 100, suppressSizeToFit: true, cellRenderer: this.phone },
            { headerName: 'Field', field: 'field', width: 100, suppressSizeToFit: true, cellRenderer: this.field },
            { headerName: 'Priority', field: 'priority', width: 100, suppressSizeToFit: true, cellRenderer: this.priority },
            { headerName: 'Remarks', field: 'remarks', width: 150, suppressSizeToFit: true },
            { headerName: 'Resolved By', field: 'resolvedBy', width: 150, suppressSizeToFit: true },
            {
                headerName: 'Support Date', field: 'supportDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },
               
       
            { headerName: 'Verified By', field: 'verifiedBy', width: 150, suppressSizeToFit: true },
            {
                headerName: 'Verified Date', field: 'verifiedDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },
            

            //{
            //    headerName: "Actions",
            //    suppressMenu: true,
            //    suppressSorting: true,
            //    template:
            //        `<button type="button" data-action-type="Support" class="btn btn-link btn-xs">
            //  Delete
            // </button>`
            //}
        ];

        if (this.canManageUsers)
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: 'requestName',
                suppressSorting: true,
                width: 250,

                //cellRenderer: this.actions,
                template:
                    `<button type="button" data-action-type="Support" class="btn btn-link btn-xs">
              Delete
             </button>`

            });
        
    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "Support":
                    return this.onActionDeleteClick(data);

            }
        }
    }
   
    public phone(params) {
        if (params.data.phone == false) {
            return ('<span style="background-color:maroon;color:white;font-size:15px">NO</span>');
        }
        else {
            return ('<span style="background-color:maroon;color:white;font-size:15px">YES</span>')
        }
    }
    public field(params) {
        if (params.data.field == false) {
            return ('<span style="background-color:maroon;color:white;font-size:15px">NO</span>');
        }
        else {
            return ('<span style="background-color:maroon;color:white;font-size:15px">YES</span>')
        }
    }
    public priority(params) {
        if (params.data.phone == false) {
            return ('<span style="background-color:maroon;color:white;font-size:15px">LOW</span>');
        }
        else {
            return ('<span style="background-color:maroon;color:white;font-size:15px">HIGH</span>')
        }
    }
    public verifiedBy(params) {
        if (params.data.verifiedBy == null) {
            return ('<span style="background-color:maroon;color:white;font-size:15px">NOT VERIFIED</span>');
        }
        else {
            return (params.data.verifiedBy)
        }
    }
   
    ngAfterViewInit() {

        //this.supportEditor.changesSavedCallback = () => {
        //    this.addNewSupportToList();
        //    this.editorModal.hide();
        //};

        //this.supportEditor.changesCancelledCallback = () => {
        //    this.editedSupport = null;
        //    this.sourceSupport = null;
        //    this.editorModal.hide();
        //};
    
    }


    addNewSupportToList() {
        if (this.sourceSupport) {
            Object.assign(this.sourceSupport, this.editedSupport);

            let sourceIndex = this.rowsCache.indexOf(this.sourceSupport, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceSupport, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedSupport = null;
            this.sourceSupport = null;
        }
        else {
            let support = new Support();
            Object.assign(support, this.editedSupport);
            this.editedSupport = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>Support).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, support);
            this.rows.splice(0, 0, support);
        }
        this.loadSupportData('deploymentRequestId');
    }


    loadSupportData(id) {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.supportService.getSupportInfo(-1,-1, id).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(supports: Support[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        supports.forEach((support, index, supports) => {

            (<any>support).index = index + 1;
        });
        this.rowsCache = [...supports];
        this.rows = supports;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.supportService.getSupportInfo().subscribe(data => {
            params.api.setRowData(data);
        });

    }

    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.deploymentRequestId, r.remarks, r.verifiedBy, r.status));
    }

    loadSupportList(allSupport: Support[], id) {
        //this.isGeneralEditor = true;
        this.supportEdit.deploymentRequestId = id;
        this.allSupport = [...allSupport];
        this.support = this.supportEdit = new SupportEdit();
        this.loadSupportData(id);
        return this.supportEdit;
    }


    onEditorModalHidden() {
        this.editingSupportName = null;
        this.supportEditor.resetForm(true);
    }


    //newSupport() {
    //    this.editingSupportName = null;
    //    this.sourceSupport = null;
    //    this.editedSupport = this.supportEditor.newSupport(this.allSupport);
    //    this.editorModal.show();
    //}


    //editSupport(row: SupportEdit) {
    //    this.editingSupportName = { name: row.id };
    //    this.sourceSupport = row;
    //    this.editedSupport = this.supportEditor.editSupport(row, this.allSupport);
    //    this.editorModal.show();
    //}


    onActionDeleteClick(row: SupportEdit) {
        this.alertService.showDialog('Are you sure you want to delete Serial Number Support\"' + row.serialKey + '\"?', DialogType.confirm, () => this.deleteSupportHelper(row));
    }


    deleteSupportHelper(row: SupportEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.supportService.deleteSupport(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }



    get canViewUsers() {
        return this.supportService.userHasPermission(Permission.viewUsersPermission);
    }
   
    get canViewRoles() {
        return this.supportService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.supportService.userHasPermission(Permission.manageUsersPermission);
    }
   
}
