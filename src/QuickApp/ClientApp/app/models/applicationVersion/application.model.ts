export class Application {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, bankId?: string, applicationDate?: Date, applicationVer?: string, kernelType?: string, remarks?: string, bankCode?: string) {
        this.id = id;
        this.bankId = bankId;
        this.applicationDate = applicationDate;
        this.applicationVer = applicationVer;
        this.kernelType = kernelType;
        this.remarks = remarks;
        this.bankCode = bankCode;

    }


 
    public id: string;
    public bankId: string;
    public applicationDate: Date;
    public applicationVer: string;
    public kernelType: string;
    public remarks: string;
    public bankCode: string;
}