﻿

export class Configuration {
    constructor(id?: string, deploymentRequestId?: string, serialNumberId?: string, applicationVersionId?: string, verification?:
        string, statusName?: string, requestName?: string, serialKey?: string, isSale?: boolean, previousSerial?: string, inventoryAddId?:string, status?:string, inventoryId?:string) {

        this.id = id;
        this.deploymentRequestId = deploymentRequestId;
        this.serialNumberId = serialNumberId;
        this.applicationVersionId = applicationVersionId;
        this.verification = verification;
        this.statusName = statusName;
        this.requestName = requestName;
        this.serialKey = serialKey;
        this.isSale = isSale;
        this.previousSerial = previousSerial;
        this.inventoryAddId = inventoryAddId;
        this.status = status;
        this.inventoryId = inventoryId;
    }

    public id: string;
    public deploymentRequestId: string
    public serialNumberId: string
    public applicationVersionId: string
    public verification: string
    public statusName: string
    public requestName: string
    public serialKey: string
    public isSale: boolean
    public previousSerial: string
    public inventoryAddId: string
    public inventoryId: string;
    public status: string;
}
export class NotReceivedPos {
    constructor(id?: string, deploymentRequestId?: string, configurationId?: string, serialNumberId?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, inventoryAddId?: string, inventoryId?:string, status?: string,
        bankName?: string, merchant?: string, idMerchant?: string, idTerminal?: string, outlet?: string, address?: string, district?: string, contactNo?: string, contactPerson?: string, applicationFormat?:string,applicationVersionId?:string, receivedBy?: string, deployedLocation?:string, receivedContactNo?:string, serialKey?: string,createdBy?: string, createdDate?: Date, updatedDate?:Date, item?:string, configStatus?:string) {

        this.id = id;
        this.deploymentRequestId = deploymentRequestId;
        this.configurationId = configurationId;
        this.serialNumberId = serialNumberId;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.status = status;
        this.bankName = bankName;
        this.merchant = merchant;
        this.idMerchant = idMerchant;
        this.idTerminal = idTerminal;
        this.outlet = outlet;
        this.address = address;
        this.district = district;
        this.contactNo = contactNo;
        this.contactPerson = contactPerson;
        this.address = address;
        this.serialKey = serialKey;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.item = item;
        this.configStatus = configStatus;
        this.deployedLocation = deployedLocation;
        this.receivedBy = receivedBy;
        this.receivedContactNo = receivedContactNo;
        this.applicationFormat = applicationFormat;
        this.inventoryAddId = inventoryAddId;
        this.inventoryId = inventoryId;
        this.applicationVersionId = applicationVersionId;
    }

    public id: string;
    public deploymentRequestId: string
    public serialNumberId: string
    public configurationId: string
    public primaryItem: string
    public secondaryItem: string
    public tertiaryItem: string
    public detailItem: string
    public status: string
    public bankName: string
    public merchant: string
    public idMerchant: string
    public idTerminal: string
    public outlet: string
    public address: string
    public district: string
    public contactNo: string
    public contactPerson: string
    public serialKey: string
    public createdBy:string
    public createdDate: Date
    public updatedDate: Date
    public item: string
    public configStatus: string
    public deployedLocation: string
    public receivedBy: string
    public receivedContactNo: string
    public applicationFormat: string
    public inventoryAddId: string
    public inventoryId: string
    public applicationVersionId:string

}

export class DamagePos {
    constructor(id?: string, applicationVersionId?: string, deploymentRequestId?: string, configurationId?: string, serialNumberId?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, inventoryAddId?: string, inventoryId?: string, status?: string,
        bankName?: string, merchant?: string, idMerchant?: string, idTerminal?: string, outlet?: string, address?: string, district?: string, contactNo?: string, contactPerson?: string, serialKey?: string, createdDate?: Date, updatedDate?: Date, item?: string, createdBy?: string) {

        this.id = id;
        this.deploymentRequestId = deploymentRequestId;
        this.configurationId = configurationId;
        this.serialNumberId = serialNumberId;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.status = status;
        this.bankName = bankName;
        this.merchant = merchant;
        this.idMerchant = idMerchant;
        this.idTerminal = idTerminal;
        this.outlet = outlet;
        this.address = address;
        this.district = district;
        this.contactNo = contactNo;
        this.contactPerson = contactPerson;
        this.address = address;
        this.serialKey = serialKey;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.item = item;
        this.applicationVersionId = applicationVersionId;
        this.inventoryId = inventoryId;
        this.inventoryAddId = inventoryAddId;
    }

    public id: string;
    public deploymentRequestId: string
    public serialNumberId: string
    public configurationId: string
    public primaryItem: string
    public secondaryItem: string
    public tertiaryItem: string
    public detailItem: string
    public status: string
    public bankName: string
    public merchant: string
    public idMerchant: string
    public idTerminal: string
    public outlet: string
    public address: string
    public district: string
    public contactNo: string
    public contactPerson: string
    public serialKey: string
    public createdBy: string
    public createdDate: Date
    public updatedDate: Date
    public item: string
    public applicationVersionId: string
    public inventoryId: string
    public inventoryAddId: string
}
