export class DeployEdit {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, inventoryId?: string, deployDate?: Date, receivedBy?: string, connectivity?: string, merchantLocation?: string,
        longitude?: string, latitude?: string, image?: string, scanned?: string, remarks?: string, deploymentRequestId?: string,
        verification?: string, statusName?: string, requestName?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, connectivity1?: string, connectivity2?: string, connectivity3?: string, connectivity4?: string, connectivity5?: string, connectivity6?: string, dispatch?: boolean, isDeployed?: boolean, item?: string, statusForDeploy?: string) {
        this.id = id;
        this.inventoryId = inventoryId;
        this.receivedBy = receivedBy;
        this.connectivity = connectivity;
        this.merchantLocation = merchantLocation;
        this.latitude = latitude;
        this.longitude = longitude;
        this.image = image;
        this.scanned = scanned;
        this.remarks = remarks;
        this.deploymentRequestId = deploymentRequestId;
        this.deployDate = deployDate;
        this.verification = verification;
        this.statusName = statusName;
        this.requestName = requestName;
        this.connectivity1 = connectivity1;
        this.connectivity2 = connectivity2;
        this.connectivity3 = connectivity3;
        this.connectivity4 = connectivity4;
        this.connectivity5 = connectivity5;
        this.connectivity6 = connectivity6;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.dispatch = dispatch;
        this.isDeployed = isDeployed;
        this.item = item;
        this.statusForDeploy = statusForDeploy;
    }


    public id: string;
    public inventoryId: string;
    public deployDate: Date;
    public receivedBy: string;
    public connectivity: string;
    public merchantLocation: string;
    public latitude: string;
    public longitude: string;
    public image: string;
    public scanned: string;
    public remarks: string;
    public deploymentRequestId: string;
    public verification: string;
    public statusName: string;
    public requestName: string
    public connectivity1: string;
    public connectivity2: string;
    public connectivity3: string;
    public connectivity4: string;
    public connectivity5: string;
    public connectivity6: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public dispatch: boolean;
    public isDeployed: boolean;
    public item: string;
    public statusForDeploy: string;

}