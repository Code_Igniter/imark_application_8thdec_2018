﻿

export class DeploymentRequest {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type

    constructor(id?: string, bankId?: string, inventoryId?: string, bankName?: string, merchant?: string, idMerchant?: string, outlet?: string, district?: string,
        address?: string, contactPerson?: string, contactNo1?: string, contactNo2?: string, idTerminal?: string, terminalType?: string, currency?: string,
        tipAdjustment?: boolean, manualTransaction?: boolean, preAuthorization?: boolean, primaryNacNumber?: string, secondaryNacNumber?: string,
        priority?: boolean, statusId?: number, statusName?: string, remarks?: string, verification?: string,
        name?: string, requestTypeId?: string, requestName?: string, refund?: boolean, checkerId?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string,
        serialNumberId?: string, serialNumber?: string, serialKey?: string, remarksForDeploy?: string, soldDate?: Date, item?: string, rentalInventoryAdd?: string, receivedBy?: string, receivedContactNo?: string, configStatus?: string, applicationVersion?: string, isImarkProperty?: boolean) {
        this.id = id;
        this.bankId = bankId;
        this.bankName = bankName;
        this.merchant = merchant;
        this.idMerchant = idMerchant;
        this.outlet = outlet;
        this.district = district;
        this.address = address;
        this.contactPerson = contactPerson;
        this.contactNo1 = contactNo1;
        this.contactNo2 = contactNo2;
        this.idTerminal = idTerminal;
        this.terminalType = terminalType;
        this.currency = currency;
        this.tipAdjustment = tipAdjustment;
        this.manualTransaction = manualTransaction;
        this.preAuthorization = preAuthorization;
        this.primaryNacNumber = primaryNacNumber;
        this.secondaryNacNumber = secondaryNacNumber;
        this.priority = priority;
        this.statusId = statusId;
        this.statusName = statusName;
        this.remarks = remarks;
        this.verification = verification;
        this.name = name;
        this.requestTypeId = requestTypeId;
        this.requestName = requestName;
        this.refund = refund;
        this.checkerId = checkerId;
        this.inventoryId = inventoryId;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.serialNumberId = serialNumberId;
        this.serialNumber = serialNumber;
        this.remarksForDeploy = remarksForDeploy;
        this.soldDate = soldDate;
        this.item = item;
        this.rentalInventoryAdd = rentalInventoryAdd;
        this.receivedBy = receivedBy;
        this.receivedContactNo = receivedContactNo;
        this.configStatus = configStatus;
        this.applicationVersion = applicationVersion;
        this.serialKey = serialKey;
        this.isImarkProperty = isImarkProperty;
    }

    public id: string;
    public bankId: string;
    public bankName: string;
    public merchant: string;
    public idMerchant: string;
    public outlet: string;
    public district: string;
    public address: string;
    public contactPerson: string;
    public contactNo1: string;
    public contactNo2: string;
    public idTerminal: string;
    public terminalType: string;
    public currency: string;
    public tipAdjustment: boolean;
    public manualTransaction: boolean;
    public preAuthorization: boolean;
    public primaryNacNumber: string;
    public secondaryNacNumber: string;
    public priority: boolean;
    public statusId: number;
    public statusName: string;
    public remarks: string;
    public verification: string;
    public name: string;
    public requestTypeId: string;
    public requestName: string;
    public refund: boolean;
    public checkerId: string;
    public inventoryId: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public serialNumberId: string;
    public serialNumber: string;
    public remarksForDeploy: string;
    public soldDate: Date;
    public item: string;
    public rentalInventoryAdd: string;
    public receivedBy: string;
    public receivedContactNo: string;
    public configStatus: string;
    public applicationVersion: string;
    public serialKey: string;
    public isImarkProperty: boolean;

}