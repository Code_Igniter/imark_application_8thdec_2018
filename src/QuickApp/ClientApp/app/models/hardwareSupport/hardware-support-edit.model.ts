﻿
export class HardwareSupportEdit {

    constructor(id?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, serialNo?: string, purchaseDate?: Date, manufacturer?: string, contactPerson?: string, contactNo?: string, bankId?: string, bankName?: string) {
        this.id = id;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.serialNo = serialNo;
        this.purchaseDate = purchaseDate;
        this.manufacturer = manufacturer;
        this.contactPerson = contactPerson;
        this.contactNo = contactNo;
        this.bankId = bankId;
        this.bankName = bankName;

    }

    public id: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public serialNo: string;
    public purchaseDate: Date;
    public manufacturer: string;
    public contactPerson: string;
    public contactNo: string;
    public bankId: string;
    public bankName: string;
}