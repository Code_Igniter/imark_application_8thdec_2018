﻿

export class InventoryAddEdit {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type

    constructor(id?: string, totalNumber?: number, buyDate?: Date, inventoryId?: string, primaryItem?: string, secondaryItem?: string,
        tertiaryItem?: string, detailItem?: string, hasSerialNo?: boolean, remainingNumber?: number, bankStatus?: string, deployNumber?: number,
        soldNumber?: number, maintenanceNumber?: number, available?: number, notReceivedNumber?: number, damageNumber?: number, configureNumber?: number, reConfigureNumber?: number, pendingDeploy?:number, item?:string) {
        this.id = id;
        this.inventoryId = inventoryId;
        this.totalNumber = totalNumber;
        this.buyDate = buyDate;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.hasSerialNo = hasSerialNo;
        this.remainingNumber = remainingNumber;
        this.bankStatus = bankStatus;
        this.available = available;
        this.soldNumber = soldNumber;
        this.maintenanceNumber = maintenanceNumber;
        this.deployNumber = deployNumber;
        this.notReceivedNumber = notReceivedNumber;
        this.damageNumber = damageNumber;
        this.configureNumber = configureNumber;
        this.reConfigureNumber = reConfigureNumber;
        this.pendingDeploy = pendingDeploy;
        this.item = item;
    }

    public id: string;
    public inventoryId: string;
    public totalNumber: number;
    public buyDate: Date;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public hasSerialNo: boolean;
    public remainingNumber: number;
    public bankStatus: string;
    public soldNumber: number;
    public maintenanceNumber: number;
    public deployNumber: number;
    public available: number;
    public notReceivedNumber: number;
    public damageNumber: number;
    public configureNumber: number;
    public reConfigureNumber: number;
    public pendingDeploy: number;
    public item: string;
}