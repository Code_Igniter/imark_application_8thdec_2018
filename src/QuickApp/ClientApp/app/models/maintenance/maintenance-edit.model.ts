﻿import { MaintenanceViewModel } from './maintenanceViewModel.model';
export class MaintenanceEdit {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, serialNumberId?: string, serialNumber?: string, deploymentRequestId?: string, status?: string, terminate?: string,
        remarks?: string, resolvedBy?: string, issue?: string, verification?: string, verifiedBy?: string, verifiedDate?: Date, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string,
        inventoryId?: string, inventoryAddId?: string, applicationVersionId?: string, InventoryName?: string, Bank?: string, Merchant?: string, DeployDate?: Date, BuyDate?: Date, maintenanceVm?: MaintenanceViewModel[], itemRow?: MaintenanceViewModel[],
        bankName?: string, merchant?: string, idMerchant?: string, idTerminal?: string, item?: string, applicationFormat?: string, outlet?: string, address?: string, district?: string, contactPerson?: string, contactNo?: string) {
        this.id = id;
        this.serialNumberId = serialNumberId;
        this.serialNumber = serialNumber;
        this.deploymentRequestId = deploymentRequestId;
        this.status = status;
        this.terminate = terminate;
        this.resolvedBy = resolvedBy;
        this.issue = issue;
        this.remarks = remarks;
        this.Bank = Bank;
        this.BuyDate = BuyDate;
        this.DeployDate = DeployDate;
        this.InventoryName = InventoryName;
        this.inventoryId = inventoryId;
        this.verification = verification;
        this.verifiedBy = verifiedBy;
        this.verifiedDate = verifiedDate;
        this.maintenanceVm = maintenanceVm;
        this.itemRow = itemRow;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.bankName = bankName;
        this.merchant = merchant;
        this.idMerchant = idMerchant;
        this.idTerminal = idTerminal;
        this.item = item;
        this.applicationFormat = applicationFormat;
        this.applicationVersionId = applicationVersionId;
        this.inventoryAddId = inventoryAddId;
        this.outlet = outlet;
        this.contactPerson = contactPerson;
        this.contactNo = contactNo;
        this.address = address;
        this.district = district;
    }


    public id: string;
    public serialNumberId: string;
    public serialNumber: string;
    public deploymentRequestId: string;
    public status: string;
    public terminate: string;
    public resolvedBy: string;
    public issue: string;
    public remarks: string;
    public verification: string;
    public verifiedBy: string;
    public verifiedDate: Date;
    public inventoryId: string;
    public InventoryName: string;
    public Bank: string;
    public Merchant: string;
    public DeployDate: Date;
    public BuyDate: Date;
    public maintenanceVm: MaintenanceViewModel[];
    public itemRow: MaintenanceViewModel[];
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public bankName: string;
    public merchant: string;
    public idMerchant: string;
    public idTerminal: string;
    public item: string;
    public applicationFormat: string;
    public inventoryAddId: string;
    public applicationVersionId: string;
    public outlet: string;
    public contactPerson: string;
    public contactNo: string;
    public address: string;
    public district: string;


}
