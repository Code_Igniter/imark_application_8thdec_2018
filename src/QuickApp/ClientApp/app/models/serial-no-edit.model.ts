﻿

export class SerialNoEdit {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, serialKey?: string, inventoryAddId?: string, isSold?: boolean, totalSerialKey?: number, remainingSerialKey?: number, bankName?: string) {
        this.id = id;
        this.serialKey = serialKey;
        this.inventoryAddId = inventoryAddId;
        this.isSold = isSold;
        this.totalSerialKey = totalSerialKey;
        this.remainingSerialKey = remainingSerialKey;
        this.bankName = bankName;
    }

    public id: string;
    public serialKey: string;
    public inventoryAddId: string;
    public isSold: boolean;
    public totalSerialKey: number;
    public remainingSerialKey: number;
    public bankName: string;

}