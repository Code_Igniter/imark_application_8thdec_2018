﻿export class Support {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, deploymentRequestId?: string, bankName?: string, merchant?: string, idMerchant?: string, outlet?: string, idTerminal?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, phone?: boolean, field?: boolean, status?: boolean,
        priority?: boolean, issue?: string, contactPerson?: string, contactNo?: string, resolvedBy?: string,
        remarks?: string, verification?: string, verifiedBy?: string, verifiedDate?: Date, serialKey?: string, item?: string, applicationFormat?: string,
        address?: string, merchantLocation?: string, receivedBy?: string, receivedContactNo?:string) {
        this.id = id;
        this.deploymentRequestId = deploymentRequestId;
        this.phone = phone;
        this.field = field;
        this.status = status;
        this.priority = priority;
        this.issue = issue;
        this.contactPerson = contactPerson;
        this.contactNo = contactPerson;
        this.resolvedBy = resolvedBy;
        this.remarks = remarks;
        this.verification = verification;
        this.verifiedBy = verifiedBy;
        this.verifiedDate = verifiedDate;
        this.bankName = bankName;
        this.merchant = merchant;
        this.idMerchant = idMerchant;
        this.outlet = outlet;
        this.idTerminal = idTerminal;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.serialKey = serialKey;
        this.item = item;
        this.applicationFormat = applicationFormat;
        this.address = address;
        this.merchantLocation = merchantLocation;
        this.receivedBy = receivedBy;
        this.receivedContactNo = receivedContactNo;
    }


    public id: string;
    public deploymentRequestId: string;
    public phone: boolean;
    public field: boolean;
    public status: boolean;
    public priority: boolean;
    public issue: string;
    public contactPerson: string;
    public contactNo: string;
    public resolvedBy: string;
    public remarks: string;
    public verification: string;
    public verifiedBy: string;
    public verifiedDate: Date;
    public bankName: string;
    public merchant: string;
    public idMerchant: string;
    public outlet: string;
    public idTerminal: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public serialKey: string;
    public item: string;
    public applicationFormat: string;
    public address: string;
    public merchantLocation: string;
    public receivedBy: string;
    public receivedContactNo: string;

}