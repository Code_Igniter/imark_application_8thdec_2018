﻿import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { ConfigurationEndpoint } from './configuration-endpoint.service';
import { AuthService } from '../auth.service';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';
import { Configuration, NotReceivedPos, DamagePos } from '../../models/configuration/configuration.model';
import { ConfigurationEdit, DamagePosEdit } from '../../models/configuration/configuration-edit.model';
import { NotReceivedPosEdit } from '../../models/configuration/configuration-edit.model';




export type ConfigurationChangedOperation = "add" | "delete" | "modify";
export type ConfigurationChangedEventArg = { configuration: Configuration[] | string[], operation: ConfigurationChangedOperation };



@Injectable()
export class ConfigurationsService {

    public static readonly configurationAddedOperation: ConfigurationChangedOperation = "add";
    public static readonly configurationDeletedOperation: ConfigurationChangedOperation = "delete";
    public static readonly configurationModifiedOperation: ConfigurationChangedOperation = "modify";

    private _configurationChanged = new Subject<ConfigurationChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private ConfigurationEndpoint: ConfigurationEndpoint) {

    }


    getConfiguration(configurationId?: string) {
        return this.ConfigurationEndpoint.getConfigurationEndpoint<Configuration>(configurationId);
    }

    //getUserAndRoles(userId?: string) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUserEndpoint<User>(userId),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}

    getConfigurationInfo(page?: number, pageSize?: number) {
        return this.ConfigurationEndpoint.getConfigurationsEndpoint<Configuration[]>(page, pageSize);
    }

    serialConfiguration(page?: number, pageSize?: number, serial?:string) {
        return this.ConfigurationEndpoint.getSerialConfigurationsEndpoint<Configuration[]>(page, pageSize);
    }
    notReceivedSerialPage(page?: number, pageSize?: number) {
        return this.ConfigurationEndpoint.getNotReceivedSerialPageEndpoint<NotReceivedPos[]>(page, pageSize);
    }
    damagePosPage(page?: number, pageSize?: number) {
        return this.ConfigurationEndpoint.getDamagePosPageEndpoint<DamagePos[]>(page, pageSize);
    }

    //getUsersAndRoles(page?: number, pageSize?: number) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUsersEndpoint<User[]>(page, pageSize),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}
  


    updateConfiguration(configuration: ConfigurationEdit) {
        if (configuration.id) {
            return this.ConfigurationEndpoint.getUpdateConfigurationEndpoint(configuration, configuration.id);
        }
        else {

            return this.ConfigurationEndpoint.getConfigurationByPrimaryItemEndpoint<Configuration>(configuration.id)

                .mergeMap(foundUser => {
                    configuration.id = foundUser.id;
                    return this.ConfigurationEndpoint.getUpdateConfigurationEndpoint(configuration, configuration.id)
                });
        }
    }
    updateConfigurationStatus(configuration: ConfigurationEdit) {
        if (configuration.id) {
            return this.ConfigurationEndpoint.getUpdateConfigurationStatusEndpoint(configuration, configuration.id);
        }
        else {

            return this.ConfigurationEndpoint.getConfigurationByPrimaryItemEndpoint<Configuration>(configuration.id)

                .mergeMap(foundUser => {
                    configuration.id = foundUser.id;
                    return this.ConfigurationEndpoint.getUpdateConfigurationStatusEndpoint(configuration, configuration.id)
                });
        }
    }
    updateNotReceived(notReceived: NotReceivedPosEdit) {
        if (notReceived.id) {
            return this.ConfigurationEndpoint.getUpdateNotReceivedEndpoint(notReceived, notReceived.id);
        }
        else {

            return this.ConfigurationEndpoint.getNotReceivedByPrimaryItemEndpoint<NotReceivedPos>(notReceived.id)

                .mergeMap(foundUser => {
                    notReceived.id = foundUser.id;
                    return this.ConfigurationEndpoint.getUpdateNotReceivedEndpoint(notReceived, notReceived.id)
                });
        }
    }
    //updateNotReceived(notReceived: NotReceivedPosEdit) {
    //    if (notReceived.id) {
    //        return this.ConfigurationEndpoint.getUpdateNotReceivedEndpoint(notReceived, notReceived.id);
    //    }
    //    else {

    //        return this.ConfigurationEndpoint.getNotReceivedByPrimaryItemEndpoint<NotReceivedPos>(notReceived.id)

    //            .mergeMap(foundUser => {
    //                notReceived.id = foundUser.id;
    //                return this.ConfigurationEndpoint.getUpdateNotReceivedEndpoint(notReceived, notReceived.id)
    //            });
    //    }
    //}


    newConfiguration(configuration: ConfigurationEdit) {
        return this.ConfigurationEndpoint.getNewConfigurationEndpoint<Configuration>(configuration);
    }

    notReceivedSerial(configuration: ConfigurationEdit) {
        return this.ConfigurationEndpoint.getNotReceivedSerialEndpoint<Configuration>(configuration);
    }
    newDamage(configuration: DamagePosEdit) {
        return this.ConfigurationEndpoint.getNewDamagePosEndpoint<DamagePos>(configuration);
    }



    deleteConfiguration(configurationOrConfigurationId: string | ConfigurationEdit): Observable<Configuration> {

        if (typeof configurationOrConfigurationId === 'string' || configurationOrConfigurationId instanceof String) {
            return this.ConfigurationEndpoint.getDeleteConfigurationEndpoint<Configuration>(<string>configurationOrConfigurationId);
            //.do(data => this.onInventoryCountChanged([data], AccountService.posDeletedOperation));
        }
        else {

            if (configurationOrConfigurationId.id) {
                return this.deleteConfiguration(configurationOrConfigurationId.id);
            }
            else {
                return this.ConfigurationEndpoint.getConfigurationByPrimaryItemEndpoint<Configuration>(configurationOrConfigurationId.id)
                    .mergeMap(configuration => this.deleteConfiguration(configuration.id));
            }
        }
    }




    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}