﻿


import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { DashboardEndPoint } from './dashboard-endpoint.service';
import { AuthService } from '../auth.service';
import { Bank } from '../../models/bank/bank.model';
import { BankEdit } from '../../models/bank/bank-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';





@Injectable()
export class DashboardService {

    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private DashboardEndPoint: DashboardEndPoint) {

    }

    getBankDeployInfo(page?: number, pageSize?: number) {
        return this.DashboardEndPoint.getBankDeployEndpoint<Bank[]>(page, pageSize);
    }
    getStockDashboardInfo(page?: number, pageSize?: number) {
        return this.DashboardEndPoint.getStockDashboardEndpoint<Bank[]>(page, pageSize);
    }




    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}