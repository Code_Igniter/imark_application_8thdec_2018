

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class DeployEndpoint extends EndpointFactory {

    private readonly _deployUrl: string = "/api/deployment";
    private readonly _deployPageUrl: string = "/api/deployment/pagination";
    private readonly _deployConnectivityPageUrl: string = "/api/deployment/connectivity";
    private readonly _currentUserUrl: string = "/api/account/users/me";



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }




    getDeployEndpoint<T>(deployId?: string): Observable<T> {
        let endpointUrl = deployId ? `${this._deployUrl}/${deployId}` : this._deployUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeployEndpoint(deployId));
            });
    }



    getDeployByTotalEndpoint<T>(deployName: string): Observable<T> {
        let endpointUrl = `${this._deployUrl}/${deployName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeployByTotalEndpoint(deployName));
            });
    }



    getDeploysEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._deployPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeploysEndpoint(page, pageSize));
            });
    }
    getConnectivityEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._deployConnectivityPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getConnectivityEndpoint(page, pageSize));
            });
    }


    getNewDeployEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._deployUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewDeployEndpoint(userObject));
            });
    }


    getUpdateDeployEndpoint<T>(userObject: any, deployId?: string, dispatch?: boolean): Observable<T> {
        let endpointUrl = deployId ? `${this._deployUrl}/${deployId}/${dispatch}` : this._deployUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateDeployEndpoint(userObject, deployId,dispatch));
            });
    }






    getDeleteDeployEndpoint<T>(deployId: string): Observable<T> {
        let endpointUrl = `${this._deployUrl}/${deployId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteDeployEndpoint(deployId));
            });
    }




}