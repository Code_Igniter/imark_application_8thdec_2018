
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { DeployEndpoint } from './deploy-endpoint.service';
import { AuthService } from '../auth.service';
import { Deploy } from '../../models/deploy/deploy.model';
import { DeployEdit } from '../../models/deploy/deploy-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type DeployChangedOperation = "add" | "delete" | "modify";
export type DeployChangedEventArg = { deploy: Deploy[] | string[], operation: DeployChangedOperation };



@Injectable()
export class DeployService {

    public static readonly deployAddedOperation: DeployChangedOperation = "add";
    public static readonly deployDeletedOperation: DeployChangedOperation = "delete";
    public static readonly deployModifiedOperation: DeployChangedOperation = "modify";

    private _deployChanged = new Subject<DeployChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private DeployEndpoint: DeployEndpoint) {

    }


    getDeploy(deploymentId?: string) {
        return this.DeployEndpoint.getDeployEndpoint<Deploy>(deploymentId);
    }
   

    getDeployInfo(page?: number, pageSize?: number) {
        return this.DeployEndpoint.getDeploysEndpoint<Deploy[]>(page, pageSize);
    }


    updateDeploy(deploy: DeployEdit, dispatch?: boolean) {
        if (deploy.id) {
            return this.DeployEndpoint.getUpdateDeployEndpoint(deploy, deploy.id, dispatch);
        }
        else {

            return this.DeployEndpoint.getDeployByTotalEndpoint<Deploy>(deploy.id)

                .mergeMap(foundUser => {
                    deploy.id = foundUser.id;
                    return this.DeployEndpoint.getUpdateDeployEndpoint(deploy, deploy.id)
                });
        }
    }
    getConnectivity(page?: number, pageSize?: number) {
        return this.DeployEndpoint.getConnectivityEndpoint<Deploy[]>(page, pageSize);
    }


    newDeploy(deploy: DeployEdit) {
        return this.DeployEndpoint.getNewDeployEndpoint<Deploy>(deploy);
    }




    deleteDeploy(deployOrDeployId: string | DeployEdit): Observable<Deploy> {

        if (typeof deployOrDeployId === 'string' || deployOrDeployId instanceof String) {
            return this.DeployEndpoint.getDeleteDeployEndpoint<Deploy>(<string>deployOrDeployId);
        }
        else {

            if (deployOrDeployId.id) {
                return this.deleteDeploy(deployOrDeployId.id);
            }
            else {
                return this.DeployEndpoint.getDeployByTotalEndpoint<Deploy>(deployOrDeployId.id)
                    .mergeMap(deploy => this.deleteDeploy(deploy.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}