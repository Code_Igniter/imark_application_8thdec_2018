﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class DeploymentEndpoint extends EndpointFactory {

    private readonly _deploymentUrl: string = "/api/deploymentRequest";
    private readonly _posStockUrl: string = "/api/posStock/newPartialDeployment";
    private readonly _deploymentPageUrl: string = "/api/deploymentRequest/pagination";
    private readonly _deploymentUndeployedPageUrl: string = "/api/deploymentRequest/Undeployedpartial";
    private readonly _partialRentalUrl: string = "/api/deploymentRequest/reconfigurePartialRental";
    private readonly _deployListPageUrl: string = "/api/deploylist/pagination";
    private readonly _currentUserUrl: string = "/api/account/users/me";
    private readonly _posStockConfigurationUrl: string = "/api/posStock/newPartialDeploy";


    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }




    getDeploymentEndpoint<T>(deploymentId?: string): Observable<T> {
        let endpointUrl = deploymentId ? `${this._deploymentUrl}/${deploymentId}` : this._deploymentUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeploymentEndpoint(deploymentId));
            });
    }



    getDeploymentByTotalEndpoint<T>(deploymentName: string): Observable<T> {
        let endpointUrl = `${this._deploymentUrl}/${deploymentName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeploymentByTotalEndpoint(deploymentName));
            });
    }



    getDeploymentsEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._deploymentPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeploymentsEndpoint(page, pageSize));
            });
    }
    getBankIdsEndpoint<T>(bankName?: string, deploymentReqId?: string, page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._deploymentUndeployedPageUrl}/${bankName}/${deploymentReqId}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getBankIdsEndpoint(bankName, deploymentReqId, page, pageSize));
            });
    }

    getDeploysEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._deployListPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeploysEndpoint(page, pageSize));
            });
    }
   
    getPartialRentalEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._partialRentalUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPartialRentalEndpoint(page, pageSize));
            });
    }
    getNewPartialRentalDeployEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._posStockConfigurationUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewPartialRentalDeployEndpoint(userObject));
            });
    }
    getNewDeploymentEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._deploymentUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewDeploymentEndpoint(userObject));
            });
    }
    getNewPartialRentalDeploymentEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._posStockUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewPartialRentalDeploymentEndpoint(userObject));
            });
    }


    getUpdateDeploymentEndpoint<T>(userObject: any, deploymentId?: string): Observable<T> {
        let endpointUrl = deploymentId ? `${this._deploymentUrl}/${deploymentId}` : this._deploymentUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateDeploymentEndpoint(userObject, deploymentId));
            });
    }






    getDeleteDeploymentEndpoint<T>(deploymentId: string): Observable<T> {
        let endpointUrl = `${this._deploymentUrl}/${deploymentId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteDeploymentEndpoint(deploymentId));
            });
    }




}