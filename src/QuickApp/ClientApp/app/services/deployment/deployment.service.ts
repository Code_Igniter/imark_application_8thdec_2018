﻿


import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { DeploymentEndpoint } from './deployment-endpoint.service';
import { AuthService } from '../auth.service';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';
import { PosStock } from '../../models/posStock/pos-stock.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { Inventory } from '../../models/inventory.model';




export type DeploymentChangedOperation = "add" | "delete" | "modify";
export type DeploymentChangedEventArg = { deployment: DeploymentRequest[] | string[], operation: DeploymentChangedOperation };



@Injectable()
export class DeploymentService {

    public static readonly deploymentAddedOperation: DeploymentChangedOperation = "add";
    public static readonly deploymentDeletedOperation: DeploymentChangedOperation = "delete";
    public static readonly deploymentModifiedOperation: DeploymentChangedOperation = "modify";

    private _deploymentChanged = new Subject<DeploymentChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private DeploymentEndpoint: DeploymentEndpoint) {

    }


    getDeployment(deploymentId?: string) {
        return this.DeploymentEndpoint.getDeploymentEndpoint<DeploymentRequest>(deploymentId);
    }
   

    getDeploymentInfo(page?: number, pageSize?: number) {
        return this.DeploymentEndpoint.getDeploymentsEndpoint<DeploymentRequest[]>(page, pageSize);
    }
    getBankIdInfo(bankName?: string, deploymentReqId?: string, page?: number, pageSize?: number) {
        return this.DeploymentEndpoint.getBankIdsEndpoint<Inventory[]>(bankName, deploymentReqId, page, pageSize);
    }

    getDeployInfo(page?: number, pageSize?: number) {
        return this.DeploymentEndpoint.getDeploysEndpoint<DeploymentRequest[]>(page, pageSize);
    }
    getPartialRental(page?: number, pageSize?: number) {
        return this.DeploymentEndpoint.getPartialRentalEndpoint<DeploymentRequest[]>(page, pageSize);
    }
    updateDeployment(deployment: DeploymentRequestEdit) {
        if (deployment.id) {
            return this.DeploymentEndpoint.getUpdateDeploymentEndpoint(deployment, deployment.id );
        }
        else {

            return this.DeploymentEndpoint.getDeploymentByTotalEndpoint<DeploymentRequest>(deployment.id)

                .mergeMap(foundUser => {
                    deployment.id = foundUser.id;
                    return this.DeploymentEndpoint.getUpdateDeploymentEndpoint(deployment, deployment.id)
                });
        }
    }


    newDeployment(deployment: DeploymentRequestEdit) {
        return this.DeploymentEndpoint.getNewDeploymentEndpoint<DeploymentRequest>(deployment);
    }

    newPartialRentalDeployment(deployment: DeploymentRequestEdit) {
        return this.DeploymentEndpoint.getNewPartialRentalDeploymentEndpoint<DeploymentRequest>(deployment);
    }
    newPartialRentalDeploy(deploy: DeploymentRequestEdit) {
        return this.DeploymentEndpoint.getNewPartialRentalDeployEndpoint<DeploymentRequestEdit>(deploy);
    }


    deleteDeployment(deploymentOrDeploymentId: string | DeploymentRequestEdit): Observable<DeploymentRequest> {

        if (typeof deploymentOrDeploymentId === 'string' || deploymentOrDeploymentId instanceof String) {
            return this.DeploymentEndpoint.getDeleteDeploymentEndpoint<DeploymentRequest>(<string>deploymentOrDeploymentId);
            //.do(data => this.onInventoryAddCountChanged(data.inventoryId));
        }
        else {

            if (deploymentOrDeploymentId.id) {
                return this.deleteDeployment(deploymentOrDeploymentId.id);
            }
            else {
                return this.DeploymentEndpoint.getDeploymentByTotalEndpoint<DeploymentRequest>(deploymentOrDeploymentId.id)
                    .mergeMap(deployment => this.deleteDeployment(deployment.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}