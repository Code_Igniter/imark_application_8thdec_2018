﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { HardwareSupportEndpoint } from './hardware-support-endpoint.service';
import { AuthService } from '../auth.service';
import { HardwareSupport } from '../../models/hardwareSupport/hardware-support.model';
import { HardwareSupportEdit } from '../../models/hardwareSupport/hardware-support-edit.model';
//import { InventoryAdd } from '../models/inventory-add.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type HardwareSupportChangedOperation = "add" | "delete" | "modify";
export type HardwareSupportChangedEventArg = { hardwareSupport: HardwareSupport[] | string[], operation: HardwareSupportChangedOperation };



@Injectable()
export class HardwareSupportService {

    public static readonly hardwareSupportAddedOperation: HardwareSupportChangedOperation = "add";
    public static readonly hardwareSupportDeletedOperation: HardwareSupportChangedOperation = "delete";
    public static readonly hardwareSupportModifiedOperation: HardwareSupportChangedOperation = "modify";

    private _hardwareSupportChanged = new Subject<HardwareSupportChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private HardwareSupportEndpoint: HardwareSupportEndpoint) {

    }


    getHardwareSupport(hardwareSupportId?: string) {
        return this.HardwareSupportEndpoint.getHardwareSupportEndpoint<HardwareSupport>(hardwareSupportId);
    }

    //getUserAndRoles(userId?: string) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUserEndpoint<User>(userId),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}

    getHardwareSupportInfo(page?: number, pageSize?: number, hasSerial?: boolean) {
        return this.HardwareSupportEndpoint.getHardwareSupportsEndpoint<HardwareSupport[]>(page, pageSize);
    }

    //getUsersAndRoles(page?: number, pageSize?: number) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUsersEndpoint<User[]>(page, pageSize),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}


    updateHardwareSupport(hardwareSupport: HardwareSupportEdit) {
        if (hardwareSupport.id) {
            return this.HardwareSupportEndpoint.getUpdateHardwareSupportEndpoint(hardwareSupport, hardwareSupport.id);
        }
        else {

            return this.HardwareSupportEndpoint.getHardwareSupportByPrimaryItemEndpoint<HardwareSupport>(hardwareSupport.id)

                .mergeMap(foundUser => {
                    hardwareSupport.id = foundUser.id;
                    return this.HardwareSupportEndpoint.getUpdateHardwareSupportEndpoint(hardwareSupport, hardwareSupport.id)
                });
        }
    }


    newHardwareSupport(hardwareSupport: HardwareSupportEdit) {
        return this.HardwareSupportEndpoint.getNewHardwareSupportEndpoint<HardwareSupport>(hardwareSupport);
    }

    //addHardwareSupport(hardwareSupport: HardwareSupportAdd) {
    //    return this.HardwareSupportEndpoint.getAddHardwareSupportEndpoint<HardwareSupportAdd>(hardwareSupport);
    //}



    deleteHardwareSupport(hardwareSupportOrHardwareSupportId: string | HardwareSupportEdit): Observable<HardwareSupport> {

        if (typeof hardwareSupportOrHardwareSupportId === 'string' || hardwareSupportOrHardwareSupportId instanceof String) {
            return this.HardwareSupportEndpoint.getDeleteHardwareSupportEndpoint<HardwareSupport>(<string>hardwareSupportOrHardwareSupportId);
            //.do(data => this.onInventoryCountChanged([data], AccountService.posDeletedOperation));
        }
        else {

            if (hardwareSupportOrHardwareSupportId.id) {
                return this.deleteHardwareSupport(hardwareSupportOrHardwareSupportId.id);
            }
            else {
                return this.HardwareSupportEndpoint.getHardwareSupportByPrimaryItemEndpoint<HardwareSupport>(hardwareSupportOrHardwareSupportId.primaryItem)
                    .mergeMap(hardwareSupport => this.deleteHardwareSupport(hardwareSupport.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}