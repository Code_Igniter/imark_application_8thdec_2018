﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class MaintenanceEndpoint extends EndpointFactory {


    private readonly _maintenanceUrl: string = "/api/maintenance";
    private readonly _maintenanceFromDeploymentUrl: string = "/api/maintenance/fromDeployment";
    private readonly _maintenancePageUrl: string = "/api/maintenance/pagination";
    private readonly _maintenanceIssuePageUrl: string = "/api/maintenance/issue";
    private readonly _maintenanceFromNotReceivedUrl: string = "/api/maintenance/postMaintenanceFromNotReceive";
    private readonly _serialListPageUrl: string = "/api/maintenance/seriallist";
    private readonly _currentUserUrl: string = "/api/account/users/me";



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }




    getMaintenanceEndpoint<T>(maintenanceId?: string): Observable<T> {
        let endpointUrl = maintenanceId ? `${this._maintenanceUrl}/${maintenanceId}` : this._maintenanceUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getMaintenanceEndpoint(maintenanceId));
            });
    }



    getMaintenanceByTotalEndpoint<T>(maintenanceName: string): Observable<T> {
        let endpointUrl = `${this._maintenanceUrl}/${maintenanceName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getMaintenanceByTotalEndpoint(maintenanceName));
            });
    }



    getMaintenancesEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._maintenancePageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getMaintenancesEndpoint(page, pageSize));
            });
    }
    getMaintenanceIssuesEndpoint<T>(page?: number, pageSize?: number, id?: string): Observable<T> {
        let endpointUrl = `${this._maintenanceIssuePageUrl}/${-1}/${-1}/${id}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getMaintenanceIssuesEndpoint(page, pageSize,id));
            });
    }

    getSerialListEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._serialListPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialListEndpoint(page, pageSize));
            });
    }




    getNewMaintenanceEndpoint<T>(userObject: any,serialId?: string): Observable<T> {

        let endpointUrl = serialId ? `${this._maintenanceUrl}/${serialId}` : this._maintenanceUrl;

        return this.http.post<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewMaintenanceEndpoint(userObject, serialId));
            });
        //return this.http.post<T>(this._maintenanceUrl, JSON.stringify(userObject), this.getRequestHeaders())
        //    .catch(error => {
        //        return this.handleError(error, () => this.getNewMaintenanceEndpoint(userObject,serialId));
        //    });
    }
    getNewMaintenanceFromDeploymentEndpoint<T>(userObject: any, serialId?: string): Observable<T> {

        let endpointUrl = serialId ? `${this._maintenanceFromDeploymentUrl}/${serialId}` : this._maintenanceFromDeploymentUrl;

        return this.http.post<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewMaintenanceFromDeploymentEndpoint(userObject, serialId));
            });
        //return this.http.post<T>(this._maintenanceUrl, JSON.stringify(userObject), this.getRequestHeaders())
        //    .catch(error => {
        //        return this.handleError(error, () => this.getNewMaintenanceEndpoint(userObject,serialId));
        //    });
    }



    getUpdateMaintenanceEndpoint<T>(userObject: any, maintenanceId?: string): Observable<T> {
        let endpointUrl = maintenanceId ? `${this._maintenanceUrl}/${maintenanceId}` : this._maintenanceUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateMaintenanceEndpoint(userObject, maintenanceId));
            });
    }
    getAddNewMaintenacePosEndpoint<T>(userObject: any, deploymentRequestId?: string): Observable<T> {

        let endpointUrl = deploymentRequestId ? `${this._maintenanceFromNotReceivedUrl}/${deploymentRequestId}` : this._maintenanceFromNotReceivedUrl;

        return this.http.post<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getAddNewMaintenacePosEndpoint(userObject, deploymentRequestId));
            });

    }





    getDeleteMaintenanceEndpoint<T>(maintenanceId: string): Observable<T> {
        let endpointUrl = `${this._maintenanceUrl}/${maintenanceId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteMaintenanceEndpoint(maintenanceId));
            });
    }




}