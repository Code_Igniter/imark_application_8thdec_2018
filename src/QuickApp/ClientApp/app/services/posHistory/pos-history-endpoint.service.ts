﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { Maintenance } from '../../models/maintenance/maintenance.model';


@Injectable()
export class PosHistoryEndpoint extends EndpointFactory {

    private readonly _posHistoryUrl: string = "/api/posHistory";

    private readonly _posHistoryPageUrl: string = "/api/posHistory/pagination";
    private readonly _damagePosUrl: string = "/api/posHistory/damagePos";
    private readonly _posRecordPageUrl: string = "api/posRecord/pagination";
    private readonly _posSparesHistoryPageUrl: string = "/api/posRecord/posSpares";
    private readonly _posIndividualPageUrl: string = "/api/posRecord/individual";
    private readonly _currentUserUrl: string = "/api/account/users/me";
    private readonly _maintenancePosUrl: string = "/api/maintenance/postMaintenance";

    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }
    getPosHistoryEndpoint<T>(posHistoryId?: string): Observable<T> {
        let endpointUrl = posHistoryId ? `${this._posHistoryUrl}/${posHistoryId}` : this._posHistoryUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosHistoryEndpoint(posHistoryId));
            });
    }
    getPosHistoryByTotalEndpoint<T>(posHistoryName: string): Observable<T> {
        let endpointUrl = `${this._posHistoryUrl}/${posHistoryName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosHistoryByTotalEndpoint(posHistoryName));
            });
    }
    getPosHistorysEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._posHistoryPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosHistorysEndpoint(page, pageSize));
            });
    }
    getPosSparesHistorysEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._posSparesHistoryPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosSparesHistorysEndpoint(page, pageSize));
            });
    }
    getIndividualHistorysEndpoint<T>(page?: number, pageSize?: number, id?: string): Observable<T> {
        let endpointUrl = `${this._posIndividualPageUrl}/${-1}/${-1}/${id}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getIndividualHistorysEndpoint(page, pageSize,id));
            });
    }

    //Reconfigure-case
    getNewPosHistoryEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._posHistoryUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewPosHistoryEndpoint(userObject));
            });
    }

    //Retrieve-case
    getNewPosRetrieveEndpoint<T>(userObject: any, retrieve?: string, deploymentRequestId?: string): Observable<T> {
        let endpointUrl = `${this._posHistoryUrl}/${retrieve}/${deploymentRequestId}`;
        
        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewPosRetrieveEndpoint(retrieve, deploymentRequestId, userObject));
            });
    }
   
    getMaintenanceRetrieveEndpoint<T>(maintenancePos?: string, serialId?: string): Observable<T> {
        let endpointUrl = `${this._posHistoryUrl}/${maintenancePos}/${serialId}`;

        return this.http.put<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getMaintenanceRetrieveEndpoint(maintenancePos, serialId));
            });
    }
   
    getNewMaintenanceEndpoint<T>(userObject: any, deploymentRequestId?: string): Observable<T> {

        let endpointUrl = deploymentRequestId ? `${this._maintenancePosUrl}/${deploymentRequestId}` : this._maintenancePosUrl;

        return this.http.post<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewMaintenanceEndpoint(userObject, deploymentRequestId));
            });
       
    }

    getDamageRetrieveEndpoint<T>(damagePos?: string, maintenanceId?: string): Observable<T> {
        let endpointUrl = `${this._posHistoryUrl}/${damagePos}/${maintenanceId}`;

        return this.http.put<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDamageRetrieveEndpoint(damagePos, maintenanceId));
            });
    }
    getNewDamagePosEndpoint<T>(userObject: any, deploymentRequestId?: string): Observable<T> {

        let endpointUrl = deploymentRequestId ? `${this._damagePosUrl}/${deploymentRequestId}` : this._damagePosUrl;

        return this.http.post<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewDamagePosEndpoint(userObject, deploymentRequestId));
            });

    }
    getNewPosRetrieveVerifyEndpoint<T>(retrieveVerify?: string, deploymentRequestId?: string): Observable<T> {
        let endpointUrl = `${this._posHistoryUrl}/${retrieveVerify}/${deploymentRequestId}`;

        return this.http.put<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewPosRetrieveEndpoint(retrieveVerify, deploymentRequestId));
            });
    }
    getUndoRetrievePosEndpoint<T>(undo?: string, deploymentRequestId?: string): Observable<T> {
        let endpointUrl = `${this._posHistoryUrl}/${undo}/${deploymentRequestId}`;

        return this.http.put<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewPosRetrieveEndpoint(undo, deploymentRequestId));
            });
    }

    //getRetrieveByTotalEndpoint<T>(retrieveName: string): Observable<T> {
    //    let endpointUrl = `${this._posHistoryUrl}/${retrieveName}`;

    //    return this.http.put<T>(endpointUrl, this.getRequestHeaders())
    //        .catch(error => {
    //            return this.handleError(error, () => this.getRetrieveByTotalEndpoint(retrieveName));
    //        });
    //}
    //getUpdatePosHistoryEndpoint<T>(userObject: any, posHistoryId?: string): Observable<T> {
    //    let endpointUrl = posHistoryId ? `${this._posHistoryUrl}/${posHistoryId}` : this._posHistoryUrl;

    //    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
    //        .catch(error => {
    //            return this.handleError(error, () => this.getUpdatePosHistoryEndpoint(userObject, posHistoryId));
    //        });
    //}
    getDeletePosHistoryEndpoint<T>(posHistoryId: string): Observable<T> {
        let endpointUrl = `${this._posHistoryUrl}/${posHistoryId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeletePosHistoryEndpoint(posHistoryId));
            });
    }
    getPosRecordsEndPoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._posRecordPageUrl}/${-1}/${-1}`;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosHistorysEndpoint(page, pageSize));
            });
    }



}