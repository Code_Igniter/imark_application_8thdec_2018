﻿


import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { PosHistoryEndpoint } from './pos-history-endpoint.service';
import { AuthService } from '../auth.service';
import { PosHistory } from '../../models/posHistory/pos-history.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { Maintenance } from '../../models/maintenance/maintenance.model';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { DamagePosEdit } from '../../models/configuration/configuration-edit.model';
import { DamagePos } from '../../models/configuration/configuration.model';




export type PosHistoryChangedOperation = "add" | "delete" | "modify";
export type PosHistoryChangedEventArg = { deployment: PosHistory[] | string[], operation: PosHistoryChangedOperation };



@Injectable()
export class PosHistoryService {

    public static readonly posHistoryAddedOperation: PosHistoryChangedOperation = "add";
    public static readonly posHistoryDeletedOperation: PosHistoryChangedOperation = "delete";
    public static readonly posHistoryModifiedOperation: PosHistoryChangedOperation = "modify";

    private _posHistoryChanged = new Subject<PosHistoryChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService, 
        private posHistoryEndpoint: PosHistoryEndpoint) {

    }


    getPosHistory(posHistoryId?: string) {
        return this.posHistoryEndpoint.getPosHistoryEndpoint<DeploymentRequest>(posHistoryId);
    }


    getPosHistoryInfo(page?: number, pageSize?: number) {
        return this.posHistoryEndpoint.getPosHistorysEndpoint<PosHistory[]>(page, pageSize);
    }
    getPosSparesHistoryInfo(page?: number, pageSize?: number) {
        return this.posHistoryEndpoint.getPosSparesHistorysEndpoint<PosHistory[]>(page, pageSize);
    }

    getIndividualHistoryInfo(page?: number, pageSize?: number, id?: string) {
        return this.posHistoryEndpoint.getIndividualHistorysEndpoint<PosHistory[]>(page, pageSize, id);
    }



    newPosHistory(posHistory: PosHistory) {
        return this.posHistoryEndpoint.getNewPosHistoryEndpoint<PosHistory>(posHistory);
    }
     //Subsh
    newPosRetrieve(posHistory: PosHistory, retrieve?: string, deploymentRequestId?: string) {
        return this.posHistoryEndpoint.getNewPosRetrieveEndpoint<PosHistory[]>(posHistory, retrieve, deploymentRequestId);
    }

    //newPosVerifyRetrieve(posHistory: PosHistory, retrieveVerify?: string) {
    //    return this.posHistoryEndpoint.getNewPosRetrieveVerifyEndpoint<PosHistory[]>(posHistory, retrieveVerify);
    //}
    newPosVerifyRetrieve(retrieveVerify?: string, deploymentRequestId?: string) {
        return this.posHistoryEndpoint.getNewPosRetrieveVerifyEndpoint<DeploymentRequest[]>(retrieveVerify, deploymentRequestId);
    }
    undoRetrievePos(undo?: string, deploymentRequestId?: string) {
        return this.posHistoryEndpoint.getUndoRetrievePosEndpoint<DeploymentRequest[]>(undo, deploymentRequestId);
    }
    damagePos(maintenance: DamagePosEdit, deploymentRequestId?: string) {
        return this.posHistoryEndpoint.getNewDamagePosEndpoint<DamagePos>(maintenance, deploymentRequestId);
    }

    maintenancePos(maintenancePos?: string, serialId?: string) {
        return this.posHistoryEndpoint.getMaintenanceRetrieveEndpoint<DeploymentRequest[]>(maintenancePos, serialId);
    }
    newMaintenance(maintenance: MaintenanceEdit, deploymentRequestId?: string) {
        return this.posHistoryEndpoint.getNewMaintenanceEndpoint<Maintenance>(maintenance, deploymentRequestId);
    }
   
    deletePosHistory(posHistoryOrPosHistoryId: string | PosHistory): Observable<PosHistory> {

        if (typeof posHistoryOrPosHistoryId === 'string' || posHistoryOrPosHistoryId instanceof String) {
            return this.posHistoryEndpoint.getDeletePosHistoryEndpoint<PosHistory>(<string>posHistoryOrPosHistoryId);

        }
        else {

            if (posHistoryOrPosHistoryId.id) {
                return this.deletePosHistory(posHistoryOrPosHistoryId.id);
            }
            else {
                return this.posHistoryEndpoint.getPosHistoryByTotalEndpoint<PosHistory>(posHistoryOrPosHistoryId.id)
                    .mergeMap(posHistory => this.deletePosHistory(posHistory.id));
            }
        }
    }

    getPosRecordInfo(page?: number, pageSize?: number) {
        return this.posHistoryEndpoint.getPosRecordsEndPoint<PosHistory[]>(page, pageSize);
    }

    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }

  
}