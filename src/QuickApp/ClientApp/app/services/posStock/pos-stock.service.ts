﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { PosStockEndpoint } from './pos-stock-endpoint.service';
import { AuthService } from '../auth.service';
import { PosStock } from '../../models/posStock/pos-stock.model';
import { PosStockEdit } from '../../models/posStock/pos-stock-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type PosStockChangedOperation = "add" | "delete" | "modify";
export type PosStockChangedEventArg = { posStock: PosStock[] | string[], operation: PosStockChangedOperation };



@Injectable()
export class PosStockService {

    public static readonly posStockAddedOperation: PosStockChangedOperation = "add";
    public static readonly posStockDeletedOperation: PosStockChangedOperation = "delete";
    public static readonly posStockModifiedOperation: PosStockChangedOperation = "modify";

    private _posStockChanged = new Subject<PosStockChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private PosStockEndpoint: PosStockEndpoint) {

    }

    getPosStockInfo(posStockId?: string) {
        return this.PosStockEndpoint.getPosStocksEndpoint<PosStock[]>(posStockId);
    }
    getPosStock(page?: number, pageSize?: number, serialId?:string) {
        return this.PosStockEndpoint.getStockEndpoint<PosStock[]>(page, pageSize, serialId);
    }



    addPosStock(posStock: PosStock, inventoryId?: string, isSale?: boolean) {
        return this.PosStockEndpoint.getPosStockEndpoint<PosStock>(posStock, inventoryId, isSale);
    }


    updateSerial(posStock: PosStockEdit) {
        if (posStock.id) {
            return this.PosStockEndpoint.getUpdatePosStockEndpoint(posStock, posStock.id);
        }
        else {

            return this.PosStockEndpoint.getPosStockByStockEndpoint<PosStock>(posStock.id)

                .mergeMap(foundUser => {
                    posStock.id = foundUser.id;
                    return this.PosStockEndpoint.getUpdatePosStockEndpoint(posStock, posStock.id)
                });
        }
    }

    deletePosStock(posStockId: string | PosStockEdit): Observable<PosStock> {

        if (typeof posStockId === 'string' || posStockId instanceof String) {
            return this.PosStockEndpoint.getDeletePosStockEndpoint<PosStock>(<string>posStockId);
          
        }
        else {

            if (posStockId.id) {
                return this.deletePosStock(posStockId.id);
            }
            else {
                return this.PosStockEndpoint.getPosStockByStockEndpoint<PosStock>(posStockId.id)
                    .mergeMap(serial => this.deletePosStock(posStockId.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}