﻿


import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { RequestTypeEndpoint } from './request-type-endpoint.service';
import { AuthService } from '../auth.service';
import { RequestType } from '../../models/request-type.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type RequestTypeChangedOperation = "add" | "delete" | "modify";
export type RequestTypeChangedEventArg = { request: RequestType[] | string[], operation: RequestTypeChangedOperation };



@Injectable()
export class RequestService {

    public static readonly requestAddedOperation: RequestTypeChangedOperation = "add";
    public static readonly requestDeletedOperation: RequestTypeChangedOperation = "delete";
    public static readonly requestModifiedOperation: RequestTypeChangedOperation = "modify";

    private _requestChanged = new Subject<RequestTypeChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private RequestTypeEndpoint: RequestTypeEndpoint) {

    }


   

    getRequestTypeInfo(page?: number, pageSize?: number) {
        return this.RequestTypeEndpoint.getRequestsEndpoint<RequestType[]>(page, pageSize);
    }


   

    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}