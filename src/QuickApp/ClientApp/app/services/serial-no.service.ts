﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { SerialEndpoint } from './serial-no-endpoint.service';
import { AuthService } from './auth.service';
import { SerialNo } from '../models/serial-no.model';
import { SerialNoEdit } from '../models/serial-no-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../models/permission.model';




export type SerialChangedOperation = "add" | "delete" | "modify";
export type SerialChangedEventArg = { serial: SerialNo[] | string[], operation: SerialChangedOperation };



@Injectable()
export class SerialNoService {

    public static readonly serialAddedOperation: SerialChangedOperation = "add";
    public static readonly serialDeletedOperation: SerialChangedOperation = "delete";
    public static readonly serialModifiedOperation: SerialChangedOperation = "modify";

    private _serialChanged = new Subject<SerialChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private SerialEndpoint: SerialEndpoint) {

    }
    getSerialPage(page?: number, pageSize?: number, id?:string) {
        return this.SerialEndpoint.getSerialPageEndpoint<SerialNo[]>(page, pageSize, id);
    }

    getSerialInfo(inventoryAddId?: string, config?: string) {
        return this.SerialEndpoint.getSerialsEndpoint<SerialNo[]>(inventoryAddId, config);
    }
    getPosStockList(inventoryAddId?: string) {
        return this.SerialEndpoint.getPosStockEndpoint<SerialNo[]>(inventoryAddId);
    }

    getSerialPosStockInfo(inventoryAddId?: string) {
        return this.SerialEndpoint.getSerialsStockEndpoint<SerialNo[]>(inventoryAddId);
    }

  
    addSerial(serial: SerialNo) {
        return this.SerialEndpoint.getSerialEndpoint<SerialNo>(serial);
    }


    updateSerial(serial: SerialNoEdit) {
        if (serial.id) {
            return this.SerialEndpoint.getUpdateSerialEndpoint(serial, serial.id);
        }
        else {

            return this.SerialEndpoint.getSerialBySerialKeyEndpoint<SerialNo>(serial.id)

                .mergeMap(foundUser => {
                    serial.id = foundUser.id;
                    return this.SerialEndpoint.getUpdateSerialEndpoint(serial, serial.id)
                });
        }
    }

    deleteSerial(serialOrSerialId: string | SerialNoEdit): Observable<SerialNo> {

        if (typeof serialOrSerialId === 'string' || serialOrSerialId instanceof String) {
            return this.SerialEndpoint.getDeleteSerialEndpoint<SerialNo>(<string>serialOrSerialId);
            //.do(data => this.onInventoryCountChanged([data], AccountService.posDeletedOperation));
        }
        else {

            if (serialOrSerialId.id) {
                return this.deleteSerial(serialOrSerialId.id);
            }
            else {
                return this.SerialEndpoint.getSerialBySerialKeyEndpoint<SerialNo>(serialOrSerialId.serialKey)
                    .mergeMap(serial => this.deleteSerial(serial.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}