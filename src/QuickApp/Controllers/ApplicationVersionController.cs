﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class ApplicationVersionController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public ApplicationVersionController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;


        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<ApplicationVersionViewModel>))]
       // [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult Get(int page, int pageSize)
        {
            var applicationVersionQuery = _unitOfWork.ApplicationVersions.GetAll().Join(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new ApplicationVersionViewModel
            {
                Id = a.Id,
                BankId= b.Id,
                BankCode=b.BankCode,
                ApplicationDate=a.ApplicationDate,
                KernelType=a.KernelType,
                ApplicationVer=a.ApplicationVer,
                Remarks=a.Remarks

            });



            return Ok(applicationVersionQuery);

        }
        [HttpGet("{id}/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<ApplicationVersionViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult Get(Guid id, int page, int pageSize)
        {
            var bankId = _unitOfWork.DeploymentRequests.Find(x => x.Id == id).Select(s => s.BankId).SingleOrDefault();
            var bankCode = _unitOfWork.Banks.Find(x => x.Id == bankId).Select(s => s.BankCode).FirstOrDefault();
            var applicationVersionQuery = _unitOfWork.ApplicationVersions.GetAll().Join(_unitOfWork.Banks.GetAll().Where(x=>x.BankCode==bankCode), a => a.BankId, b => b.Id, (a, b) => new ApplicationVersionViewModel
            {
                Id = a.Id,
                BankId = b.Id,
                BankCode = b.BankCode,
                ApplicationDate = a.ApplicationDate,
                KernelType = a.KernelType,
                ApplicationVer = a.ApplicationVer,
                Remarks = a.Remarks

            });



            return Ok(applicationVersionQuery);

        }

        // GET api/<controller>/5

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var _applicationVersion = _unitOfWork.ApplicationVersions.GetGuid(id);
                return Ok(_applicationVersion);
            }
            catch
            {
                throw;
            }
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]ApplicationVersionViewModel _applicationVersionVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationVersion applicationVersion = new ApplicationVersion()
            {
                Id = Guid.NewGuid(),
                ApplicationDate = _applicationVersionVM.ApplicationDate,
                BankId=_applicationVersionVM.BankId,
                KernelType = _applicationVersionVM.KernelType,
                ApplicationVer = _applicationVersionVM.ApplicationVer,
                Remarks = _applicationVersionVM.Remarks,
                

            };
            try
            {

                _unitOfWork.ApplicationVersions.Add(applicationVersion);
                _unitOfWork.SaveChanges();

            }
            catch (DbUpdateException)
            {
                if (ItemAlreadyExists(applicationVersion.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }


        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody]ApplicationVersionViewModel _applicationVersionVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationVersion applicationVersion = new ApplicationVersion()
            {
                Id = _applicationVersionVM.Id,
                ApplicationDate = _applicationVersionVM.ApplicationDate,
                ApplicationVer = _applicationVersionVM.ApplicationVer,
                KernelType = _applicationVersionVM.KernelType,
                Remarks = _applicationVersionVM.Remarks,

            };
            try
            {

                _unitOfWork.ApplicationVersions.Update(applicationVersion);
                _unitOfWork.SaveChanges();


            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(ApplicationVersionViewModel))]
        public void Delete(Guid id)
        {
            var _applicationToDelete = _unitOfWork.ApplicationVersions.GetGuid(id);
            try
            {
                _unitOfWork.ApplicationVersions.Remove(_applicationToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
        private bool ItemAlreadyExists(Guid Id)
        {
            int count = _unitOfWork.ApplicationVersions.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
