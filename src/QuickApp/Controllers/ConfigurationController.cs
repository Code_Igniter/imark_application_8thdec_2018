﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class ConfigurationController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public ConfigurationController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;


        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<ConfigurationViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize)
        {
            IEnumerable<Configuration> configurationQuery = _unitOfWork.Configurations.GetAll();

            if (page != -1)
                configurationQuery = configurationQuery.Skip((page - 1) * pageSize);

            if (pageSize != -1)
                configurationQuery = configurationQuery.Take(pageSize);

            var configurationList = configurationQuery.ToList();

            List<ConfigurationViewModel> usersVM = new List<ConfigurationViewModel>();

            foreach (var item in configurationList)
            {
                var userVM = Mapper.Map<ConfigurationViewModel>(item);
                

                usersVM.Add(userVM);
            }

            return Ok(usersVM);

        }

        // GET api/<controller>/5


        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                DeploymentRequest _request = _unitOfWork.DeploymentRequests.GetGuid(id);
                string StatusName = _unitOfWork.Statuses.GetGuid(_request.StatusId).StatusName;
                string RequestName = _unitOfWork.RequestTypes.GetGuid(_request.RequestTypeId).RequestName;
                Configuration _configuration = _unitOfWork.Configurations.GetAll().Where(a => a.DeploymentRequestId == id).FirstOrDefault();
                if (_configuration == null)
                {
                    ConfigurationViewModel _config = new ConfigurationViewModel()
                    {
                        DeploymentRequestId = id,
                        StatusName = StatusName,
                        RequestName = RequestName,
                    };
                    //_config.DeploymentRequestId = id;
                    //_config.StatusName = StatusName;
                    //_config.RequestName = RequestName;
                    return Ok(_config);
                }
                else
                {
                    string serialKey = _unitOfWork.SerialNumbers.GetGuid(_configuration.SerialNumberId).SerialKey;
                    var userVM = Mapper.Map<ConfigurationViewModel>(_configuration);
                    userVM.StatusName = StatusName;
                    userVM.RequestName = RequestName;
                    userVM.SerialKey = serialKey;
                    return Ok(userVM);
                }
              
            }
            catch
            {
                throw;
            }
        }

        [HttpPost("notReceivedPos")]
        public IActionResult NotReceived([FromBody]ConfigurationViewModel _configurationVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var deployId = _unitOfWork.Deployments.Find(x => x.DeploymentRequestId == _configurationVM.DeploymentRequestId).Select(s => s.Id).SingleOrDefault();
            var deploy = _unitOfWork.Deployments.GetGuid(deployId);
            var deploymentReq = _unitOfWork.DeploymentRequests.GetGuid(_configurationVM.DeploymentRequestId);
            var _posHistoryId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == _configurationVM.SerialNumberId).Select(s => s.Id).LastOrDefault();
            var posHistory = _unitOfWork.PosHistorys.GetGuid(_posHistoryId);

           
            int duplicate = _unitOfWork.NotReceivedPos.GetAll().Where(a => a.SerialNumberId == _configurationVM.SerialNumberId).Count();

            if (duplicate > 0)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {
                NotReceivedPos notReceived = new NotReceivedPos()
                {
                    Id = Guid.NewGuid(),
                    DeploymentRequestId = _configurationVM.DeploymentRequestId,
                    SerialNumberId = _configurationVM.SerialNumberId,
                    ApplicationVersionId = _configurationVM.ApplicationVersionId,
                    InventoryId= _configurationVM.InventoryId,
                    InventoryAddId= _configurationVM.InventoryAddId,
                    Status = _configurationVM.Status,
                    DeployedLocation=deploy.MerchantLocation,
                    ReceivedBy=deploy.ReceivedBy,
                    ReceivedContactNo=deploy.Longitude,
                    Merchant=deploymentReq.Merchant,
                    IdMerchant=deploymentReq.IdMerchant,
                    IdTerminal=deploymentReq.IdTerminal,
                    Outlet=deploymentReq.Outlet,
                    District = deploymentReq.District,
                    Address = deploymentReq.Address,
                    ContactPerson = deploymentReq.ContactPerson,
                    ContactNo = deploymentReq.ContactNo1
                };
                try
                {
                    if (posHistory != null)
                    {

                        posHistory.IsReceived = "NOT RECEIVED";
                        posHistory.Scanned = _configurationVM.Status;
                        _unitOfWork.NotReceivedPos.Add(notReceived);
                        _unitOfWork.PosHistorys.Update(posHistory);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                        _unitOfWork.NotReceivedPos.Add(notReceived);
                        _unitOfWork.SaveChanges();
                    }

                }
                catch (DbUpdateException)
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }


        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]ConfigurationViewModel _configurationVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var configSerial = _unitOfWork.Configurations.Find(x=>x.SerialKey==_configurationVM.SerialKey).Select(x => x.SerialKey).FirstOrDefault();
            var inventoryAddId = _unitOfWork.SerialNumbers.Find(x => x.Id == _configurationVM.SerialNumberId).Select(s => s.InventoryAddId).SingleOrDefault();
            var _posId = _unitOfWork.PosStocks.Find(x => x.SerialNumberId == _configurationVM.SerialNumberId).Select(s => s.Id).SingleOrDefault();
            var pos = _unitOfWork.PosStocks.GetGuid(_posId);
         
            if (configSerial == _configurationVM.SerialKey)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {


                Configuration configuration = new Configuration()
                {
                    Id = Guid.NewGuid(),
                    ApplicationVersionId = _configurationVM.ApplicationVersionId,
                    Verification = _configurationVM.Verification,
                    DeploymentRequestId = _configurationVM.DeploymentRequestId,
                    SerialNumberId = _configurationVM.SerialNumberId,
                    InventoryId=_configurationVM.InventoryId,
                    InventoryAddId=_configurationVM.InventoryAddId

                };
                try
                {
                    if (pos == null)
                    {
                        _unitOfWork.Configurations.Add(configuration);
                        _unitOfWork.SaveChanges();
                        
                    }
                    else
                    {
                        pos.Status = "PartialRentalDeployedVerified";
                        configuration.Verification = "PartialRentalConfigured";
                        _unitOfWork.PosStocks.Update(pos);
                        _unitOfWork.Configurations.Add(configuration);
                        _unitOfWork.SaveChanges();

                    }
                }
                catch (DbUpdateException)
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }


        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody]ConfigurationViewModel _configurationVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var inventoryAddId = _unitOfWork.SerialNumbers.Find(x => x.Id == _configurationVM.SerialNumberId).Select(s => s.InventoryAddId).SingleOrDefault();
            var request = _unitOfWork.DeploymentRequests.GetGuid(_configurationVM.DeploymentRequestId);
           

           
            var _posId = _unitOfWork.PosStocks.Find(x => x.SerialNumberId == _configurationVM.SerialNumberId).Select(s=>s.Id).SingleOrDefault();
            var pos = _unitOfWork.PosStocks.GetGuid(_posId);
            var posHistoryId = _unitOfWork.PosHistorys.Find(x => x.DeploymentRequestId == _configurationVM.DeploymentRequestId).Select(s => s.Id).LastOrDefault();
            var posHistory = _unitOfWork.PosHistorys.GetGuid(posHistoryId);
            var deployId = _unitOfWork.Deployments.Find(x => x.DeploymentRequestId == _configurationVM.DeploymentRequestId).Select(s => s.Id).SingleOrDefault();

                var deploy = _unitOfWork.Deployments.GetGuid(deployId);
            if (deploy != null)
            {
                deploy.InventoryAddId = inventoryAddId;
                deploy.Verification = "Pending";
            }
            if (deploy == null)
            {
                Deployment _deploy = new Deployment()
                {
                    Id = Guid.NewGuid(),
                    BankId = request.BankId,
                    DeploymentRequestId = request.Id,
                    InventoryId = request.InventoryId,
                    PrimaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == request.InventoryId).Select(x => x.PrimaryItem).FirstOrDefault(),
                    SecondaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == request.InventoryId).Select(x => x.SecondaryItem).FirstOrDefault(),
                    TertiaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == request.InventoryId).Select(x => x.TertiaryItem).FirstOrDefault(),
                    DetailItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == request.InventoryId).Select(x => x.DetailItem).FirstOrDefault(),
                    DeployDate = DateTime.UtcNow.AddHours(5).AddMinutes(45),
                    Verification = null

                };
            }
            _configurationVM.InventoryId = request.InventoryId;
          

            if (_configurationVM.Verification == null || _configurationVM.Verification == "Pending")
            {
                request.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "Configured").Select(a => a.Id).FirstOrDefault();
                _configurationVM.Verification = "Pending";
         
            }

           
            if (_configurationVM.Verification == "Verified")
            {
                request.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "ConfigurationVerified").Select(a => a.Id).FirstOrDefault();
         

            }
           


            var maintenanceSerial = _unitOfWork.Maintenances.Find(x => x.SerialNumberId == _configurationVM.SerialNumberId).Select(s => s.SerialNumberId).SingleOrDefault();
            var notReceivedSerial = _unitOfWork.NotReceivedPos.Find(x => x.SerialNumberId == _configurationVM.SerialNumberId).Select(s => s.SerialNumberId).SingleOrDefault();
            if (maintenanceSerial== _configurationVM.SerialNumberId || notReceivedSerial == _configurationVM.SerialNumberId )
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {


                var configuration = Mapper.Map<Configuration>(_configurationVM);
                configuration.Id = _configurationVM.Id;
                configuration.InventoryAddId = inventoryAddId;
                configuration.IsSale = _configurationVM.IsSale;
                if (_configurationVM.Verification == "Verified" && pos != null)
                {
                    request.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "ConfigurationVerified").Select(a => a.Id).FirstOrDefault();

                    pos.DeploymentId = _configurationVM.DeploymentRequestId;
                    pos.Status = "PartialRentalDeployedVerified";
                    configuration.Verification = "PartialRentalConfigured";
                }
                try
                {
                    if(pos==null && posHistory==null )
                    {

                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.Deployments.Update(deploy);
                        _unitOfWork.SaveChanges();
                    }

                    //else if (pos == null && posHistory != null)
                    //{

                    //    _unitOfWork.DeploymentRequests.Update(request);
                    //    _unitOfWork.Configurations.Update(configuration);
                    //    _unitOfWork.Deployments.Update(deploy);
                    //    _unitOfWork.SaveChanges();
                    //}

                    else if (pos!=null && posHistory == null )
                    {
                        pos.Status = "PartialRentalDeployedVerified";
                        configuration.Verification = "PartialRentalConfigured";
                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.PosStocks.Update(pos);
                        _unitOfWork.SaveChanges();
                    }
                    else if (pos != null && posHistory != null)
                    {
                        pos.Status = "PartialRentalDeployedVerified";
                        configuration.Verification = "PartialRentalConfigured";
                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.Deployments.Update(deploy);

                        _unitOfWork.PosStocks.Update(pos);
                        _unitOfWork.SaveChanges();
                    }
                    else if (pos==null && posHistory != null && _configurationVM.Status == null )
                    {
                        posHistory.IsReceived = "IN USE";
                        posHistory.Scanned = "NOT REPLACED";
                        _unitOfWork.PosHistorys.Update(posHistory);
                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.Deployments.Update(deploy);
                        _unitOfWork.SaveChanges();
                    }
                    else if (pos == null && posHistory != null && _configurationVM.PreviousSerial=="!received" )
                    {
                        var notReceivedId = _unitOfWork.NotReceivedPos.Find(x => x.SerialNumberId == posHistory.SerialNumberId).Select(s => s.Id).SingleOrDefault();
                        var notReceived = _unitOfWork.NotReceivedPos.GetGuid(notReceivedId);

                        notReceived.Status = _configurationVM.Status;
                        
                        posHistory.IsReceived = "NOT RECEIVED";
                        posHistory.Scanned = _configurationVM.Status;
                        _unitOfWork.NotReceivedPos.Update(notReceived);
                        _unitOfWork.PosHistorys.Update(posHistory);
                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.Deployments.Update(deploy);
                        _unitOfWork.SaveChanges();
                    }
                    else if (pos == null && posHistory != null && _configurationVM.PreviousSerial == "received")
                    {

                        posHistory.IsReceived = "RECEIVED";
                        posHistory.Scanned = "IN STOCK";
                        _unitOfWork.PosHistorys.Update(posHistory);
                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.Deployments.Update(deploy);
                        _unitOfWork.SaveChanges();

                       
                    }

                    else if (pos == null && _configurationVM.PreviousSerial == "received" && posHistory == null)
                    {
     
                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.Deployments.Update(deploy);
                        //_unitOfWork.PosStocks.Update(pos);
                        _unitOfWork.SaveChanges();
                    }

                    else
                    {
                        _unitOfWork.DeploymentRequests.Update(request);
                        _unitOfWork.Configurations.Update(configuration);
                        _unitOfWork.Deployments.Update(deploy);
                        _unitOfWork.SaveChanges();

                    }
                    //_unitOfWork.Deployments.Update(deploy);
                  
                }
                catch (DbUpdateException)
                {

                    throw;

                }
            }
            

            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        [HttpPut("addConfigStatus/{id}")]
        public IActionResult PostStatus([FromBody]ConfigurationViewModel _configurationVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var config = _unitOfWork.Configurations.GetGuid(_configurationVM.Id);
            config.Status = _configurationVM.Status;

            try
            {

                _unitOfWork.Configurations.Update(config);
                _unitOfWork.SaveChanges();
                return new StatusCodeResult(StatusCodes.Status200OK);
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(ConfigurationViewModel))]
        public void Delete(Guid id)
        {
            var _configToDelete = _unitOfWork.Configurations.GetGuid(id);
            try
            {
                _unitOfWork.Configurations.Remove(_configToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
        private bool ItemAlreadyExists(Guid Id)
        {
            int count = _unitOfWork.Configurations.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
