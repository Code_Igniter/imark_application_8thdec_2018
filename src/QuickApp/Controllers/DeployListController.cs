﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class DeployListController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;
        public DeployListController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            this._unitOfWork = unitOfWork;
            _context = context;

        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<DeploymentRequestViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize)
        {
            

            IEnumerable<DeploymentAddRequestViewModel> deployQuery = _unitOfWork.DeploymentRequests.GetAll().Join(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new { DeploymentRequest = a, Bank = b })
                                              .Join(_unitOfWork.Configurations.GetAll(), c => c.DeploymentRequest.Id, d => d.DeploymentRequestId, (c, d) => new { c.DeploymentRequest, c.Bank, Configuration = d })
                                              .Join(_unitOfWork.ApplicationVersions.GetAll(), e => e.Configuration.ApplicationVersionId, f => f.Id, (e, f) => new { e.DeploymentRequest, e.Bank, e.Configuration, Application = f })
                                              .Join(_unitOfWork.Deployments.GetAll(), g => g.DeploymentRequest.Id, h => h.DeploymentRequestId, (g, h) => new { g.DeploymentRequest, g.Bank, g.Configuration, g.Application, Deployment = h })
                                              .Join(_context.Users, x => x.Deployment.CreatedBy, y => y.Id, (x, y) => new { x.DeploymentRequest, x.Bank, x.Configuration, x.Application,x.Deployment, User = y })
                                              .Join(_unitOfWork.Inventorys.GetAll(), k => k.DeploymentRequest.InventoryId, m => m.Id, (k, m) => new { k.DeploymentRequest, k.Bank, k.Configuration, k.Application,k.Deployment, k.User, Inventory = m })
                                              .Join(_unitOfWork.Statuses.GetAll(), p => p.DeploymentRequest.StatusId, q => q.Id, (p, q) => new { p.DeploymentRequest, p.Bank, p.Configuration, p.Application,p.Deployment, p.User, p.Inventory, Status = q })
                                              .Join(_unitOfWork.RequestTypes.GetAll(), p => p.DeploymentRequest.RequestTypeId, q => q.Id, (p, q) => new { p.DeploymentRequest, p.Bank, p.Configuration, p.Application,p.Deployment, p.User, p.Inventory, p.Status, RequestType = q })
  
                                              .Select(s => new DeploymentAddRequestViewModel
                                              {
                                                  Id = s.DeploymentRequest.Id,

                                                  BankName = s.Bank.BankName,
                                                  BankCode = s.Bank.BankCode,
                                                  Merchant = s.DeploymentRequest.Merchant,
                                                  IdMerchant = s.DeploymentRequest.IdMerchant,
                                                  IdTerminal = s.DeploymentRequest.IdTerminal,
                                                  Outlet = s.DeploymentRequest.Outlet,
                                                  Address = s.DeploymentRequest.Address,
                                                  ContactPerson = s.DeploymentRequest.ContactPerson,
                                                  ContactNo1 = s.DeploymentRequest.ContactNo1,
                                                  InventoryId = s.DeploymentRequest.InventoryId,
                                                  BankId = s.DeploymentRequest.BankId,
                                                  Remarks = s.DeploymentRequest.Remarks,
                                                  CreatedDate=s.DeploymentRequest.CreatedDate,
                                                  SerialKey = s.Configuration.SerialKey,
                                                  PreviousSerial = s.Configuration.PreviousSerial,
                                                  ConfigStatus = s.Configuration.Status,
                                                  ApplicationVersion = s.Bank.BankCode + " " + s.Application.ApplicationVer + " " + s.Application.ApplicationDate.Value.ToShortDateString() + " " + s.Application.KernelType,
                                                  Item = s.Inventory.PrimaryItem + ' ' + s.Inventory.SecondaryItem + ' ' + s.Inventory.TertiaryItem + ' ' + s.Inventory.DetailItem,
                                                  StatusForDeploy=s.Deployment.StatusForDeploy,
                                                  CreatedBy = s.User.FullName,
                                                  StatusName = s.Status.StatusName,
                                                  RequestName = s.RequestType.RequestName,
                                                  StatusId = s.DeploymentRequest.StatusId,
                                                  RequestTypeId = s.DeploymentRequest.RequestTypeId

                                              }).Where(a => a.StatusName == "ConfigurationVerified"  || a.StatusName == "Deployed" && a.RequestName != "Delete").OrderByDescending(x => x.CreatedDate);



            return Ok(deployQuery);

        }
        

       // GET api/<controller>/5

       [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var _deploymentRequest = await _unitOfWork.DeploymentRequests.GetGuidAsync(id);

                return Ok(_deploymentRequest);

            }
            catch
            {
                throw;
            }
        }

       
      
    }
}
