﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class DeploymentRequestController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;
        public DeploymentRequestController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            this._unitOfWork = unitOfWork;
            _context = context;

        }


        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<DeploymentAddRequestViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize)
        {
                      
            IEnumerable<DeploymentAddRequestViewModel> deploymentRequestQuery = _unitOfWork.DeploymentRequests.GetAll().Join(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new { DeploymentRequest = a, Bank = b })
                                                .GroupJoin(_unitOfWork.Configurations.GetAll(), k => k.DeploymentRequest.Id, m => m.DeploymentRequestId, (k, m) => new { k.DeploymentRequest, k.Bank, configurations = m })
                                                .Join(_context.Users, x => x.DeploymentRequest.CreatedBy, y => y.Id, (x, y) => new { x.DeploymentRequest, x.Bank,x.configurations, User = y })
                                                .Join(_unitOfWork.Inventorys.GetAll(), k => k.DeploymentRequest.InventoryId, m => m.Id, (k, m) => new { k.DeploymentRequest, k.Bank, k.configurations, k.User, Inventory = m })
                                                .Join(_unitOfWork.Statuses.GetAll(), p => p.DeploymentRequest.StatusId, q => q.Id, (p, q) => new { p.DeploymentRequest, p.Bank, p.configurations, p.User, p.Inventory, Status = q })
                                                .Join(_unitOfWork.RequestTypes.GetAll(), p => p.DeploymentRequest.RequestTypeId, q => q.Id, (p, q) => new { p.DeploymentRequest, p.Bank, p.configurations, p.User, p.Inventory, p.Status, RequestType = q })
                                                .Select(s => new DeploymentAddRequestViewModel
                                                {


                                                    Id = s.DeploymentRequest.Id,

                                                    BankName = s.Bank.BankName,
                                                    BankCode = s.Bank.BankCode,
                                                    Merchant = s.DeploymentRequest.Merchant,
                                                    IdMerchant = s.DeploymentRequest.IdMerchant,
                                                    IdTerminal = s.DeploymentRequest.IdTerminal,
                                                    Outlet = s.DeploymentRequest.Outlet,
                                                    Address = s.DeploymentRequest.Address,
                                                    ContactPerson = s.DeploymentRequest.ContactPerson,
                                                    ContactNo1 = s.DeploymentRequest.ContactNo1,
                                                    InventoryId = s.DeploymentRequest.InventoryId,
                                                    BankId = s.DeploymentRequest.BankId,
                                                    Remarks = s.DeploymentRequest.Remarks,
                                                    District = s.DeploymentRequest.District,
                                                    ContactNo2 = s.DeploymentRequest.ContactNo2,
                                                    PrimaryNacNumber = s.DeploymentRequest.PrimaryNacNumber,
                                                    SecondaryNacNumber = s.DeploymentRequest.SecondaryNacNumber,
                                                    Currency = s.DeploymentRequest.Currency,
                                                    Priority = s.DeploymentRequest.Priority,
                                                    RemarksForDeploy=s.DeploymentRequest.RemarksForDeploy,
                                                    TipAdjustment = s.DeploymentRequest.TipAdjustment,
                                                    PreAuthorization = s.DeploymentRequest.PreAuthorization,
                                                    ManualTransaction = s.DeploymentRequest.ManualTransaction,
                                                    Refund = s.DeploymentRequest.Refund,
                                                    SerialKey = s.configurations.Select(u=>u.SerialKey).FirstOrDefault()==null?"no serialano": s.configurations.Select(u => u.SerialKey).FirstOrDefault(),
                                                    //PreviousSerial = s.Configuration.PreviousSerial,
                                                    //ConfigStatus = s.Configuration.Status,
                                                    //ApplicationVersion = s.Bank.BankCode + " " + s.Application.ApplicationVer + " " + s.Application.ApplicationDate.Value.ToShortDateString() + " " + s.Application.KernelType,
                                                    Item = s.Inventory.PrimaryItem + ' ' + s.Inventory.SecondaryItem + ' ' + s.Inventory.TertiaryItem + ' ' + s.Inventory.DetailItem,


                                                    CreatedDate = s.DeploymentRequest.CreatedDate,

                                                    CreatedBy = s.User.FullName,
                                                    StatusName = s.Status.StatusName,
                                                    RequestName = s.RequestType.RequestName,
                                                    StatusId = s.DeploymentRequest.StatusId,
                                                    RequestTypeId = s.DeploymentRequest.RequestTypeId

                                                }).Where(a => a.StatusName == "Requested" || a.StatusName == "RequestVerified" || a.StatusName == "Configured" ||
                                                a.StatusName == "PartialRentalConfigured" || a.StatusName == "Retrieve").OrderByDescending(x => x.CreatedDate);



            return Ok(deploymentRequestQuery);

        }


        

        [HttpGet("Undeployedpartial/{bankName}/{deploymentReqId}/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<InventoryandAddViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult GetUndeployed(Guid bankName, Guid deploymentReqId, int page, int pageSize)
        {

            //var bankID = _unitOfWork.Banks.GetAll().Where(x => x.Status == "Partial Rental" || x.Status == "Both").Select(s => s.Id).SingleOrDefault();
            //var inventId = _unitOfWork.InventorysAdd.GetAll().Select(s => s.Id).SingleOrDefault();
            var depReq = _unitOfWork.DeploymentRequests.GetGuid(deploymentReqId);
            if (depReq == null)
            {
                var bankStatus = _unitOfWork.Banks.Find(x => x.Id == bankName).Select(s => s.Status).FirstOrDefault();
                var posSoldQuery = _unitOfWork.Inventorys.GetAll().GroupJoin(_unitOfWork.PosStocks.GetAll().Where(x => x.BankId == bankName), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    BankId = bankName,
                    BankName = b.Select(s => s.BankName).FirstOrDefault(),
                    BankStatus = bankStatus,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = _unitOfWork.PosStocks.GetAll().Where(x => x.InventoryId == a.Id && x.BankId == bankName).Select(s => s.Id).Count(),
                    Deployed = _unitOfWork.PosStocks.GetAll().Where(x => x.InventoryId == a.Id && x.BankId == bankName && x.Status == "PartialRentalDeployedVerified").Select(s => s.Id).Count(),
                });

                return Ok(posSoldQuery);
            }
            else
            {
                //var depReq = _unitOfWork.DeploymentRequests.GetGuid(deploymentReqId);
                var bankId = _unitOfWork.Banks.GetGuid(depReq.BankId).Id;
                var bankStatus = _unitOfWork.Banks.Find(x => x.Id == bankId).Select(s => s.Status).FirstOrDefault();
                var posSoldQuery = _unitOfWork.Inventorys.GetAll().GroupJoin(_unitOfWork.PosStocks.GetAll().Where(x => x.BankId == bankName), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    DeploymentRequestId = depReq.Id,
                    BankId = bankName,
                    BankName = b.Select(s => s.BankName).FirstOrDefault(),
                    BankStatus = bankStatus,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = _unitOfWork.PosStocks.GetAll().Where(x => x.InventoryId == a.Id && x.BankId == bankId).Select(s => s.Id).Count(),
                    Deployed = _unitOfWork.PosStocks.GetAll().Where(x => x.InventoryId == a.Id && x.BankId == bankId && x.Status == "PartialRentalDeployedVerified").Select(s => s.Id).Count(),
                });

                return Ok(posSoldQuery);

            }

        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var _deploymentRequest = await _unitOfWork.DeploymentRequests.GetGuidAsync(id);



                return Ok(_deploymentRequest);

            }
            catch
            {
                throw;
            }
        }


        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]DeploymentRequestViewModel _depRequestVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_depRequestVM.Verification == null)
            {
                _depRequestVM.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "Requested").Select(a => a.Id).FirstOrDefault();
                _depRequestVM.Verification = "Pending";
                _depRequestVM.RequestTypeId = _unitOfWork.RequestTypes.GetAll().Where(a => a.RequestName == "New").Select(a => a.Id).FirstOrDefault();
            }

            var bankStatus = _unitOfWork.Banks.GetGuid(_depRequestVM.BankId).Status;

            DeploymentRequest depRequest = new DeploymentRequest()
            {
                Id = Guid.NewGuid(),
                BankId = _depRequestVM.BankId,
                InventoryId = _depRequestVM.InventoryId,
                Address = _depRequestVM.Address,
                ContactNo1 = _depRequestVM.ContactNo1,
                ContactNo2 = _depRequestVM.ContactNo2,
                ContactPerson = _depRequestVM.ContactPerson,
                Currency = _depRequestVM.Currency,
                District = _depRequestVM.District,
                ManualTransaction = _depRequestVM.ManualTransaction,
                IdMerchant = _depRequestVM.IdMerchant,
                Outlet = _depRequestVM.Outlet,
                Merchant = _depRequestVM.Merchant,
                PreAuthorization = _depRequestVM.PreAuthorization,
                PrimaryNacNumber = _depRequestVM.PrimaryNacNumber,
                Priority = _depRequestVM.Priority,
                Remarks = _depRequestVM.Remarks,
                RemarksForDeploy = _depRequestVM.RemarksForDeploy,
                SecondaryNacNumber = _depRequestVM.SecondaryNacNumber,
                IdTerminal = _depRequestVM.IdTerminal,
                TerminalType = _depRequestVM.TerminalType,
                TipAdjustment = _depRequestVM.TipAdjustment,
                Refund = _depRequestVM.Refund,
                RequestTypeId = _depRequestVM.RequestTypeId,
                StatusId = _depRequestVM.StatusId,
                Verification = _depRequestVM.Verification,
                RentalInventoryAdd = _depRequestVM.RentalInventoryAdd
            };
            try
            {

                if (_depRequestVM.IsImarkProperty == true) { 
                    depRequest.RentalInventoryAdd = "RentalInventory";
                _unitOfWork.DeploymentRequests.Add(depRequest);

                _unitOfWork.SaveChanges();
            }
                else if(bankStatus == "Rental")
                {
                    depRequest.RentalInventoryAdd = "Rental";
                    _unitOfWork.DeploymentRequests.Add(depRequest);

                    _unitOfWork.SaveChanges();
                }
                else{
                    depRequest.RentalInventoryAdd = "PartialRentalInventory";
                    _unitOfWork.DeploymentRequests.Add(depRequest);

                    _unitOfWork.SaveChanges();
                }

            }
            catch (DbUpdateException)
            {
                if (ItemAlreadyExists(depRequest.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }


        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody]DeploymentRequestViewModel _depRequestVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var bankName = _unitOfWork.Banks.GetAll().Where(x => x.Status == true).Select(s => s.BankName).FirstOrDefault();
            if (_depRequestVM.Verification == "")
            {
                _depRequestVM.Verification = "Pending";

            }
            if (_depRequestVM.Verification == "Verified")

            {
                _depRequestVM.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "RequestVerified").Select(a => a.Id).FirstOrDefault();

            }

            //  var statusId = _unitOfWork.Statuses.GetAll().Where(x => x.StatusName == "PartialRental").Select(s => s.Id).SingleOrDefault();

            var bankStatus = _unitOfWork.Banks.GetGuid(_depRequestVM.BankId).Status;
            var requestName = _unitOfWork.RequestTypes.GetAll().Where(s => s.Id == _depRequestVM.RequestTypeId).Select(x => x.RequestName).FirstOrDefault();
            DeploymentRequest depRequest = new DeploymentRequest()
            {
                Id = _depRequestVM.Id,
                BankId = _depRequestVM.BankId,
                InventoryId = _depRequestVM.InventoryId,
                Address = _depRequestVM.Address,
                Verification = _depRequestVM.Verification,
                ContactNo1 = _depRequestVM.ContactNo1,
                ContactNo2 = _depRequestVM.ContactNo2,
                ContactPerson = _depRequestVM.ContactPerson,
                Currency = _depRequestVM.Currency,
                District = _depRequestVM.District,
                ManualTransaction = _depRequestVM.ManualTransaction,
                IdMerchant = _depRequestVM.IdMerchant,
                Outlet = _depRequestVM.Outlet,
                Merchant = _depRequestVM.Merchant,
                PreAuthorization = _depRequestVM.PreAuthorization,
                PrimaryNacNumber = _depRequestVM.PrimaryNacNumber,
                Priority = _depRequestVM.Priority,
                Remarks = _depRequestVM.Remarks,
                RemarksForDeploy = _depRequestVM.RemarksForDeploy,
                RequestTypeId = _depRequestVM.RequestTypeId,
                SecondaryNacNumber = _depRequestVM.SecondaryNacNumber,
                StatusId = _depRequestVM.StatusId,
                IdTerminal = _depRequestVM.IdTerminal,
                TerminalType = _depRequestVM.TerminalType,
                TipAdjustment = _depRequestVM.TipAdjustment,
                Refund = _depRequestVM.Refund,
                RentalInventoryAdd = _depRequestVM.RentalInventoryAdd


            };
            try
            {
                if (requestName == "New" && _depRequestVM.Verification == "Verified" && bankStatus == "Rental")
                {
                    Deployment _deploy = new Deployment()
                    {
                        Id = Guid.NewGuid(),
                        BankId = _depRequestVM.BankId,
                        DeploymentRequestId = _depRequestVM.Id,
                        InventoryId = _depRequestVM.InventoryId,
                        PrimaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.PrimaryItem).FirstOrDefault(),
                        SecondaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.SecondaryItem).FirstOrDefault(),
                        TertiaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.TertiaryItem).FirstOrDefault(),
                        DetailItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.DetailItem).FirstOrDefault(),
                        DeployDate = DateTime.UtcNow.AddHours(5).AddMinutes(45),
                        Verification = null


                    };
                   
                    depRequest.RentalInventoryAdd = "Rental";
                    _unitOfWork.Deployments.Add(_deploy);
                    _unitOfWork.DeploymentRequests.Update(depRequest);
                    _unitOfWork.SaveChanges();
                }
                else if (requestName == "New" && _depRequestVM.Verification == "Verified" && (bankStatus == "both" || bankStatus == "Partial Rental"))
                {
                    Deployment _deploy = new Deployment()
                    {
                        Id = Guid.NewGuid(),
                        BankId = _depRequestVM.BankId,
                        DeploymentRequestId = _depRequestVM.Id,
                        InventoryId = _depRequestVM.InventoryId,
                        PrimaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.PrimaryItem).FirstOrDefault(),
                        SecondaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.SecondaryItem).FirstOrDefault(),
                        TertiaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.TertiaryItem).FirstOrDefault(),
                        DetailItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.DetailItem).FirstOrDefault(),
                        DeployDate = DateTime.UtcNow.AddHours(5).AddMinutes(45),
                        Verification = null


                    };
                    if (_depRequestVM.IsImarkProperty == true)
                    {
                        depRequest.RentalInventoryAdd = "RentalInventory";
                    }
                  
                    else
                    {
                        depRequest.RentalInventoryAdd = "PartialRentalInventory";
                    }
                    _unitOfWork.Deployments.Add(_deploy);
                    _unitOfWork.DeploymentRequests.Update(depRequest);
                    _unitOfWork.SaveChanges();
                }
               else if ((requestName == "Reconfigure" || requestName == "Retrieve") && _depRequestVM.Verification == "Verified")
                {
                    if (_depRequestVM.IsImarkProperty == true)
                    {
                        depRequest.RentalInventoryAdd = "RentalInventory";
                    }
                  
                    else
                    {
                        depRequest.RentalInventoryAdd = "PartialRentalInventory";
                    }
                     var _deploy = _unitOfWork.Deployments.Find(x => x.DeploymentRequestId == _depRequestVM.Id).FirstOrDefault();
                    _deploy.InventoryId = _depRequestVM.InventoryId;
                       _deploy.PrimaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.PrimaryItem).FirstOrDefault();
                    _deploy.SecondaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.SecondaryItem).FirstOrDefault();
                    _deploy.TertiaryItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.TertiaryItem).FirstOrDefault();
                    _deploy.DetailItem = _unitOfWork.Inventorys.GetAll().Where(s => s.Id == _depRequestVM.InventoryId).Select(x => x.DetailItem).FirstOrDefault();
                    _deploy.DeployDate = DateTime.UtcNow.AddHours(5).AddMinutes(45);
                        _deploy.Verification = null;
                    _unitOfWork.Deployments.Update(_deploy);
                    _unitOfWork.DeploymentRequests.Update(depRequest);

                    _unitOfWork.SaveChanges();
                }
                else
                { 
                    depRequest.RentalInventoryAdd = "PartialRentalInventory";
                    _unitOfWork.DeploymentRequests.Update(depRequest);

                    _unitOfWork.SaveChanges();
                }
            }

            //try
            //{

            //        _unitOfWork.DeploymentRequests.Update(depRequest);
            //    _unitOfWork.Deployments.Update(_deploy);


            //    _unitOfWork.SaveChanges();

            //}
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }


        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(DeploymentRequestViewModel))]
        public void Delete(Guid id)
        {
            var _depRequestToDelete = _unitOfWork.DeploymentRequests.GetGuid(id);
            try
            {
                _unitOfWork.DeploymentRequests.Remove(_depRequestToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }

        private bool ItemAlreadyExists(Guid Id)
        {
            int count = _unitOfWork.DeploymentRequests.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
