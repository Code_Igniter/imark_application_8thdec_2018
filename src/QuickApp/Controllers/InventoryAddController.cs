﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class InventoryAddController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;
        public InventoryAddController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            this._unitOfWork = unitOfWork;
            _context = context;
        }
       

        [HttpGet("terminalType/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<InventoryandAddViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult Get(int page, int pageSize)
        {
            try
            {
                var inventorys = _unitOfWork.Inventorys.GetAll().Where(x => x.HasSerialNo == true).ToList();
                var InventoryInfo = inventorys.GroupJoin(_unitOfWork.InventorysAdd.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                  
                    HasSerialNo = a.HasSerialNo,
                    BuyDate = b.Select(s => s.BuyDate).FirstOrDefault(),
                    TotalNumber = _unitOfWork.InventorysAdd.Find(x=>x.InventoryId==a.Id).Sum(s=>s.TotalNumber),
                    Remarks = b.Select(s => s.Remarks).FirstOrDefault()

                }).ToList();

                var totalStock = InventoryInfo.GroupJoin(_unitOfWork.PosStocks.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    InventoryAddId = a.InventoryAddId,

                    TotalNumber = a.TotalNumber,
                    TotalSoldDeploy = _unitOfWork.PosStocks.GetAll().Where(s => s.InventoryId == a.Id && s.Status == "undeployed").Select(s => s.Id).Count(),
                    SoldNumber = b.Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count()

                }).GroupJoin(_unitOfWork.DeploymentRequests.GetAll().Where(x => x.Verification == "Pending"), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,


                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    PendingDeploymentRequest=b.Where(s => s.InventoryId == a.Id && s.Verification=="Pending").Select(s=>s.Id).Count()

                }).GroupJoin(_unitOfWork.Configurations.GetAll().Where(x => x.Verification == "Verified" || x.Verification=="Pending" ), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,


                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy=a.TotalSoldDeploy,
                    PendingDeploymentRequest=a.PendingDeploymentRequest,
                    ConfigureNumber = b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count()

                }).GroupJoin(_unitOfWork.Deployments.GetAll().Where(x => x.Verification == "Verified"), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    PendingDeploymentRequest = a.PendingDeploymentRequest,
                    ConfigureNumber = a.ConfigureNumber,
                    DeployNumber = b.Where(x => x.InventoryId == a.Id && x.Verification == "Verified").Select(s => s.Id).Count()
                }).GroupJoin(_unitOfWork.Deployments.GetAll().Where(x => x.Verification == "Pending"), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    PendingDeploymentRequest = a.PendingDeploymentRequest,
                    ConfigureNumber = a.ConfigureNumber,
                    DeployNumber=a.DeployNumber,
                    PendingDeploy = b.Where(x => x.InventoryId == a.Id && x.Verification == "Pending").Select(s => s.Id).Count()
                }).GroupJoin(_unitOfWork.Maintenances.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,


                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    PendingDeploymentRequest = a.PendingDeploymentRequest,
                    ConfigureNumber = a.ConfigureNumber,
                    DeployNumber = a.DeployNumber,
                    PendingDeploy=a.PendingDeploy,
                    MaintenanceNumber = b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count(),
                }).GroupJoin(_unitOfWork.NotReceivedPos.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,



                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    PendingDeploymentRequest = a.PendingDeploymentRequest,
                    ConfigureNumber = a.ConfigureNumber,
                    DeployNumber = a.DeployNumber,
                    PendingDeploy = a.PendingDeploy,
                    MaintenanceNumber = a.MaintenanceNumber,

                    NotReceivedNumber = b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count()


                }).GroupJoin(_unitOfWork.DamagePos.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,

                     Item=a.PrimaryItem+" "+a.SecondaryItem+" "+a.TertiaryItem+" "+a.DetailItem,

                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    PendingDeploymentRequest = a.PendingDeploymentRequest,
                    ConfigureNumber = a.ConfigureNumber,
                    DeployNumber = a.DeployNumber,
                    PendingDeploy = a.PendingDeploy,
                    MaintenanceNumber = a.MaintenanceNumber,
                    NotReceivedNumber = a.NotReceivedNumber,
                    DamageNumber = b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count(),
                    Available = a.TotalNumber  - a.ConfigureNumber - a.TotalSoldDeploy - a.MaintenanceNumber - a.NotReceivedNumber - b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count(),
                });
                return Ok(totalStock);

            }

            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<InventoryandAddViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult GetInventoryAdd(int page, int pageSize)
        {
            try
            {
                var inventorys = _unitOfWork.InventorysAdd.GetAll().ToList();
                var InventoryInfo = inventorys.GroupJoin(_unitOfWork.Inventorys.GetAll().Where(x => x.HasSerialNo == true), a => a.InventoryId, b => b.Id, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    InventoryId=a.InventoryId,
                    PrimaryItem = b.Where(x=>x.HasSerialNo==true).Select(s=>s.PrimaryItem).FirstOrDefault(),
                    SecondaryItem = b.Where(x => x.HasSerialNo == true).Select(s => s.SecondaryItem).FirstOrDefault(),
                    TertiaryItem = b.Where(x => x.HasSerialNo == true).Select(s => s.TertiaryItem).FirstOrDefault(),
                    DetailItem = b.Where(x => x.HasSerialNo == true).Select(s => s.DetailItem).FirstOrDefault(),
                    HasSerialNo=b.Select(s=>s.HasSerialNo==true).FirstOrDefault(),
                    BuyDate = a.BuyDate,
                    TotalNumber = a.TotalNumber,
                    Remarks = a.Remarks

                }).ToList();

                var totalStock = InventoryInfo.GroupJoin(_unitOfWork.PosStocks.GetAll(), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    InventoryId=a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                   HasSerialNo=a.HasSerialNo,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    SoldNumber = b.Where(s => s.InventoryAddId == a.Id).Select(s => s.Id).Count(),
                    TotalSoldDeploy = _unitOfWork.PosStocks.GetAll().Where(s => s.InventoryAddId == a.Id && s.Status == "undeployed").Select(s => s.Id).Count(), //??????

                }).GroupJoin(_unitOfWork.Configurations.GetAll().Where(x => x.Verification == "Verified" || x.Verification=="Pending" ), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    HasSerialNo = a.HasSerialNo,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy=a.TotalSoldDeploy,
                    ConfigureNumber = b.Where(x => x.InventoryAddId == a.Id).Select(s => s.Id).Count(),
                    ReConfigureNumber = b.Where(x => x.InventoryAddId == a.Id && (x.Verification==null || x.Verification=="Pending")).Select(s => s.Id).Count(),


                }).GroupJoin(_unitOfWork.Deployments.GetAll().Where(x=>x.Verification=="Verified"), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    HasSerialNo = a.HasSerialNo,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    ReConfigureNumber=a.ReConfigureNumber,
                    DeployNumber = b.Where(x => x.InventoryAddId == a.Id && x.Verification == "Verified").Select(s => s.Id).Count()
                })
                .GroupJoin(_unitOfWork.Deployments.GetAll().Where(x => x.Verification == "Pending"), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    HasSerialNo = a.HasSerialNo,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    ReConfigureNumber = a.ReConfigureNumber,
                    DeployNumber=a.DeployNumber,
                    PendingDeploy = b.Where(x => x.InventoryAddId == a.Id && x.Verification == "Pending").Select(s => s.Id).Count()
                }).GroupJoin(_unitOfWork.Maintenances.GetAll(), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    HasSerialNo = a.HasSerialNo,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    ReConfigureNumber = a.ReConfigureNumber,
                    DeployNumber = a.DeployNumber,
                    PendingDeploy=a.PendingDeploy,
                    MaintenanceNumber = b.Where(x => x.InventoryAddId == a.Id).Select(s => s.Id).Count(),
                }).GroupJoin(_unitOfWork.NotReceivedPos.GetAll(), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    HasSerialNo = a.HasSerialNo,

                    TotalNumber = a.TotalNumber,
                    AvailableAfterSold = a.AvailableAfterSold,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    ReConfigureNumber = a.ReConfigureNumber,
                    DeployNumber = a.DeployNumber,
                    PendingDeploy = a.PendingDeploy,
                    MaintenanceNumber = a.MaintenanceNumber,
                    NotReceivedNumber = b.Where(x => x.InventoryAddId == a.Id).Select(s => s.Id).Count()


                }).GroupJoin(_unitOfWork.DamagePos.GetAll(), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    HasSerialNo = a.HasSerialNo,

                    TotalNumber = a.TotalNumber,
           
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    ReConfigureNumber = a.ReConfigureNumber,
                    DeployNumber = a.DeployNumber,
                    PendingDeploy = a.PendingDeploy,
                    MaintenanceNumber = a.MaintenanceNumber,
                    NotReceivedNumber = a.NotReceivedNumber,
                    DamageNumber = b.Where(x => x.InventoryAddId == a.Id).Select(s => s.Id).Count()
                }).GroupJoin(_unitOfWork.SerialNumbers.GetAll(), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Item=a.PrimaryItem+" "+a.SecondaryItem+" "+a.TertiaryItem+" "+a.DetailItem,
                    InventoryAddId = a.InventoryAddId,
                    TotalNumber = a.TotalNumber,
                    HasSerialNo = a.HasSerialNo,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    ReConfigureNumber = a.ReConfigureNumber,
                    DeployNumber = a.DeployNumber,
                    PendingDeploy = a.PendingDeploy,
                    MaintenanceNumber = a.MaintenanceNumber,
                    NotReceivedNumber = a.NotReceivedNumber,
                    DamageNumber=a.DamageNumber,
                    RemainingNumber = a.TotalNumber - b.Select(s => s.Id).Count(),
                    Available = a.TotalNumber  - a.TotalSoldDeploy - a.ConfigureNumber  - a.MaintenanceNumber - a.NotReceivedNumber - a.DamageNumber

                });

                return Ok(totalStock.Where(x=>x.HasSerialNo==true).OrderByDescending(x=>x.BuyDate));

            }

            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("posStock/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<InventoryAddViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult GetStock(int page, int pageSize)
        {
            try
            {
                var inventorys = _unitOfWork.Inventorys.GetAll().Where(x => x.HasSerialNo == true).ToList();
                var InventoryInfo = inventorys.GroupJoin(_unitOfWork.InventorysAdd.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,

                    HasSerialNo = a.HasSerialNo,
                    BuyDate = b.Select(s => s.BuyDate).FirstOrDefault(),
                    TotalNumber = _unitOfWork.InventorysAdd.Find(x => x.InventoryId == a.Id).Sum(s => s.TotalNumber),
                    Remarks = b.Select(s => s.Remarks).FirstOrDefault()

                }).ToList();

                var totalStock = InventoryInfo.GroupJoin(_unitOfWork.PosStocks.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    InventoryAddId = a.InventoryAddId,

                    TotalNumber = a.TotalNumber,
                    SoldNumber = b.Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count(),
                    TotalSoldDeploy = _unitOfWork.PosStocks.GetAll().Where(s => s.InventoryId == a.Id && s.Status == "undeployed").Select(s => s.Id).Count()

                }).GroupJoin(_unitOfWork.Configurations.GetAll().Where(x => x.Verification == "Verified" || x.Verification == "Pending"), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,


                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy=a.TotalSoldDeploy,
                    ConfigureNumber = b.Where(x => x.InventoryId == a.Id && (x.Verification == "Verified" || x.Verification == "Pending")).Select(s => s.Id).Count()

                }).GroupJoin(_unitOfWork.Maintenances.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel

                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,


                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    MaintenanceNumber = b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count(),
                }).GroupJoin(_unitOfWork.NotReceivedPos.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,



                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    MaintenanceNumber = a.MaintenanceNumber,
                    NotReceivedNumber = b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count()


                }).GroupJoin(_unitOfWork.DamagePos.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new InventoryandAddViewModel
                {
                    Id = a.Id,
                    BuyDate = a.BuyDate,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Item=a.PrimaryItem+" "+a.SecondaryItem+" "+a.TertiaryItem+" "+a.DetailItem,


                    TotalNumber = a.TotalNumber,
                    SoldNumber = a.SoldNumber,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    ConfigureNumber = a.ConfigureNumber,
                    MaintenanceNumber = a.MaintenanceNumber,
                    NotReceivedNumber = a.NotReceivedNumber,
                    DamageNumber = b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count(),
                    Available = a.TotalNumber - a.TotalSoldDeploy - a.ConfigureNumber - a.MaintenanceNumber - a.NotReceivedNumber - b.Where(x => x.InventoryId == a.Id).Select(s => s.Id).Count(),
                });
                return Ok(totalStock);

            }

            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {


                var _inventory = _unitOfWork.InventorysAdd.GetGuid(id);
                InventoryAddViewModel _inventoryandAdd = new InventoryAddViewModel()
                {
                    //Id = Guid.NewGuid(),
                    Id=_inventory.Id,
                    PrimaryItem = _unitOfWork.Inventorys.GetAll().Where(x=>x.Id==_inventory.InventoryId).Select(x=>x.PrimaryItem).FirstOrDefault(),
                    SecondaryItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == _inventory.InventoryId).Select(x => x.SecondaryItem).FirstOrDefault(),
                    TertiaryItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == _inventory.InventoryId).Select(x => x.TertiaryItem).FirstOrDefault(),
                    DetailItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == _inventory.InventoryId).Select(x => x.DetailItem).FirstOrDefault(),
                    TotalNumber = _inventory.TotalNumber,
                    BuyDate = _inventory.BuyDate,
                    InventoryId = _inventory.InventoryId,
                    RemainingNumber = _inventory.TotalNumber - _unitOfWork.SerialNumbers.GetAll().Where(x => x.InventoryAddId == id).Select(x => x.Id).Count(),
                    Remarks = _inventory.Remarks,
                };
                return Ok(_inventoryandAdd);
               

            }
            catch
            {
                throw;
            }
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]InventoryAddViewModel _inventoryAddVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            InventoryAdd inventoryAdd = new InventoryAdd()
            {
                Id = Guid.NewGuid(),
                TotalNumber = _inventoryAddVM.TotalNumber,
                BuyDate = _inventoryAddVM.BuyDate,
                InventoryId =_inventoryAddVM.InventoryId,
                PrimaryItem=_unitOfWork.Inventorys.GetAll().Where(x=>x.Id==_inventoryAddVM.InventoryId).Select(x=>x.PrimaryItem).FirstOrDefault(),
                SecondaryItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == _inventoryAddVM.InventoryId).Select(x => x.SecondaryItem).FirstOrDefault(),
                TertiaryItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == _inventoryAddVM.InventoryId).Select(x => x.TertiaryItem).FirstOrDefault(),
                 DetailItem= _unitOfWork.Inventorys.GetAll().Where(x => x.Id == _inventoryAddVM.InventoryId).Select(x => x.DetailItem).FirstOrDefault(),
                 Remarks=_inventoryAddVM.Remarks,
            };
            try
            {
                _unitOfWork.InventorysAdd.Add(inventoryAdd);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody]InventoryAddViewModel _inventoryAddVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            InventoryAdd inventoryAdd = new InventoryAdd();


            inventoryAdd.Id = _inventoryAddVM.Id;
            inventoryAdd.InventoryId = _inventoryAddVM.InventoryId;
            inventoryAdd.PrimaryItem = _unitOfWork.Inventorys.Find(x => x.Id == _inventoryAddVM.InventoryId).Select(s => s.PrimaryItem).FirstOrDefault();
            inventoryAdd.SecondaryItem = _unitOfWork.Inventorys.Find(x => x.Id == _inventoryAddVM.InventoryId).Select(s => s.SecondaryItem).FirstOrDefault();
            inventoryAdd.TertiaryItem = _unitOfWork.Inventorys.Find(x => x.Id == _inventoryAddVM.InventoryId).Select(s => s.TertiaryItem).FirstOrDefault();
            inventoryAdd.DetailItem = _unitOfWork.Inventorys.Find(x => x.Id == _inventoryAddVM.InventoryId).Select(s => s.DetailItem).FirstOrDefault();
            inventoryAdd.TotalNumber = _inventoryAddVM.TotalNumber;
            inventoryAdd.BuyDate = _inventoryAddVM.BuyDate;
        
            try
            {

                _unitOfWork.InventorysAdd.Update(inventoryAdd);
                _unitOfWork.SaveChanges();

            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var _invToDelete = _unitOfWork.InventorysAdd.GetGuid(id);
            try
            {
                _unitOfWork.InventorysAdd.Remove(_invToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
    }
}
