﻿using AutoMapper;
using DAL;
using DAL.Core;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class PosHistoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;
        public PosHistoryController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<PosHistoryViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult Get(int page, int pageSize)
        {
            try
            {
               
                IEnumerable<PosHistoryViewModel> posHistoryQuery = _unitOfWork.DeploymentRequests.GetAll().Join(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new { DeploymentRequest = a, Bank = b })
                                                                .Join(_unitOfWork.Configurations.GetAll(), c => c.DeploymentRequest.Id, d => d.DeploymentRequestId, (c, d) => new { c.DeploymentRequest, c.Bank, Configuration = d })
                                                                .Join(_unitOfWork.ApplicationVersions.GetAll(), e => e.Configuration.ApplicationVersionId, f => f.Id, (e, f) => new { e.DeploymentRequest, e.Bank, e.Configuration, Application = f })
                                                                .Join(_unitOfWork.Deployments.GetAll(), g => g.DeploymentRequest.Id, h => h.DeploymentRequestId, (g, h) => new { g.DeploymentRequest, g.Bank, g.Configuration, g.Application, Deployment = h })
                                                                .Join(_context.Users, x => x.DeploymentRequest.CreatedBy, y => y.Id, (x, y) => new { x.DeploymentRequest, x.Bank, x.Configuration, x.Application, x.Deployment, User = y })
                                                                .Join(_context.Users, k => k.Deployment.CreatedBy, m => m.Id, (k, m) => new { k.DeploymentRequest, k.Bank, k.Configuration, k.Application, k.Deployment, k.User, UserUpdated = m })
                                                                .Join(_unitOfWork.Statuses.GetAll(), p => p.DeploymentRequest.StatusId, q => q.Id, (p, q) => new { p.DeploymentRequest, p.Bank, p.Configuration, p.Application, p.Deployment, p.User, p.UserUpdated, Status = q })
                                                                 .Join(_unitOfWork.Inventorys.GetAll(), p => p.Deployment.InventoryId, q => q.Id, (p, q) => new { p.DeploymentRequest, p.Bank, p.Configuration, p.Application, p.Deployment, p.User, p.UserUpdated,p.Status, Inventory = q })
                                                                   .Join(_unitOfWork.SerialNumbers.GetAll(), p => p.Configuration.SerialNumberId, q => q.Id, (p, q) => new { p.DeploymentRequest, p.Bank, p.Configuration, p.Application, p.Deployment, p.User, p.UserUpdated, p.Status,p.Inventory, SerialNumber = q })
                                                                .Select(s => new PosHistoryViewModel
                                                                {
                                                                    Id = s.DeploymentRequest.Id,
                                                                    DeploymentRequestId = s.DeploymentRequest.Id,
                                                                    BankName = s.Bank.BankName ,
                                                                    BankCode = s.Bank.BankCode,
                                                                    Merchant = s.DeploymentRequest.Merchant,
                                                                    IdMerchant = s.DeploymentRequest.IdMerchant,
                                                                    IdTerminal = s.DeploymentRequest.IdTerminal,
                                                                    Outlet = s.DeploymentRequest.Outlet,
                                                                    Address = s.DeploymentRequest.Address,
                                                                    District = s.DeploymentRequest.District,
                                                                    ContactPerson = s.DeploymentRequest.ContactPerson,
                                                                    ContactNo1 = s.DeploymentRequest.ContactNo1,
                                                                    InventoryId = s.DeploymentRequest.InventoryId,
                                                                    BankId = s.DeploymentRequest.BankId,
                                                                    PrimaryNacNumber = s.DeploymentRequest.PrimaryNacNumber,
                                                                    SecondaryNacNumber = s.DeploymentRequest.SecondaryNacNumber,
                                                                    Remarks = s.DeploymentRequest.Remarks,
                                                                    SerialNumberId=s.Configuration.SerialNumberId,
                                                                    SerialKey = s.Configuration.SerialKey,
                                                                    PreviousSerial = s.Configuration.PreviousSerial,
                                                                    IsSold=s.SerialNumber.IsSold,
                                                                    ApplicationVersionId = s.Configuration.ApplicationVersionId,
                                                                    ConfigStatus = s.Configuration.Status,
                                                                    ApplicationFormat = s.Bank.BankCode + " " + s.Application.ApplicationVer + " " + s.Application.ApplicationDate.Value.ToShortDateString() + " " + s.Application.KernelType,
                                                                    Item = s.Inventory.PrimaryItem + ' ' + s.Inventory.SecondaryItem + ' ' + s.Inventory.TertiaryItem + ' ' + s.Inventory.DetailItem,

                                                                    ReceivedBy =s.Deployment.ReceivedBy,
                                                                    ReceivedContactNo =s.Deployment.Longitude,
                                                                    Connectivity=s.Deployment.Connectivity,
                                                                    RemarksDeploy = s.Deployment.Remarks,
                                                                    MerchantLocation =s.Deployment.MerchantLocation,
                                                                    CreatedDate = s.DeploymentRequest.CreatedDate,
                                                                    DeployDate = s.Deployment.DeployDate,
                                                                    CreatedBy = s.User.FullName,
                                                                    VerifiedBy = s.UserUpdated.FullName,
                                                                    StatusName=s.Status.StatusName,
                                                                    StatusId=s.DeploymentRequest.StatusId

                                                                }).Where(a => a.StatusName == "DeployedVerified").OrderByDescending(x=>x.DeployDate);


                return Ok(posHistoryQuery);

                
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var bankId = _unitOfWork.DeploymentRequests.Find(x => x.Id == id).Select(s => s.BankId).SingleOrDefault();
                var bankName = _unitOfWork.Banks.Find(x => x.Id == bankId).Select(s => s.BankName).FirstOrDefault();
          
                var inventoryId= _unitOfWork.DeploymentRequests.Find(x => x.Id == id).Select(s => s.InventoryId).SingleOrDefault();
                var primaryItem= _unitOfWork.Inventorys.Find(x => x.Id == inventoryId).Select(s => s.PrimaryItem).FirstOrDefault();
                var secondaryItem = _unitOfWork.Inventorys.Find(x => x.Id == inventoryId).Select(s => s.SecondaryItem).FirstOrDefault();
                var tertiaryItem = _unitOfWork.Inventorys.Find(x => x.Id == inventoryId).Select(s => s.TertiaryItem).FirstOrDefault();
                var detailItem = _unitOfWork.Inventorys.Find(x => x.Id == inventoryId).Select(s => s.DetailItem).FirstOrDefault();
                var serialNumber = _unitOfWork.Configurations.Find(x => x.DeploymentRequestId == id).Select(s => s.SerialKey).FirstOrDefault();
                DeploymentRequestViewModel _depReq = new DeploymentRequestViewModel();

                var _deploymentRequest = await _unitOfWork.DeploymentRequests.GetGuidAsync(id);
                var _deployId = _unitOfWork.Deployments.Find(x=>x.DeploymentRequestId==id).Select(s=>s.Id).SingleOrDefault();
                var deploy = _unitOfWork.Deployments.GetGuid(_deployId);
                if (deploy != null)
                {
                    _depReq.BankName = bankName;
                    _depReq.Merchant = _deploymentRequest.Merchant;
                    _depReq.IdMerchant = _deploymentRequest.IdMerchant;
                    _depReq.IdTerminal = _deploymentRequest.IdTerminal;
                    _depReq.ContactPerson = _deploymentRequest.ContactPerson;
                    _depReq.ContactNo1 = _deploymentRequest.ContactNo1;
                    _depReq.Address = _deploymentRequest.Address;
                    _depReq.MerchantLocation = deploy.MerchantLocation;
                    _depReq.ReceivedBy = deploy.ReceivedBy;
                    _depReq.ReceivedContactNo = deploy.Longitude;
                    _depReq.Item = primaryItem + " " + secondaryItem + " " + tertiaryItem + " " + detailItem;
                    _depReq.SerialNumber = serialNumber;
                    _depReq.Connectivity = deploy.Connectivity;
                }
                else
                {
                    _depReq.BankName = bankName;
                    _depReq.Merchant = _deploymentRequest.Merchant;
                    _depReq.IdMerchant = _deploymentRequest.IdMerchant;
                    _depReq.IdTerminal = _deploymentRequest.IdTerminal;
                    _depReq.ContactPerson = _deploymentRequest.ContactPerson;
                    _depReq.ContactNo1 = _deploymentRequest.ContactNo1;
                    _depReq.Address = _deploymentRequest.Address;
                    
                }
                return Ok(_depReq);
               
            }
            catch
            {
                throw;
            }
        }


        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]PosHistoryViewModel _posHistoryVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            PosHistory _posHistory = new PosHistory();
            _posHistory = Mapper.Map<PosHistory>(_posHistoryVM);

            _posHistory.DeploymentRequestId = _posHistoryVM.Id;
            _posHistory.Id = Guid.NewGuid();
            _posHistory.RequestName = "Reconfigure";
        

            var request = _unitOfWork.DeploymentRequests.GetGuid(_posHistory.DeploymentRequestId);
          
            request.Verification = null;

            request.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "Reconfigure").Select(a => a.Id).FirstOrDefault();
            request.StatusId = _unitOfWork.Statuses.Find(a => a.StatusName == "Requested").Select(a => a.Id).FirstOrDefault();
            var config = _unitOfWork.Configurations.Find(a => a.DeploymentRequestId == _posHistory.DeploymentRequestId).FirstOrDefault();
            config.Verification = "Pending";

            var deploy = _unitOfWork.Deployments.Find(a => a.DeploymentRequestId == _posHistory.DeploymentRequestId).FirstOrDefault();
            deploy.Verification = "Pending";
            _posHistory.ApplicationDateVersion = _posHistoryVM.ApplicationFormat;
            _posHistory.TerminalType = _posHistoryVM.Item;
            _posHistory.ContactNo = _posHistoryVM.ContactNo1;
            _posHistory.IsReceived = "NOT IN USE";
            _posHistory.Scanned = "Deployment Request";
            _posHistory.Longitude = _posHistoryVM.ReceivedContactNo;
            _posHistory.Address = _posHistoryVM.Address;
            _posHistory.Remarks = "Reconfigured";      //_posHistoryVM.RemarksDeploy;
            _posHistory.DeployDate = _posHistoryVM.DeployDate;
         

            try
            {

                _unitOfWork.PosHistorys.Add(_posHistory);
              
                _unitOfWork.DeploymentRequests.Update(request);
                _unitOfWork.Configurations.Update(config);
                _unitOfWork.Deployments.Update(deploy);
                //_unitOfWork.Reconfigures.Add(reconfig);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ItemAlreadyExists(_posHistory.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);

        }

        [HttpPut("retrieve/{id}")]
        public IActionResult Post(string retrieve,Guid id, [FromBody]PosHistoryViewModel _posHistoryVM)
        {

            PosHistory _posHistory = new PosHistory();
            _posHistory = Mapper.Map<PosHistory>(_posHistoryVM);

            _posHistory.DeploymentRequestId = _posHistoryVM.Id;
            _posHistory.Id = Guid.NewGuid();
            _posHistory.RequestName = "Retrieve";                       
            _posHistory.ApplicationDateVersion = _posHistoryVM.ApplicationFormat;
            _posHistory.TerminalType = _posHistoryVM.Item;
            _posHistory.ContactNo = _posHistoryVM.ContactNo1;
            _posHistory.IsReceived = "---NOT IN USE---";
            _posHistory.Scanned = "Deployment Request---";
            _posHistory.Longitude = _posHistoryVM.ReceivedContactNo;
            _posHistory.Address = _posHistoryVM.Address;
            _posHistory.Remarks = "Retrieved";      // _posHistoryVM.RemarksDeploy;
            _posHistory.DeployDate = _posHistoryVM.DeployDate;



            var request = _unitOfWork.DeploymentRequests.GetGuid(id);
            request.StatusId = _unitOfWork.Statuses.Find(a => a.StatusName == "Retrieve").Select(a => a.Id).FirstOrDefault();
            request.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "Retrieve").Select(a => a.Id).FirstOrDefault();
            var config = _unitOfWork.Configurations.Find(a => a.DeploymentRequestId == id).FirstOrDefault();
            config.Verification = null;
            var deploy = _unitOfWork.Deployments.Find(a => a.DeploymentRequestId == id).FirstOrDefault();
            deploy.Verification = null;
            try
            {
                _unitOfWork.PosHistorys.Add(_posHistory);
                _unitOfWork.DeploymentRequests.Update(request);
                _unitOfWork.Configurations.Update(config);
                _unitOfWork.Deployments.Update(deploy);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                
                    throw;
               
            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        [HttpPut("maintenancePos/{id}")]
        public IActionResult PutMaintenance(string maintenancePos, Guid id)
        {

            Maintenance _maintenance = new Maintenance();
            var depReq = _unitOfWork.DeploymentRequests.GetGuid(id);
            depReq.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "Delete").Select(a => a.Id).FirstOrDefault();
            depReq.StatusId = _unitOfWork.Statuses.Find(a => a.StatusName == "RetrieveVerified").Select(a => a.Id).FirstOrDefault();
            var configId = _unitOfWork.Configurations.Find(x => x.DeploymentRequestId == id).Select(s => s.Id).SingleOrDefault();
            var config = _unitOfWork.Configurations.GetGuid(configId);
            var appId = _unitOfWork.ApplicationVersions.Find(x => x.Id == config.ApplicationVersionId).Select(s => s.Id).SingleOrDefault();
            var inventoryId = _unitOfWork.Inventorys.Find(x => x.Id == depReq.InventoryId).Select(s => s.Id).SingleOrDefault();
            var serialNumberId = _unitOfWork.SerialNumbers.Find(x => x.Id == config.SerialNumberId).Select(s => s.Id).SingleOrDefault();
            var serialNumber = _unitOfWork.SerialNumbers.Find(x => x.Id == serialNumberId).Select(s => s.SerialKey).FirstOrDefault();
            _maintenance.Id = Guid.NewGuid();
            _maintenance.DeploymentRequestId = id;
            _maintenance.InventoryId = inventoryId;
            _maintenance.SerialNumberId = serialNumberId;
            _maintenance.SerialNumber = serialNumber;
            _maintenance.ApplicationVersionId = appId;
            _maintenance.Merchant = depReq.Merchant;
            _maintenance.IdMerchant = depReq.IdMerchant;
            _maintenance.IdTerminal = depReq.IdTerminal;
            _maintenance.Outlet = depReq.Outlet;
            _maintenance.District = depReq.District;
            _maintenance.Address = depReq.Address;
            _maintenance.ContactPerson = depReq.ContactPerson;
            _maintenance.ContactNo = depReq.ContactNo1;
            _maintenance.Status = "Under Repair";

            try
            {
                _unitOfWork.Maintenances.Add(_maintenance);
                _unitOfWork.DeploymentRequests.Update(depReq);
                _unitOfWork.Configurations.Remove(config);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }


        [HttpPost("damagePos/{id}")]
        public IActionResult DamagePos([FromBody]DamagePoseViewModel _damagePosVM, Guid id)
        {
            var _posHistoryId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == _damagePosVM.SerialNumberId && x.IsReceived == "IN MAINTENANCE" && x.Scanned=="NOT IN USE").Select(s => s.Id).SingleOrDefault();
            var posHistory = _unitOfWork.PosHistorys.GetGuid(_posHistoryId);
            DamagePos _damage = new DamagePos();

            var maintenanceId = _unitOfWork.Maintenances.Find(x => x.DeploymentRequestId == id).Select(s => s.Id).SingleOrDefault();
            var maintenance = _unitOfWork.Maintenances.GetGuid(maintenanceId);
            var depReq = _unitOfWork.DeploymentRequests.GetGuid(id);
            _damage.Id = Guid.NewGuid();
            _damage.DeploymentRequestId = id;
            _damage.InventoryId = _damagePosVM.InventoryId;
            _damage.InventoryAddId = _damagePosVM.InventoryAddId;
            _damage.SerialNumberId = _damagePosVM.SerialNumberId;
            _damage.ApplicationVersionId = _damagePosVM.ApplicationVersionId;
            _damage.Status = _damagePosVM.Status;
            _damage.Merchant = _damagePosVM.Merchant;
            _damage.IdMerchant = _damagePosVM.IdMerchant;
            _damage.IdTerminal = _damagePosVM.IdTerminal;
            _damage.Outlet = _damagePosVM.Outlet;
            _damage.District = _damagePosVM.District;
            _damage.Address = _damagePosVM.Address;
            _damage.ContactPerson = _damagePosVM.ContactPerson;
            _damage.ContactNo = _damagePosVM.ContactNo;


            try
            {
                if (posHistory != null)
                {
                    posHistory.IsReceived = "Damage";
                    posHistory.Scanned = _damagePosVM.Status;
                    _unitOfWork.PosHistorys.Update(posHistory);
                    _unitOfWork.DamagePos.Add(_damage);
                    _unitOfWork.Maintenances.Remove(maintenance);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    _unitOfWork.DamagePos.Add(_damage);
                    _unitOfWork.Maintenances.Remove(maintenance);
                    _unitOfWork.SaveChanges();
                }

            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

      
        [HttpPut("retrieveVerify/{id}")]
        public IActionResult Put(string retrieveVerify, Guid id)
        {

            var request = _unitOfWork.DeploymentRequests.GetGuid(id);
            request.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "Retrieve").Select(a => a.Id).FirstOrDefault();
            request.StatusId = _unitOfWork.Statuses.Find(a => a.StatusName == "RetrieveVerified").Select(a => a.Id).FirstOrDefault();
            var config = _unitOfWork.Configurations.GetAll().Where(x => x.DeploymentRequestId == request.Id).SingleOrDefault();
            try
            {
                _unitOfWork.DeploymentRequests.Update(request);
                _unitOfWork.Configurations.Remove(config);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        [HttpPut("undo/{id}")]
        public IActionResult Put( Guid id)
        {

            var request = _unitOfWork.DeploymentRequests.GetGuid(id);
            request.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "New").Select(a => a.Id).FirstOrDefault();
            request.StatusId = _unitOfWork.Statuses.Find(a => a.StatusName == "DeployedVerified").Select(a => a.Id).FirstOrDefault();
            var config = _unitOfWork.Configurations.GetAll().Where(x => x.DeploymentRequestId == request.Id).SingleOrDefault();
            try
            {
                _unitOfWork.DeploymentRequests.Update(request);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody]PosHistoryViewModel _posHistoryVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            //var user = await _userManager.GetUserAsync(User);
            PosHistory _posHistory = new PosHistory();

            _posHistory = Mapper.Map<PosHistory>(_posHistoryVM);
           
            _posHistory.DeploymentRequestId = _posHistoryVM.Id;
            _posHistory.Id = Guid.NewGuid();

            _posHistory.IdMerchant = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).IdMerchant;
            _posHistory.Merchant = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).Merchant;
            _posHistory.IdTerminal = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).IdTerminal;
            _posHistory.Outlet = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).Outlet;
            _posHistory.District = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).District;
            _posHistory.Address = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).Address;
            _posHistory.ContactPerson = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).ContactPerson;
            _posHistory.ContactNo = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).ContactNo1;
            _posHistory.InventoryId = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).InventoryId;
            _posHistory.PrimaryNacNumber = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).PrimaryNacNumber;
            _posHistory.SecondaryNacNumber = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).SecondaryNacNumber;
            _posHistory.Remarks = _unitOfWork.DeploymentRequests.GetGuid(_posHistoryVM.Id).Remarks;
            _posHistory.TerminalType = _posHistoryVM.Item;
            _posHistory.ApplicationVersionId = _posHistoryVM.ApplicationVersionId;
            _posHistory.BankId = _posHistoryVM.BankId;
            _posHistory.InventoryId = _posHistoryVM.InventoryId;
            _posHistory.SerialNumberId = _posHistoryVM.SerialNumberId;

            var bankCode = _unitOfWork.Banks.Find(x => x.Id == _posHistoryVM.BankId).Select(s => s.BankCode).FirstOrDefault();
            var appVersion = _unitOfWork.ApplicationVersions.Find(x => x.Id == _posHistoryVM.ApplicationVersionId).Select(s => s.ApplicationVer).FirstOrDefault();
            var appDate = _unitOfWork.ApplicationVersions.Find(x => x.Id == _posHistoryVM.ApplicationVersionId).Select(s => s.ApplicationDate).FirstOrDefault();
            var kernelType = _unitOfWork.ApplicationVersions.Find(x => x.Id == _posHistoryVM.ApplicationVersionId).Select(s => s.KernelType).FirstOrDefault();

            _posHistory.ApplicationDateVersion = bankCode+" "+appVersion+" "+appDate+" "+kernelType;
            _posHistory.IsReceived = _posHistoryVM.PreviousSerial;
            _posHistory.Scanned = _posHistoryVM.ConfigStatus;
            _posHistory.Connectivity = _posHistoryVM.Connectivity;
            _posHistory.ReceivedBy = _posHistoryVM.ReceivedBy;
            _posHistory.Longitude = _posHistoryVM.ReceivedContactNo;
            _posHistory.MerchantLocation = _posHistoryVM.MerchantLocation;
            _posHistory.DeployDate = _posHistoryVM.DeployDate;



            _posHistory = _unitOfWork.PosHistorys.GetAll().Where(x => x.RequestName == "Reconfigure").FirstOrDefault();

          

            var request = _unitOfWork.DeploymentRequests.GetGuid(_posHistory.DeploymentRequestId);
            request.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "Retrieve").Select(a => a.Id).FirstOrDefault();
            request.StatusId = _unitOfWork.Statuses.Find(a => a.StatusName == "RetrieveVerified").Select(a => a.Id).FirstOrDefault();
            //var deploy = _unitOfWork.Deployments.Find(a => a.DeploymentRequestId == _posHistory.DeploymentRequestId).FirstOrDefault();
            //_posHistory.CreatedBy = _context.Users.Where(x=>x.Id== _posHistoryVM.CreatedBy).Select(s=>s.FullName).FirstOrDefault();
            Checker checker = new Checker()
            {
                Id = Guid.NewGuid(),
                //Name = _accountManager.get.
                StatusId = request.StatusId,
                DeploymentRequestId = request.RequestTypeId,
            };
            try
            {

                _unitOfWork.PosHistorys.Add(_posHistory);
                _unitOfWork.DeploymentRequests.Update(request);
                _unitOfWork.Checkers.Add(checker);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ItemAlreadyExists(_posHistory.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }
        private bool ItemAlreadyExists(Guid Id)
        {
            int count = _unitOfWork.Deployments.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
