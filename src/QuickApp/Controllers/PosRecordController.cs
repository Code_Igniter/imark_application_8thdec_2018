﻿using AutoMapper;
using DAL;
using DAL.Core.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuickApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class PosRecordController: Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;

        public PosRecordController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<PosHistoryViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult Get(int page, int pageSize)
        {

            IEnumerable<PosHistoryViewModel> posOldRecordQuery = _unitOfWork.PosHistorys.GetAll().Join(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new { PosHistory = a, Bank = b })
                                                               .Join(_unitOfWork.Inventorys.GetAll(), x => x.PosHistory.InventoryId, y => y.Id, (x, y) => new { x.PosHistory, x.Bank, Inventory = y })
                                                              .Join(_context.Users, x => x.PosHistory.CreatedBy, y => y.Id, (x, y) => new { x.PosHistory, x.Bank, x.Inventory, User = y })
                                                              .Join(_unitOfWork.SerialNumbers.GetAll(), x => x.PosHistory.SerialNumberId, y => y.Id, (x, y) => new { x.PosHistory, x.Bank,x.Inventory,x.User, SerialNumber = y })

                                                                .Select(s => new PosHistoryViewModel
                                                                {
                                                                    Id = s.PosHistory.Id,
                                                                    DeploymentRequestId = s.PosHistory.DeploymentRequestId,
                                                                    BankName = s.PosHistory.BankName,
                                                                    Merchant = s.PosHistory.Merchant,
                                                                    IdMerchant = s.PosHistory.IdMerchant,
                                                                    IdTerminal = s.PosHistory.IdTerminal,
                                                                    Outlet = s.PosHistory.Outlet,
                                                                    Address = s.PosHistory.Address,
                                                                    District = s.PosHistory.District,
                                                                    ContactPerson = s.PosHistory.ContactPerson,
                                                                    ContactNo = s.PosHistory.ContactNo,
                                                                    InventoryId = s.PosHistory.InventoryId,
                                                                    BankId = s.PosHistory.BankId,
                                                                    PrimaryNacNumber = s.PosHistory.PrimaryNacNumber,
                                                                    SecondaryNacNumber = s.PosHistory.SecondaryNacNumber,                                                                    
                                                                    SerialNumberId = s.PosHistory.SerialNumberId,
                                                                    SerialKey = s.PosHistory.SerialKey,                                                                    
                                                                    ApplicationVersionId = s.PosHistory.ApplicationVersionId,                                                                    
                                                                    Longitude=s.PosHistory.Longitude,
                                                                    ApplicationDateVersion = s.PosHistory.ApplicationDateVersion,
                                                                    Item=s.Inventory.PrimaryItem+" "+s.Inventory.SecondaryItem+" "+s.Inventory.TertiaryItem+" "+s.Inventory.DetailItem,
                                                                    TerminalType=s.PosHistory.TerminalType,
                                                                    IsSold=s.SerialNumber.IsSold,
                                                                    ReceivedBy = s.PosHistory.ReceivedBy,
                                                                    ReceivedContactNo = s.PosHistory.Longitude,
                                                                    Connectivity = s.PosHistory.Connectivity,
                                                                    MerchantLocation = s.PosHistory.MerchantLocation,

                                                                    IsReceived = s.PosHistory.IsReceived, //serialNo status
                                                                    Scanned = s.PosHistory.Scanned, //Status
                                                                    Remarks = s.PosHistory.Remarks,                                                                  

                                                                    CreatedDate = s.PosHistory.CreatedDate,
                                                                    DeployDate = s.PosHistory.DeployDate,
                                                                    CreatedBy = s.User.FullName


                                                                }).OrderByDescending(x=>x.CreatedDate);


            return Ok(posOldRecordQuery);
        }


        [HttpGet("individual/{page:int}/{pageSize:int}/{deploymentRequestId}")]
        [Produces(typeof(List<PosHistoryViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult Get(int page, int pageSize, Guid deploymentRequestId)
        {
            
            try
            {
               
                IEnumerable<PosHistoryViewModel> posIndividualRecordQuery = _unitOfWork.PosHistorys.GetAll().Join(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new { PosHistory = a, DeploymentRequest = b })
                                                             .Join(_unitOfWork.Inventorys.GetAll(), x => x.PosHistory.InventoryId, y => y.Id, (x, y) => new { x.PosHistory, x.DeploymentRequest, Inventory = y })
                                                             .Join(_context.Users, x => x.PosHistory.CreatedBy, y => y.Id, (x, y) => new { x.PosHistory, x.DeploymentRequest, x.Inventory, User = y })
                                                              .Join(_unitOfWork.SerialNumbers.GetAll(), x => x.PosHistory.SerialNumberId, y => y.Id, (x, y) => new { x.PosHistory, x.DeploymentRequest, x.Inventory, x.User, SerialNumber = y })
                                                               .Select(s => new PosHistoryViewModel
                                                               {
                                                                   Id = s.PosHistory.Id,
                                                                   DeploymentRequestId = s.PosHistory.DeploymentRequestId,
                                                                   BankName = s.PosHistory.BankName,
                                                                   Merchant = s.PosHistory.Merchant,
                                                                   IdMerchant = s.PosHistory.IdMerchant,
                                                                   IdTerminal = s.PosHistory.IdTerminal,
                                                                   Outlet = s.PosHistory.Outlet,
                                                                   Address = s.PosHistory.Address,
                                                                   District = s.PosHistory.District,
                                                                   ContactPerson = s.PosHistory.ContactPerson,
                                                                   ContactNo = s.PosHistory.ContactNo,
                                                                   InventoryId = s.PosHistory.InventoryId,
                                                                   BankId = s.PosHistory.BankId,
                                                                   PrimaryNacNumber = s.PosHistory.PrimaryNacNumber,
                                                                   SecondaryNacNumber = s.PosHistory.SecondaryNacNumber,
                                                                   Remarks = s.PosHistory.Remarks,
                                                                   SerialNumberId = s.PosHistory.SerialNumberId,
                                                                   SerialKey = s.PosHistory.SerialKey,
                                                                   IsReceived = s.PosHistory.IsReceived,
                                                                   ApplicationVersionId = s.PosHistory.ApplicationVersionId,
                                                                   Scanned = s.PosHistory.Scanned,
                                                                   Longitude = s.PosHistory.Longitude,
                                                                   IsSold = s.SerialNumber.IsSold,
                                                                   ApplicationDateVersion = s.PosHistory.ApplicationDateVersion,
                                                                   Item = s.Inventory.PrimaryItem + " " + s.Inventory.SecondaryItem + " " + s.Inventory.TertiaryItem + " " + s.Inventory.DetailItem,
                                                                   TerminalType = s.PosHistory.TerminalType,
                                                                   ReceivedBy = s.PosHistory.ReceivedBy,
                                                                   ReceivedContactNo = s.PosHistory.Longitude,
                                                                   Connectivity = s.PosHistory.Connectivity,
                                                                   MerchantLocation = s.PosHistory.MerchantLocation,
                                                                   CreatedDate = s.PosHistory.CreatedDate,
                                                                   DeployDate = s.PosHistory.DeployDate,
                                                                   CreatedBy = s.User.FullName


                                                               }).Where(x => x.DeploymentRequestId == deploymentRequestId).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(posIndividualRecordQuery);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }


         [HttpGet("posSpares/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<PosHistoryViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public  IActionResult GetSpares(int page, int pageSize)
        {

            IQueryable<PosHistoryViewModel> posHistoryQuery = _unitOfWork.DeploymentRequests.GetAll().Join(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new { DeploymentRequest = a, Bank = b })
                                                                .Join(_unitOfWork.Configurations.GetAll(), c => c.DeploymentRequest.Id, d => d.DeploymentRequestId, (c, d) => new { c.DeploymentRequest, c.Bank, Configuration = d })
                                                                 .Join(_unitOfWork.Inventorys.GetAll(), c => c.DeploymentRequest.InventoryId, d => d.Id, (c, d) => new { c.DeploymentRequest, c.Bank,c.Configuration,   Inventory = d })
                                                                .Select(s => new PosHistoryViewModel
                                                                {
                                                                    Id = s.DeploymentRequest.Id,
                                                                    DeploymentRequestId = s.DeploymentRequest.Id,
                                                                    BankName = s.Bank.BankName,
                                                                    Merchant = s.DeploymentRequest.Merchant,
                                                                    IdMerchant = s.DeploymentRequest.IdMerchant,
                                                                    IdTerminal = s.DeploymentRequest.IdTerminal,
                                                                    Outlet = s.DeploymentRequest.Outlet,
                                                                    Address = s.DeploymentRequest.Address,
                                                                    District = s.DeploymentRequest.District,
                                                                    ContactPerson = s.DeploymentRequest.ContactPerson,
                                                                    ContactNo1 = s.DeploymentRequest.ContactNo1,
                                                                    SerialKey = s.Configuration.SerialKey,
                                                                    PrimaryItem=s.Inventory.PrimaryItem,
                                                                    SecondaryItem=s.Inventory.SecondaryItem,
                                                                    TertiaryItem=s.Inventory.TertiaryItem,
                                                                    DetailItem=s.Inventory.DetailItem
                                                                    

                                                                });



            return Ok(posHistoryQuery);
        }
    }
}
