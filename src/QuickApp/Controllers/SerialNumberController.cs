﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class SerialNumberController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public SerialNumberController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpGet("{id}/{config}")]
        [Produces(typeof(List<SerialNumberViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(Guid id, string config)
        {
            try
            {

                var serialNosVM = new List<SerialNumberViewModel>();

               
                var bankDepStatus = _unitOfWork.DeploymentRequests.Find(x => x.Id == id).Select(s => s.BankId).SingleOrDefault();
                var bankStatus = _unitOfWork.Banks.Find(x=>x.Id== bankDepStatus).Select(s => s.Status).FirstOrDefault();

                var depInventoryAdd = _unitOfWork.DeploymentRequests.Find(x => x.Id == id).Select(s => s.InventoryId).SingleOrDefault();
                var depRentalInventoryAdd = _unitOfWork.DeploymentRequests.Find(x => x.Id == id).Select(s=>s.RentalInventoryAdd).FirstOrDefault();

                if (config == "config"  && (depRentalInventoryAdd == "RentalInventory" || depRentalInventoryAdd == "Rental"))
                {
                     List<SerialNumberViewModel> _serial = new List<SerialNumberViewModel>();
                    RequestType _request = new RequestType();


                    var serial = _unitOfWork.DeploymentRequests.GetAll().Where(a => a.Id == id).Join(_unitOfWork.InventorysAdd.GetAll(), a => a.InventoryId, b => b.InventoryId, (a, b) => new { DeploymentRequest = a, InventoryAdd = b }).
                        Join(_unitOfWork.SerialNumbers.GetAll(), di => di.InventoryAdd.Id, sn => sn.InventoryAddId, (di, sn) => new { di.DeploymentRequest, di.InventoryAdd, serialNum = sn.SerialKey, serialId = sn.Id, inventoryAddId=sn.InventoryAddId, isSold=sn.IsSold })
                       
                        .Select(c => new SerialNumberViewModel
                        {
                            Id = c.serialId,
                            SerialKey = c.serialNum,
                            InventoryAddId=c.inventoryAddId,
                            IsSold=c.isSold
                        }).Where(x=>x.IsSold==false).ToList();

                    
                    var result = _unitOfWork.Configurations.GetAll().Where(x => x.Verification == "Pending" || x.Verification=="Verified" || x.Verification==null).Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                    {
                        Id = a.SerialNumberId,

                        SerialKey = a.SerialKey
                    }).ToList();
                    var maintenance = _unitOfWork.Maintenances.GetAll().Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                    {
                        Id = a.SerialNumberId,
                        SerialKey=a.SerialNumber
                       
                    }).ToList();
                    var replacedPos = _unitOfWork.NotReceivedPos.GetAll().Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                    {
                        Id = a.SerialNumberId,
                        SerialKey = _unitOfWork.SerialNumbers.GetAll().Where(x=>x.Id==a.SerialNumberId).Select(s=>s.SerialKey).FirstOrDefault()

                    }).ToList();
                    var damagePos = _unitOfWork.DamagePos.GetAll().Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                    {
                        Id = a.SerialNumberId,
                        SerialKey = _unitOfWork.SerialNumbers.GetAll().Where(x => x.Id == a.SerialNumberId).Select(s => s.SerialKey).FirstOrDefault()

                    }).ToList();


                    //IEnumerable<SerialNumberViewModel> _remaining = result.Aggregate(serial.ToList(), (e, l) => { e.Remove(l); return e; }).ToList();

                    List<SerialNumberViewModel> _remaining = serial.Where(x => !result.Any(y => y.Id==x.Id && y.SerialKey == x.SerialKey) && !maintenance.Any(z => z.Id == x.Id && z.SerialKey == x.SerialKey) && !replacedPos.Any(z => z.Id == x.Id && z.SerialKey == x.SerialKey) && !damagePos.Any(z => z.Id == x.Id && z.SerialKey == x.SerialKey)).ToList();
                   
                    List<SerialNumberViewModel> stocksVM = new List<SerialNumberViewModel>();

                    foreach (SerialNumberViewModel item in _remaining)
                    {
                     
                        var serialVM = Mapper.Map<SerialNumberViewModel>(item);
                        stocksVM.Add(serialVM);
                    }

                    return Ok(stocksVM);
                 

                }

              

                else 
                {

                    var soldSerial = _unitOfWork.PosStocks.GetAll().Where(x=>x.InventoryId== depInventoryAdd && x.BankId== bankDepStatus).Join(_unitOfWork.SerialNumbers.GetAll(), a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                    {
                        Id = a.SerialNumberId,
                        SerialNumberId=a.SerialNumberId,
                        SerialKey = a.SerialNumber,
                        Status = a.Status,
                        BankName=a.BankName

                    }).Where(x => x.Status == "UnDeployed");

                    var result = _unitOfWork.Configurations.GetAll().Where(x => x.Verification == "Pending" || x.Verification == "Verified").Join(soldSerial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                    {
                        Id = a.SerialNumberId,

                        SerialKey = a.SerialKey
                    }).ToList();

                    List<SerialNumberViewModel> _remaining = soldSerial.Where(x => !result.Any(y => y.Id == x.Id && y.SerialKey == x.SerialKey)).ToList();

                    List<SerialNumberViewModel> stocksVM = new List<SerialNumberViewModel>();

                    foreach (SerialNumberViewModel item in _remaining)
                    {

                        var serialVM = Mapper.Map<SerialNumberViewModel>(item);
                        stocksVM.Add(serialVM);
                    }

                    return Ok(stocksVM);
                }
                
            }
            catch(Exception ex)
            {
                return Ok(ex);
            }
           
        }



        [HttpGet("posStock/{id}")]
        [Produces(typeof(List<SerialNumberViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(Guid id)
        {
            try
            {


                var serial = _unitOfWork.Inventorys.GetAll().Where(a => a.Id == id).Join(_unitOfWork.InventorysAdd.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new { Inventory = a, InventoryAdd = b }).
                    Join(_unitOfWork.SerialNumbers.GetAll(), di => di.InventoryAdd.Id, sn => sn.InventoryAddId, (di, sn) => new { di.Inventory, di.InventoryAdd, serialNum = sn.SerialKey, serialId = sn.Id, inventoryAddId = sn.InventoryAddId, isSold = sn.IsSold })

                    .Select(c => new SerialNumberViewModel
                    {
                        Id = c.serialId,
                        SerialKey = c.serialNum,
                        InventoryAddId = c.inventoryAddId,
                        IsSold = c.isSold
                    }).Where(x => x.IsSold == false).ToList();

                //var serial = _unitOfWork.SerialNumbers.GetAll().Where(x => x.IsSold == false).GroupJoin(_unitOfWork.InventorysAdd.GetAll().Where(x => x.Id == id), a => a.InventoryAddId, b => b.Id, (a, b) => new SerialNumberViewModel
                //{
                //    Id = a.Id,
                //    SerialKey = a.SerialKey,
                //    InventoryAddId = id,
                //});

                var result = _unitOfWork.Configurations.GetAll().Where(x => x.Verification == "Pending" || x.Verification == "Verified")
                    .Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                {
                    Id = a.SerialNumberId,

                    SerialKey = a.SerialKey
                }).ToList();

                var maintenance = _unitOfWork.Maintenances.GetAll().Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                {
                    Id = a.SerialNumberId,
                    SerialKey = a.SerialNumber

                }).ToList();

                var replacedPos = _unitOfWork.NotReceivedPos.GetAll().Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                {
                    Id = a.SerialNumberId,
                    SerialKey = _unitOfWork.SerialNumbers.GetAll().Where(x => x.Id == a.SerialNumberId).Select(s => s.SerialKey).FirstOrDefault()

                }).ToList();

                var damagePos = _unitOfWork.DamagePos.GetAll().Join(serial, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                {
                    Id = a.SerialNumberId,
                    SerialKey = _unitOfWork.SerialNumbers.GetAll().Where(x => x.Id == a.SerialNumberId).Select(s => s.SerialKey).FirstOrDefault()

                }).ToList();


                //IEnumerable<SerialNumberViewModel> _remaining = result.Aggregate(serial.ToList(), (e, l) => { e.Remove(l); return e; }).ToList();


                ///CheckIt OUt.....
                List<SerialNumberViewModel> _remaining = serial.Where(x => !result.Any(y => y.Id == x.Id && y.SerialKey == x.SerialKey) 
                && !maintenance.Any(z => z.Id == x.Id && z.SerialKey == x.SerialKey) && !replacedPos.Any(z => z.Id == x.Id && z.SerialKey == x.SerialKey) 
                && !damagePos.Any(z => z.Id == x.Id && z.SerialKey == x.SerialKey)).ToList();

                List<SerialNumberViewModel> stocksVM = new List<SerialNumberViewModel>();

                    foreach (SerialNumberViewModel item in _remaining)
                    {

                        var serialVM = Mapper.Map<SerialNumberViewModel>(item);
                        stocksVM.Add(serialVM);
                    }

                    return Ok(stocksVM);

            }
            catch (Exception ex)
            {
                return Ok(ex);
            }

        }


        /// <summary>
        /// comment-------------
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces(typeof(List<SerialNumberViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult GetSerialStock(Guid id)
        {
            var serialNosVM = new List<SerialNumberViewModel>();
            try
            {


                var stockSerialNo = _unitOfWork.SerialNumbers.GetAll().Where(x => x.InventoryAddId == id && x.IsSold == true).Select(s => new SerialNumberViewModel
                {
                    Id=s.Id,
                    SerialKey=s.SerialKey
                }).ToList();
                var result = _unitOfWork.PosStocks.GetAll().Where(x => x.InventoryId == id).Join(stockSerialNo, a => a.SerialNumberId, b => b.Id, (a, b) => new SerialNumberViewModel
                {
                    Id = a.SerialNumberId,

                    SerialKey = a.SerialNumber
                }).ToList();

                List<SerialNumberViewModel> _remaining = stockSerialNo.Where(x => !result.Any(y => y.Id == x.Id && y.SerialKey == x.SerialKey)).ToList();

                List<SerialNumberViewModel> stockVM = new List<SerialNumberViewModel>();

                foreach (SerialNumberViewModel item in _remaining)
                {

                    var serialVM = Mapper.Map<SerialNumberViewModel>(item);
                    stockVM.Add(serialVM);
                }

                return Ok(stockVM);


            }
            catch (Exception ex)
            {
                return Ok(ex);
            }

        }

        // GET: api/<controller>
        [HttpGet("pagination/{page:int}/{pageSize:int}/{id}")]
        [Produces(typeof(List<SerialNumberViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get( int page, int pageSize, Guid id)
        {


            IQueryable<SerialNumber> serialNo = _unitOfWork.SerialNumbers.GetAll().Where(a => a.InventoryAddId == id);

            //if (page != -1)
            //    serialNo = serialNo.Skip((page - 1) * pageSize);

            //if (pageSize != -1)
            //    serialNo = serialNo.Take(pageSize);

            //var serialNoList = serialNo.ToList();

            //List<SerialNumberViewModel> serialNosVM = new List<SerialNumberViewModel>();

            //foreach (var item in serialNoList)
            //{
            //    var serialNoVM = Mapper.Map<SerialNumberViewModel>(item);


            //    serialNosVM.Add(serialNoVM);
            //}

            return Ok(serialNo);
        }

        // GET api/<controller>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]SerialNumberViewModel _serialNumberVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int duplicate = _unitOfWork.SerialNumbers.GetAll().Where(a => a.SerialKey == _serialNumberVM.SerialKey).Count();
            int countSerNo = _unitOfWork.SerialNumbers.GetAll().Where(a => a.InventoryAddId == _serialNumberVM.InventoryAddId).Count();
            InventoryAdd totalSerial = _unitOfWork.InventorysAdd.GetGuid(_serialNumberVM.InventoryAddId);
            if (duplicate > 0 || countSerNo >=totalSerial.TotalNumber )
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {
                SerialNumber _serialnumber = new SerialNumber()
                {
                    Id = Guid.NewGuid(),
                    SerialKey = _serialNumberVM.SerialKey,
                    InventoryAddId = _serialNumberVM.InventoryAddId,
                };
                try
                {
                    _unitOfWork.SerialNumbers.Add(_serialnumber);
                    _unitOfWork.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    throw;
                }
                return new StatusCodeResult(StatusCodes.Status201Created);
            }
            
        }
        [HttpPut("{id}")]
        public IActionResult Put([FromBody]SerialNumberViewModel _serialVM)
        {
            int duplicate = _unitOfWork.SerialNumbers.GetAll().Where(a => a.SerialKey == _serialVM.SerialKey).Count();
            int countSerNo = _unitOfWork.SerialNumbers.GetAll().Where(a => a.InventoryAddId == _serialVM.InventoryAddId).Count();
            InventoryAdd totalSerial = _unitOfWork.InventorysAdd.GetGuid(_serialVM.InventoryAddId);
            if (duplicate > 0 )
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {

                SerialNumber serial = new SerialNumber()
                {
                    Id = _serialVM.Id,
                    SerialKey = _serialVM.SerialKey,
                    InventoryAddId = _serialVM.InventoryAddId,
                    IsSold = _serialVM.IsSold


                };
                //try
                //{

                    _unitOfWork.SerialNumbers.Update(serial);
                    _unitOfWork.SaveChanges();


                //}
                //catch (DbUpdateException)
                //{

                //    throw;

                //}
                return new StatusCodeResult(StatusCodes.Status200OK);
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var _serialToDelete = _unitOfWork.SerialNumbers.GetGuid(id);
            try
            {
                _unitOfWork.SerialNumbers.Remove(_serialToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
    }
}
