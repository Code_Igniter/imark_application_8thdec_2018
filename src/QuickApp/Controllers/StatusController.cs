﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class StatusController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public StatusController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        // GET: api/<controller>
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<StatusViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize)
        {
            var statusQuery = _unitOfWork.Statuses.GetAll().ToList();

            return Ok(statusQuery);

        }
    }
}
