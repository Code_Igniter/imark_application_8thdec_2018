﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class BankViewModel
    {
        public Guid Id { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string ContactNo1 { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string ContactPerson1 { get; set; }
        public string ContactPerson2 { get; set; }
        public string ContactNo2 { get; set; }

    }
    public class BankDashboardViewModel
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public Guid BankId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid MaintenanceId { get; set; }
        public string BankName { get; set; }
        public int MaintenanceNumber { get; set; }
        public int RollNumber { get; set; }
        public int SoldNumber { get; set; }
        public DateTime DeployDate { get; set; }
        public int Total { get; set; }
        public int PreviousMonth { get; set; }
        public int CurrentMonth { get; set; }
        public int PendingDeployment { get; set; }
        public int GEM { get; set; }
        public int EM { get; set; }
        public int IWL { get; set; }
        public string TertiaryGem{ get; set; }
        public string TertiaryEm { get; set; }
        public string TertiaryIwl { get; set; }
        public int TotalGem { get; set; }
        public int TotalEm { get; set; }
        public int TotalIwl { get; set; }
        public int TotalMaintenance { get; set; }
        public int TotalPrevMonth { get; set; }
        public int TotalCurrenMonth { get; set; }
        public int TotalPending { get; set; }
        public int TotalDeploy { get; set; }
        public int TotalBank { get; set; }
        //public string Em { get; set; }
        //public string Iwl { get; set; }
        public string Verification { get; set; }

    }
    public class StockViewModel
    {
        public Guid Id { get; set; }
        public int Total { get; set; }
        public int TotalStock { get; set; }
        public int TotalNotReceived { get; set; }
        public int TotalMaintenance { get; set; }
        public int TotalDamage { get; set; }
        public int TotalSoldDeploy { get; set; }
        public int TotalDeploy { get; set; }
        public int TotalSold { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }

    }

}
