﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class DamagePoseViewModel
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public Guid ApplicationVersionId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public string Status { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string IdTerminal { get; set; }
        public string Outlet { get; set; }
        public string Address { get; set; }
        public string ApplicationFormat { get; set; }
        public string District { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string SerialKey { get; set; }
        public string Item { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid SerialNumberId { get; set; }



    }
}
  