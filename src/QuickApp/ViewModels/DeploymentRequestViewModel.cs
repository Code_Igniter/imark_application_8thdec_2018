﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class DeploymentRequestViewModel
    {
        public Guid Id { get; set; }
       
        public Guid BankId { get; set; }
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string IdTerminal { get; set; }
        public string TerminalType { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public string RentalInventoryAdd { get; set; }
        public string Item { get; set; }
        public string Currency { get; set; }
        public bool TipAdjustment { get; set; }
        public bool ManualTransaction { get; set; }
        public bool PreAuthorization { get; set; }
        public string PrimaryNacNumber { get; set; }
        public string SecondaryNacNumber { get; set; }
        public bool Priority { get; set; }
        public bool IsImarkProperty { get; set; }
        public Guid StatusId { get; set; }
        public string Remarks { get; set; }
        public string StatusForDeploy { get; set; }
        public string RemarksForDeploy { get; set; }
        public string Verification { get; set; }
        public Guid RequestTypeId { get; set; }
        public Guid CheckerId { get; set; }
        public bool Refund { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string RequestName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string SerialKey { get; set; }
        public string StatusName { get; set; }
        public string BankName { get; set; }
        public string MerchantLocation { get; set; }
        public Guid? SerialNumberId { get; set; } //sold to partial rental bank

        public string SerialNumber { get; set; }
        public string LocationForPartialRental { get; set; }
        public string RemarksForPartialRental { get; set; }
        public DateTime SoldDate { get; set; }
        public string ReceivedBy { get; set; }
        public string ReceivedContactNo { get; set; }
        public string Connectivity { get; set; }

    }
    public class DeploymentAddRequestViewModel
    {
        public Guid Id { get; set; }
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string IdTerminal { get; set; }
        public string TerminalType { get; set; }
        public string Item { get; set; }
        public Guid InventoryId { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string Currency { get; set; }
        public bool TipAdjustment { get; set; }
        public bool ManualTransaction { get; set; }
        public bool PreAuthorization { get; set; }
        public string PrimaryNacNumber { get; set; }
        public string SecondaryNacNumber { get; set; }
        public bool Priority { get; set; }
        public Guid StatusId { get; set; }
        public string StatusName { get; set; }
        public string Remarks { get; set; }
        public string RemarksForDeploy { get; set; }
        public string Verification { get; set; }
        public string Name { get; set; }
        public Guid RequestTypeId { get; set; }
        public string RequestName { get; set; }
        public Guid CheckerId { get; set; }
        public bool Refund { get; set; }
        public string SerialKey { get; set; }
        public string PreviousSerial { get; set; }
        public string ConfigStatus { get; set; }
        public string ApplicationVersion { get; set; }
        public string StatusForDeploy { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}
