﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class DeploymentViewModel
    {
        public Guid Id { get; set; }
        public string ReceivedBy { get; set; }
        public string MerchantLocation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Image { get; set; }
        public string Scanned { get; set; }
        public string Remarks { get; set; }
        public string StatusForDeploy { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public string Verification { get; set; }
        public string StatusName { get; set; }
        public string RequestName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime DeployDate { get; set; }
        public Guid BankId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid? InventoryAddId { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string BankName { get; set; }
        public bool Dispatch { get; set; }
        public String Connectivity { get; set; }
        public String Connectivity1 { get; set; }
        public String Connectivity2 { get; set; }
        public String Connectivity3 { get; set; }
        public String Connectivity4 { get; set; }
        public String Connectivity5 { get; set; }
        public String Connectivity6 { get; set; }
    }
}
