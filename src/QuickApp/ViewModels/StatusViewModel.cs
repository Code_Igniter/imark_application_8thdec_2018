﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class StatusViewModel
    {
        public Guid Id { get; set; }
        public string StatusName { get; set; }
    }

}
